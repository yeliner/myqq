﻿namespace myQQ
{
    partial class FrmLogin
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmLogin));
            this.btnLogin = new System.Windows.Forms.Button();
            this.lblFind = new System.Windows.Forms.Label();
            this.lblAdUse = new System.Windows.Forms.Label();
            this.cbselfLogin = new System.Windows.Forms.CheckBox();
            this.cbRemenber = new System.Windows.Forms.CheckBox();
            this.cbouser = new System.Windows.Forms.ComboBox();
            this.lbltishi = new System.Windows.Forms.Label();
            this.btnDesigine = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnChange = new System.Windows.Forms.Button();
            this.picturehead = new System.Windows.Forms.PictureBox();
            this.pictureErweima = new System.Windows.Forms.PictureBox();
            this.pictureDuoge = new System.Windows.Forms.PictureBox();
            this.picturegif = new System.Windows.Forms.PictureBox();
            this.txtpwd = new System.Windows.Forms.TextBox();
            this.login = new System.Windows.Forms.NotifyIcon(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.picturehead)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureErweima)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureDuoge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picturegif)).BeginInit();
            this.SuspendLayout();
            // 
            // btnLogin
            // 
            this.btnLogin.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnLogin.Cursor = System.Windows.Forms.Cursors.AppStarting;
            this.btnLogin.FlatAppearance.BorderSize = 0;
            resources.ApplyResources(this.btnLogin, "btnLogin");
            this.btnLogin.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.UseVisualStyleBackColor = false;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // lblFind
            // 
            resources.ApplyResources(this.lblFind, "lblFind");
            this.lblFind.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblFind.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.lblFind.Name = "lblFind";
            this.lblFind.Click += new System.EventHandler(this.lblFind_Click);
            // 
            // lblAdUse
            // 
            resources.ApplyResources(this.lblAdUse, "lblAdUse");
            this.lblAdUse.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblAdUse.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.lblAdUse.Name = "lblAdUse";
            this.lblAdUse.Click += new System.EventHandler(this.lblAdUse_Click);
            // 
            // cbselfLogin
            // 
            resources.ApplyResources(this.cbselfLogin, "cbselfLogin");
            this.cbselfLogin.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbselfLogin.Name = "cbselfLogin";
            this.cbselfLogin.UseVisualStyleBackColor = true;
            // 
            // cbRemenber
            // 
            resources.ApplyResources(this.cbRemenber, "cbRemenber");
            this.cbRemenber.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cbRemenber.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbRemenber.Name = "cbRemenber";
            this.cbRemenber.UseVisualStyleBackColor = true;
            this.cbRemenber.CheckedChanged += new System.EventHandler(this.cbRemenber_CheckedChanged);
            // 
            // cbouser
            // 
            this.cbouser.Cursor = System.Windows.Forms.Cursors.IBeam;
            resources.ApplyResources(this.cbouser, "cbouser");
            this.cbouser.FormattingEnabled = true;
            this.cbouser.Name = "cbouser";
//            this.cbouser.SelectedIndexChanged += new System.EventHandler(this.cbouser_SelectedIndexChanged);
            this.cbouser.TextChanged += new System.EventHandler(this.cbouser_TextChanged);
            // 
            // lbltishi
            // 
            resources.ApplyResources(this.lbltishi, "lbltishi");
            this.lbltishi.BackColor = System.Drawing.Color.Transparent;
            this.lbltishi.ForeColor = System.Drawing.Color.Red;
            this.lbltishi.Name = "lbltishi";
            // 
            // btnDesigine
            // 
            this.btnDesigine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(124)))), ((int)(((byte)(210)))));
            resources.ApplyResources(this.btnDesigine, "btnDesigine");
            this.btnDesigine.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDesigine.FlatAppearance.BorderSize = 0;
            this.btnDesigine.Image = global::myQQ.Properties.Resources.设置图标;
            this.btnDesigine.Name = "btnDesigine";
            this.btnDesigine.UseCompatibleTextRendering = true;
            this.btnDesigine.UseVisualStyleBackColor = false;
            this.btnDesigine.Click += new System.EventHandler(this.btnDesigine_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(124)))), ((int)(((byte)(210)))));
            resources.ApplyResources(this.btnEdit, "btnEdit");
            this.btnEdit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEdit.FlatAppearance.BorderSize = 0;
            this.btnEdit.Image = global::myQQ.Properties.Resources.退出;
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.UseCompatibleTextRendering = true;
            this.btnEdit.UseVisualStyleBackColor = false;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click_1);
            // 
            // btnChange
            // 
            this.btnChange.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(124)))), ((int)(((byte)(210)))));
            resources.ApplyResources(this.btnChange, "btnChange");
            this.btnChange.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnChange.FlatAppearance.BorderSize = 0;
            this.btnChange.Image = global::myQQ.Properties.Resources.最小化;
            this.btnChange.Name = "btnChange";
            this.btnChange.UseCompatibleTextRendering = true;
            this.btnChange.UseVisualStyleBackColor = false;
            this.btnChange.Click += new System.EventHandler(this.btnChange_Click);
            // 
            // picturehead
            // 
            this.picturehead.BackgroundImage = global::myQQ.Properties.Resources.初始头像;
            resources.ApplyResources(this.picturehead, "picturehead");
            this.picturehead.Name = "picturehead";
            this.picturehead.TabStop = false;
            // 
            // pictureErweima
            // 
            this.pictureErweima.Image = global::myQQ.Properties.Resources.二维码;
            resources.ApplyResources(this.pictureErweima, "pictureErweima");
            this.pictureErweima.Name = "pictureErweima";
            this.pictureErweima.TabStop = false;
            // 
            // pictureDuoge
            // 
            this.pictureDuoge.Image = global::myQQ.Properties.Resources.灰人;
            resources.ApplyResources(this.pictureDuoge, "pictureDuoge");
            this.pictureDuoge.Name = "pictureDuoge";
            this.pictureDuoge.TabStop = false;
            // 
            // picturegif
            // 
            this.picturegif.Cursor = System.Windows.Forms.Cursors.Default;
            this.picturegif.Image = global::myQQ.Properties.Resources.登录;
            resources.ApplyResources(this.picturegif, "picturegif");
            this.picturegif.Name = "picturegif";
            this.picturegif.TabStop = false;
            this.picturegif.MouseDown += new System.Windows.Forms.MouseEventHandler(this.FrmLogin_MouseDown);
            this.picturegif.MouseMove += new System.Windows.Forms.MouseEventHandler(this.FrmLogin_MouseMove);
            this.picturegif.MouseUp += new System.Windows.Forms.MouseEventHandler(this.FrmLogin_MouseUp);
            // 
            // txtpwd
            // 
            resources.ApplyResources(this.txtpwd, "txtpwd");
            this.txtpwd.Name = "txtpwd";
          //  this.txtpwd.TextChanged += new System.EventHandler(this.txtpwd_TextChanged);
            // 
            // login
            // 
            resources.ApplyResources(this.login, "login");
            // 
            // FrmLogin
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(235)))), ((int)(((byte)(253)))));
            this.ControlBox = false;
            this.Controls.Add(this.txtpwd);
            this.Controls.Add(this.lbltishi);
            this.Controls.Add(this.btnDesigine);
            this.Controls.Add(this.btnEdit);
            this.Controls.Add(this.btnChange);
            this.Controls.Add(this.picturehead);
            this.Controls.Add(this.pictureErweima);
            this.Controls.Add(this.pictureDuoge);
            this.Controls.Add(this.btnLogin);
            this.Controls.Add(this.lblFind);
            this.Controls.Add(this.lblAdUse);
            this.Controls.Add(this.cbselfLogin);
            this.Controls.Add(this.cbRemenber);
            this.Controls.Add(this.cbouser);
            this.Controls.Add(this.picturegif);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmLogin";
            this.Load += new System.EventHandler(this.FrmLogin_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.FrmLogin_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.FrmLogin_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.FrmLogin_MouseUp);
            ((System.ComponentModel.ISupportInitialize)(this.picturehead)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureErweima)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureDuoge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picturegif)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private System.Windows.Forms.Button btnDesigine;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Button btnChange;
        public System.Windows.Forms.PictureBox picturehead;
        private System.Windows.Forms.PictureBox pictureErweima;
        private System.Windows.Forms.PictureBox pictureDuoge;
        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.Label lblFind;
        private System.Windows.Forms.Label lblAdUse;
        private System.Windows.Forms.CheckBox cbselfLogin;
        private System.Windows.Forms.CheckBox cbRemenber;
        private System.Windows.Forms.ComboBox cbouser;
        private System.Windows.Forms.PictureBox picturegif;
        private System.Windows.Forms.Label lbltishi;
        private System.Windows.Forms.TextBox txtpwd;
        private System.Windows.Forms.NotifyIcon login;

        #endregion
        // private System.Windows.Forms.PictureBox pbhead;
        //private System.Windows.Forms.PictureBox pbback;
    }
}

