﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace myQQ
{
    class DatabaseHelper
    {
        private static string connstring = "data source=.;initial catalog=MyQQ;User ID=sa;Pwd=123456";
        public static SqlConnection connection = new SqlConnection(connstring);

        //增删改操作
        public static int executeNonQuery(String sql)
        {
            int num = 0;
            try
            {
                connection.Open();
                SqlCommand cmd = new SqlCommand(sql, connection);
                num = cmd.ExecuteNonQuery();
            }
          
            finally
            {
                connection.Close();
            }
            return num;
        }
        //查询操作 返回DataSet
        public static DataSet executeDataSet(String sql)
        {
            DataSet dataSet = new DataSet();

            try
            {
                SqlDataAdapter adapter = new SqlDataAdapter(sql, connection);
                adapter.Fill(dataSet);
            }
            catch (Exception)
            {
            }
            
            return dataSet;
        }
    }
}
