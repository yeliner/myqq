﻿namespace myQQ
{
    partial class FrmEditPerson
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.paneltop = new System.Windows.Forms.Panel();
            this.lblQQ = new System.Windows.Forms.Label();
            this.lblAutoGraph = new System.Windows.Forms.Label();
            this.lblniname = new System.Windows.Forms.Label();
            this.picturehead = new System.Windows.Forms.PictureBox();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnChange = new System.Windows.Forms.Button();
            this.lblAddress = new System.Windows.Forms.Label();
            this.lblsex = new System.Windows.Forms.Label();
            this.lblniname1 = new System.Windows.Forms.Label();
            this.lblAutoGraph2 = new System.Windows.Forms.Label();
            this.txtAutograph = new System.Windows.Forms.TextBox();
            this.txteditname = new System.Windows.Forms.TextBox();
            this.txteditaddress = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dtpbrith = new System.Windows.Forms.DateTimePicker();
            this.cbsex = new System.Windows.Forms.ComboBox();
            this.btnsave = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.paneltop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picturehead)).BeginInit();
            this.SuspendLayout();
            // 
            // paneltop
            // 
            this.paneltop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(154)))), ((int)(((byte)(218)))));
            this.paneltop.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.paneltop.Controls.Add(this.lblQQ);
            this.paneltop.Controls.Add(this.lblAutoGraph);
            this.paneltop.Controls.Add(this.lblniname);
            this.paneltop.Controls.Add(this.picturehead);
            this.paneltop.Controls.Add(this.btnExit);
            this.paneltop.Controls.Add(this.btnChange);
            this.paneltop.Location = new System.Drawing.Point(1, 0);
            this.paneltop.Name = "paneltop";
            this.paneltop.Size = new System.Drawing.Size(459, 146);
            this.paneltop.TabIndex = 2;
            this.paneltop.Paint += new System.Windows.Forms.PaintEventHandler(this.paneltop_Paint);
            this.paneltop.MouseDown += new System.Windows.Forms.MouseEventHandler(this.FrmEditPerson_MouseDown);
            this.paneltop.MouseMove += new System.Windows.Forms.MouseEventHandler(this.FrmEditPerson_MouseMove);
            this.paneltop.MouseUp += new System.Windows.Forms.MouseEventHandler(this.FrmEditPerson_MouseUp);
            // 
            // lblQQ
            // 
            this.lblQQ.AutoSize = true;
            this.lblQQ.Location = new System.Drawing.Point(72, 54);
            this.lblQQ.Name = "lblQQ";
            this.lblQQ.Size = new System.Drawing.Size(0, 12);
            this.lblQQ.TabIndex = 32;
            // 
            // lblAutoGraph
            // 
            this.lblAutoGraph.AutoSize = true;
            this.lblAutoGraph.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblAutoGraph.Location = new System.Drawing.Point(72, 82);
            this.lblAutoGraph.Name = "lblAutoGraph";
            this.lblAutoGraph.Size = new System.Drawing.Size(77, 12);
            this.lblAutoGraph.TabIndex = 2;
            this.lblAutoGraph.Text = "编辑个性签名";
            // 
            // lblniname
            // 
            this.lblniname.AutoSize = true;
            this.lblniname.Location = new System.Drawing.Point(81, 42);
            this.lblniname.Name = "lblniname";
            this.lblniname.Size = new System.Drawing.Size(0, 12);
            this.lblniname.TabIndex = 26;
            // 
            // picturehead
            // 
            this.picturehead.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.picturehead.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.picturehead.Location = new System.Drawing.Point(3, 32);
            this.picturehead.Name = "picturehead";
            this.picturehead.Size = new System.Drawing.Size(63, 62);
            this.picturehead.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picturehead.TabIndex = 23;
            this.picturehead.TabStop = false;
            this.picturehead.Click += new System.EventHandler(this.picturehead_Click);
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(154)))), ((int)(((byte)(218)))));
            this.btnExit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnExit.FlatAppearance.BorderSize = 0;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Image = global::myQQ.Properties.Resources.退出;
            this.btnExit.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnExit.Location = new System.Drawing.Point(423, 0);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(26, 26);
            this.btnExit.TabIndex = 22;
            this.btnExit.UseCompatibleTextRendering = true;
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnChange
            // 
            this.btnChange.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(154)))), ((int)(((byte)(218)))));
            this.btnChange.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnChange.FlatAppearance.BorderSize = 0;
            this.btnChange.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnChange.Image = global::myQQ.Properties.Resources.最小化;
            this.btnChange.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnChange.Location = new System.Drawing.Point(391, 0);
            this.btnChange.Name = "btnChange";
            this.btnChange.Size = new System.Drawing.Size(26, 26);
            this.btnChange.TabIndex = 21;
            this.btnChange.UseCompatibleTextRendering = true;
            this.btnChange.UseVisualStyleBackColor = false;
            this.btnChange.Click += new System.EventHandler(this.btnChange_Click);
            // 
            // lblAddress
            // 
            this.lblAddress.AutoSize = true;
            this.lblAddress.Location = new System.Drawing.Point(226, 312);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(65, 12);
            this.lblAddress.TabIndex = 39;
            this.lblAddress.Text = "所 在 地：";
            // 
            // lblsex
            // 
            this.lblsex.AutoSize = true;
            this.lblsex.Location = new System.Drawing.Point(226, 277);
            this.lblsex.Name = "lblsex";
            this.lblsex.Size = new System.Drawing.Size(65, 12);
            this.lblsex.TabIndex = 38;
            this.lblsex.Text = "性    别：";
            // 
            // lblniname1
            // 
            this.lblniname1.AutoSize = true;
            this.lblniname1.Location = new System.Drawing.Point(22, 277);
            this.lblniname1.Name = "lblniname1";
            this.lblniname1.Size = new System.Drawing.Size(65, 12);
            this.lblniname1.TabIndex = 37;
            this.lblniname1.Text = "昵    称：";
            // 
            // lblAutoGraph2
            // 
            this.lblAutoGraph2.AutoSize = true;
            this.lblAutoGraph2.Location = new System.Drawing.Point(22, 164);
            this.lblAutoGraph2.Name = "lblAutoGraph2";
            this.lblAutoGraph2.Size = new System.Drawing.Size(53, 12);
            this.lblAutoGraph2.TabIndex = 40;
            this.lblAutoGraph2.Text = "个性签名";
            // 
            // txtAutograph
            // 
            this.txtAutograph.Location = new System.Drawing.Point(24, 179);
            this.txtAutograph.Multiline = true;
            this.txtAutograph.Name = "txtAutograph";
            this.txtAutograph.Size = new System.Drawing.Size(406, 51);
            this.txtAutograph.TabIndex = 41;
            // 
            // txteditname
            // 
            this.txteditname.Location = new System.Drawing.Point(84, 274);
            this.txteditname.Name = "txteditname";
            this.txteditname.Size = new System.Drawing.Size(121, 21);
            this.txteditname.TabIndex = 42;
            // 
            // txteditaddress
            // 
            this.txteditaddress.Location = new System.Drawing.Point(288, 309);
            this.txteditaddress.Name = "txteditaddress";
            this.txteditaddress.Size = new System.Drawing.Size(98, 21);
            this.txteditaddress.TabIndex = 44;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 309);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 45;
            this.label1.Text = "生    日：";
            // 
            // dtpbrith
            // 
            this.dtpbrith.AllowDrop = true;
            this.dtpbrith.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.dtpbrith.Location = new System.Drawing.Point(84, 309);
            this.dtpbrith.Name = "dtpbrith";
            this.dtpbrith.Size = new System.Drawing.Size(121, 21);
            this.dtpbrith.TabIndex = 47;
            this.dtpbrith.Value = new System.DateTime(2016, 4, 22, 15, 6, 18, 0);
            // 
            // cbsex
            // 
            this.cbsex.FormattingEnabled = true;
            this.cbsex.Items.AddRange(new object[] {
            "男",
            "女"});
            this.cbsex.Location = new System.Drawing.Point(288, 275);
            this.cbsex.Name = "cbsex";
            this.cbsex.Size = new System.Drawing.Size(98, 20);
            this.cbsex.TabIndex = 48;
            // 
            // btnsave
            // 
            this.btnsave.Location = new System.Drawing.Point(288, 363);
            this.btnsave.Name = "btnsave";
            this.btnsave.Size = new System.Drawing.Size(75, 23);
            this.btnsave.TabIndex = 49;
            this.btnsave.Text = "保存";
            this.btnsave.UseVisualStyleBackColor = true;
            this.btnsave.Click += new System.EventHandler(this.btnsave_Click);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(373, 363);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 50;
            this.btnClose.Text = "关闭";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // FrmEditPerson
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(460, 398);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnsave);
            this.Controls.Add(this.cbsex);
            this.Controls.Add(this.dtpbrith);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txteditaddress);
            this.Controls.Add(this.txteditname);
            this.Controls.Add(this.txtAutograph);
            this.Controls.Add(this.lblAutoGraph2);
            this.Controls.Add(this.lblAddress);
            this.Controls.Add(this.lblsex);
            this.Controls.Add(this.lblniname1);
            this.Controls.Add(this.paneltop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmEditPerson";
            this.Text = "FrmEditPerson";
            this.Load += new System.EventHandler(this.FrmEditPerson_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.FrmEditPerson_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.FrmEditPerson_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.FrmEditPerson_MouseUp);
            this.paneltop.ResumeLayout(false);
            this.paneltop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picturehead)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel paneltop;
        private System.Windows.Forms.Label lblQQ;
        private System.Windows.Forms.Label lblAutoGraph;
        private System.Windows.Forms.Label lblniname;
        private System.Windows.Forms.PictureBox picturehead;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnChange;
        private System.Windows.Forms.Label lblAddress;
        private System.Windows.Forms.Label lblsex;
        private System.Windows.Forms.Label lblniname1;
        private System.Windows.Forms.Label lblAutoGraph2;
        private System.Windows.Forms.TextBox txtAutograph;
        private System.Windows.Forms.TextBox txteditname;
        private System.Windows.Forms.TextBox txteditaddress;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtpbrith;
        private System.Windows.Forms.ComboBox cbsex;
        private System.Windows.Forms.Button btnsave;
        private System.Windows.Forms.Button btnClose;
    }
}