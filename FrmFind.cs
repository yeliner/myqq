﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace myQQ
{
    public partial class FrmFind : Form
    {
        public FrmFind()
        {
            InitializeComponent();
        }
        string a1, a2, a3;

        private void btnnext_Click(object sender, EventArgs e)
        {
            if (boolanswer())
            {
                ppwd.Visible = true;
                btnnext.Visible = false;
                btnok.Visible = true;

            }
        }

        private void btnok_Click(object sender, EventArgs e)
        {
            if (boolnull())
            {
                try
                {
                    Convert.ToInt32(txtPwd.Text);
                    lblresult.ForeColor = Color.Red;
                    lblresult.Text = "较低";
                }
                catch (Exception)
                {
                    lblresult.ForeColor = Color.Green;
                    lblresult.Text = "较强";
                }
                string sql = string.Format("  update [dbo].[UserInfo] set Password='{0}'  where qq={1}",txtPwd.Text,FrmLogin.qq);
                int num = DatabaseHelper.executeNonQuery(sql);
                if (num>0)
                {
                    MessageBox.Show("修改成功", "提示");
                    this.Hide();
                    FrmLogin l = new FrmLogin();
                    l.Show();
                }
            }
        }

        private bool boolnull()
        {
            lblpwdtishi.Text = ""; lblpwdagintishi.Text = "";
            if (txtPwd.Text.Trim() == "")
            {
                lblpwdtishi.Text = "密码不能为空！请输入密码";
                txtPwd.Focus();
                return false;
            }
            if (txtPwd.Text.Length < 6 || txtPwd.Text.Length > 16)
            {
                lblpwdtishi.Text = "密码为6-16位数组成！";
                txtPwd.Focus();
                return false;
            }
            
           
            if (txtpwdagin.Text.Trim() == "")
            {
                lblpwdagintishi.Text = "请确定密码！";
                txtpwdagin.Focus();
                return false;
            }

            if (!txtPwd.Text.Equals(txtpwdagin.Text))
            {
                lblpwdagintishi.Text = "确认密码必须和新密码相同！";
                txtpwdagin.Focus();
                return false;
            }
            return true;
        }

        private bool boolanswer()
        {
            lblt1.Visible = false; lblt2.Visible = false; lblt3.Visible = false;
            if (txta1.Text != a1)
            {
                lblt1.Visible = true;
                txta1.Focus();
                return false;
            }
            if (txta2.Text != a2)
            {
                lblt2.Visible = true;
                txta2.Focus();
                return false;
            }
            if (txta3.Text != a3)
            {
                lblt3.Visible = true;
                txta1.Focus();
                return false;
            }
            return true;
        }

        private void FrmFind_Load(object sender, EventArgs e)
        {
            string sql = " select * from [dbo].[safe] where qq="+FrmLogin.qq;
            DataSet ds = DatabaseHelper.executeDataSet(sql);
            try
            {
                lbla1.Text = ds.Tables[0].Rows[0]["Ask1"].ToString();
                lbla2.Text = ds.Tables[0].Rows[0]["Ask2"].ToString();
                lbla3.Text = ds.Tables[0].Rows[0]["Ask3"].ToString();
                a1= ds.Tables[0].Rows[0]["Answer1"].ToString();
                a2 = ds.Tables[0].Rows[0]["Answer2"].ToString();
                a3 = ds.Tables[0].Rows[0]["Answer3"].ToString();

            }
            catch (Exception)
            {
                panel1.Visible = false;
                btnnext.Visible = false;
                btncancel.Visible = false;
                lblnone.Visible = true;
            }
        }
        Point mouseOff;//鼠标移动位置变量
        bool leftFlag;//标记是否为左键


        private void FrmFind_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                mouseOff = new Point(-e.X, -e.Y);//得到变量的值
                leftFlag = true;                  //点击左键按下时标注为true;
            }
        }

        private void btnChange_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void btncancel_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void FrmFind_MouseMove(object sender, MouseEventArgs e)
        {
            if (leftFlag)
            {
                Point mouseSet = Control.MousePosition;
                mouseSet.Offset(mouseOff.X, mouseOff.Y);  //设置移动后的位置
                Location = mouseSet;
            }
        }

        private void FrmFind_MouseUp(object sender, MouseEventArgs e)
        {

            if (leftFlag)
            {
                leftFlag = false;//释放鼠标后标注为false;
            }

        }
    }
}
