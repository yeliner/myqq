﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace myQQ
{
    public partial class FrmZhuce : Form
    {
        public FrmZhuce()
        {
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
        public static int QQ;
        public static string pwd;
        private int Getqq()
        {//获取qq号
            Random rd = new Random();
            int qq = rd.Next(100000, 1000000);
            string sql = "select count(*) from [dbo].UserInfo where qq=" + qq;
             DataSet ds = DatabaseHelper.executeDataSet(sql);
            if (Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString())!=0)
           {
               Getqq();
           }
            return qq;
        }
        private void btnSubmit_Click(object sender, EventArgs e)
        {//显示注册成功后窗体

           if (boolnull())
            {
                QQ=Getqq();
                string sql = string.Format("insert into [dbo].[UserInfo](qq,Password,Niname,Sex,Birth,Address) values({0}, '{1}', '{2}','{3}','{4}','{5}')",
               QQ,txtPwd.Text, txtNiname.Text, rbogirl.Checked ? "女" : "男", dtpbrith.Value.ToShortDateString(),txtAddress.Text);
                int num = DatabaseHelper.executeNonQuery(sql);
                string sql1 = string.Format("insert into [dbo].[Group] values({0},'我的好友')",QQ);
                int num1 = DatabaseHelper.executeNonQuery(sql1);
                if (num > 0&&num1>0)
                {//显示注册成功后窗体
                    pwd = txtPwd.Text;
                    FrmZhuceLater l = new FrmZhuceLater();
                    this.Visible = false;
                    l.Show();
                }
            }
        }
        private bool boolnull()
        {//判断
            lblniname.Text= ""; lbltishipwd.Text = ""; lblsure.Text = "";
            if (txtNiname.Text.Trim() == "")
            {
                lblniname.Text="昵称不能为空！请输入昵称";
                txtNiname.Focus();
                return false;
            }
            
            if (txtPwd.Text.Trim() == "")
            {
                lbltishipwd.Text = "密码不能为空！请输入密码";
                txtPwd.Focus();
                return false;
            }
            if (txtPwd.Text.Length < 6 || txtPwd.Text.Length > 16)
            {
                lbltishipwd.Text = "密码为6-16位数组成！";
                txtPwd.Focus();
                return false;
            }
            if (txtpwdagin.Text.Trim() == "")
            {
                lblsure.Text = "请确定密码！";
                txtpwdagin.Focus();
                return false;
            }
           
            if (!txtPwd.Text.Equals(txtpwdagin.Text))
            {
                lblsure.Text = "确认密码必须和输入密码相同！";
                txtpwdagin.Focus();
                return false;
            }
            if (txtAddress.Text.Trim()=="")
            {
                txtAddress.Text ="中国";
            }
            return true;
        }

        Point mouseOff;//鼠标移动位置变量
        bool leftFlag;//标记是否为左键
        private void FrmZhuce_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                mouseOff = new Point(-e.X, -e.Y); //得到变量的值
                leftFlag = true;                  //点击左键按下时标注为true;
            }
        }

        private void FrmZhuce_MouseMove(object sender, MouseEventArgs e)
        {
            if (leftFlag)
            {
                Point mouseSet = Control.MousePosition;
                mouseSet.Offset(mouseOff.X, mouseOff.Y);  //设置移动后的位置
                Location = mouseSet;
            }
        }

        private void FrmZhuce_MouseUp(object sender, MouseEventArgs e)
        {
            if (leftFlag)
            {
                leftFlag = false;//释放鼠标后标注为false;
            }
        }
    }
    }

