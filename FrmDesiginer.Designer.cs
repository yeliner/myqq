﻿namespace myQQ
{
    partial class FrmDesiginer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnok = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txtdizhi = new System.Windows.Forms.TextBox();
            this.cbologindizhi = new System.Windows.Forms.ComboBox();
            this.cbologinleixin = new System.Windows.Forms.ComboBox();
            this.txtloginduankou = new System.Windows.Forms.TextBox();
            this.lbllduankou = new System.Windows.Forms.Label();
            this.lblldizhi = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblwlsz = new System.Windows.Forms.Label();
            this.lbldlfwq = new System.Windows.Forms.Label();
            this.btntext = new System.Windows.Forms.Button();
            this.cboleixin = new System.Windows.Forms.ComboBox();
            this.txtare = new System.Windows.Forms.TextBox();
            this.lblare = new System.Windows.Forms.Label();
            this.txtpwd = new System.Windows.Forms.TextBox();
            this.lblpwd = new System.Windows.Forms.Label();
            this.txtuser = new System.Windows.Forms.TextBox();
            this.lbluser = new System.Windows.Forms.Label();
            this.txtduankou = new System.Windows.Forms.TextBox();
            this.lblduankou = new System.Windows.Forms.Label();
            this.lbldizhi = new System.Windows.Forms.Label();
            this.lblleixin = new System.Windows.Forms.Label();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnChange = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(205)))), ((int)(((byte)(226)))), ((int)(((byte)(242)))));
            this.panel1.BackgroundImage = global::myQQ.Properties.Resources.上;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panel1.Controls.Add(this.btnCancel);
            this.panel1.Controls.Add(this.btnok);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.btnEdit);
            this.panel1.Controls.Add(this.btnChange);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(435, 333);
            this.panel1.TabIndex = 2;
            // 
            // btnCancel
            // 
            this.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnCancel.FlatAppearance.BorderSize = 0;
            this.btnCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnCancel.Location = new System.Drawing.Point(352, 308);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(80, 25);
            this.btnCancel.TabIndex = 16;
            this.btnCancel.Text = "取消";
            this.btnCancel.UseCompatibleTextRendering = true;
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnok
            // 
            this.btnok.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnok.FlatAppearance.BorderSize = 0;
            this.btnok.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnok.Location = new System.Drawing.Point(256, 308);
            this.btnok.Name = "btnok";
            this.btnok.Size = new System.Drawing.Size(80, 25);
            this.btnok.TabIndex = 15;
            this.btnok.Text = "确定";
            this.btnok.UseCompatibleTextRendering = true;
            this.btnok.UseVisualStyleBackColor = true;
            this.btnok.Click += new System.EventHandler(this.btnok_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(235)))), ((int)(((byte)(253)))));
            this.panel2.Controls.Add(this.txtdizhi);
            this.panel2.Controls.Add(this.cbologindizhi);
            this.panel2.Controls.Add(this.cbologinleixin);
            this.panel2.Controls.Add(this.txtloginduankou);
            this.panel2.Controls.Add(this.lbllduankou);
            this.panel2.Controls.Add(this.lblldizhi);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.lblwlsz);
            this.panel2.Controls.Add(this.lbldlfwq);
            this.panel2.Controls.Add(this.btntext);
            this.panel2.Controls.Add(this.cboleixin);
            this.panel2.Controls.Add(this.txtare);
            this.panel2.Controls.Add(this.lblare);
            this.panel2.Controls.Add(this.txtpwd);
            this.panel2.Controls.Add(this.lblpwd);
            this.panel2.Controls.Add(this.txtuser);
            this.panel2.Controls.Add(this.lbluser);
            this.panel2.Controls.Add(this.txtduankou);
            this.panel2.Controls.Add(this.lblduankou);
            this.panel2.Controls.Add(this.lbldizhi);
            this.panel2.Controls.Add(this.lblleixin);
            this.panel2.Location = new System.Drawing.Point(1, 32);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(433, 275);
            this.panel2.TabIndex = 14;
            // 
            // txtdizhi
            // 
            this.txtdizhi.BackColor = System.Drawing.SystemColors.Window;
            this.txtdizhi.Location = new System.Drawing.Point(214, 43);
            this.txtdizhi.Name = "txtdizhi";
            this.txtdizhi.ReadOnly = true;
            this.txtdizhi.Size = new System.Drawing.Size(100, 21);
            this.txtdizhi.TabIndex = 26;
            // 
            // cbologindizhi
            // 
            this.cbologindizhi.FormattingEnabled = true;
            this.cbologindizhi.Items.AddRange(new object[] {
            ""});
            this.cbologindizhi.Location = new System.Drawing.Point(214, 191);
            this.cbologindizhi.Name = "cbologindizhi";
            this.cbologindizhi.Size = new System.Drawing.Size(100, 20);
            this.cbologindizhi.TabIndex = 25;
            // 
            // cbologinleixin
            // 
            this.cbologinleixin.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbologinleixin.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbologinleixin.FormattingEnabled = true;
            this.cbologinleixin.Items.AddRange(new object[] {
            "不使用高级选项",
            "UDP类型",
            "TCP类型"});
            this.cbologinleixin.Location = new System.Drawing.Point(73, 190);
            this.cbologinleixin.Name = "cbologinleixin";
            this.cbologinleixin.Size = new System.Drawing.Size(100, 20);
            this.cbologinleixin.TabIndex = 24;
            this.cbologinleixin.SelectedIndexChanged += new System.EventHandler(this.cbologinleixin_SelectedIndexChanged);
            // 
            // txtloginduankou
            // 
            this.txtloginduankou.BackColor = System.Drawing.SystemColors.Window;
            this.txtloginduankou.Location = new System.Drawing.Point(355, 190);
            this.txtloginduankou.Name = "txtloginduankou";
            this.txtloginduankou.ReadOnly = true;
            this.txtloginduankou.Size = new System.Drawing.Size(61, 21);
            this.txtloginduankou.TabIndex = 23;
            // 
            // lbllduankou
            // 
            this.lbllduankou.AutoSize = true;
            this.lbllduankou.Location = new System.Drawing.Point(320, 193);
            this.lbllduankou.Name = "lbllduankou";
            this.lbllduankou.Size = new System.Drawing.Size(29, 12);
            this.lbllduankou.TabIndex = 22;
            this.lbllduankou.Text = "端口";
            // 
            // lblldizhi
            // 
            this.lblldizhi.AutoSize = true;
            this.lblldizhi.Location = new System.Drawing.Point(179, 193);
            this.lblldizhi.Name = "lblldizhi";
            this.lblldizhi.Size = new System.Drawing.Size(29, 12);
            this.lblldizhi.TabIndex = 20;
            this.lblldizhi.Text = "地址";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(38, 193);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 12);
            this.label3.TabIndex = 19;
            this.label3.Text = "类型";
            // 
            // lblwlsz
            // 
            this.lblwlsz.AutoSize = true;
            this.lblwlsz.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblwlsz.Location = new System.Drawing.Point(21, 14);
            this.lblwlsz.Name = "lblwlsz";
            this.lblwlsz.Size = new System.Drawing.Size(57, 12);
            this.lblwlsz.TabIndex = 18;
            this.lblwlsz.Text = "网络设置";
            // 
            // lbldlfwq
            // 
            this.lbldlfwq.AutoSize = true;
            this.lbldlfwq.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbldlfwq.Location = new System.Drawing.Point(38, 156);
            this.lbldlfwq.Name = "lbldlfwq";
            this.lbldlfwq.Size = new System.Drawing.Size(70, 12);
            this.lbldlfwq.TabIndex = 17;
            this.lbldlfwq.Text = "登录服务器";
            // 
            // btntext
            // 
            this.btntext.Location = new System.Drawing.Point(355, 116);
            this.btntext.Name = "btntext";
            this.btntext.Size = new System.Drawing.Size(61, 23);
            this.btntext.TabIndex = 16;
            this.btntext.Text = "测试";
            this.btntext.UseVisualStyleBackColor = true;
            // 
            // cboleixin
            // 
            this.cboleixin.BackColor = System.Drawing.SystemColors.HighlightText;
            this.cboleixin.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboleixin.FormattingEnabled = true;
            this.cboleixin.Items.AddRange(new object[] {
            "不使用代理",
            "HTTP代理",
            "SOCKS5代理",
            "使用浏览器设置",
            ""});
            this.cboleixin.Location = new System.Drawing.Point(73, 44);
            this.cboleixin.Name = "cboleixin";
            this.cboleixin.Size = new System.Drawing.Size(100, 20);
            this.cboleixin.TabIndex = 15;
            this.cboleixin.SelectedIndexChanged += new System.EventHandler(this.cboleixin_SelectedIndexChanged);
            // 
            // txtare
            // 
            this.txtare.BackColor = System.Drawing.SystemColors.Window;
            this.txtare.Location = new System.Drawing.Point(355, 89);
            this.txtare.Name = "txtare";
            this.txtare.ReadOnly = true;
            this.txtare.Size = new System.Drawing.Size(61, 21);
            this.txtare.TabIndex = 14;
            // 
            // lblare
            // 
            this.lblare.AutoSize = true;
            this.lblare.Location = new System.Drawing.Point(332, 92);
            this.lblare.Name = "lblare";
            this.lblare.Size = new System.Drawing.Size(17, 12);
            this.lblare.TabIndex = 13;
            this.lblare.Text = "域";
            // 
            // txtpwd
            // 
            this.txtpwd.BackColor = System.Drawing.SystemColors.Window;
            this.txtpwd.Location = new System.Drawing.Point(214, 89);
            this.txtpwd.Name = "txtpwd";
            this.txtpwd.ReadOnly = true;
            this.txtpwd.Size = new System.Drawing.Size(100, 21);
            this.txtpwd.TabIndex = 12;
            // 
            // lblpwd
            // 
            this.lblpwd.AutoSize = true;
            this.lblpwd.Location = new System.Drawing.Point(179, 92);
            this.lblpwd.Name = "lblpwd";
            this.lblpwd.Size = new System.Drawing.Size(29, 12);
            this.lblpwd.TabIndex = 11;
            this.lblpwd.Text = "密码";
            // 
            // txtuser
            // 
            this.txtuser.BackColor = System.Drawing.SystemColors.Window;
            this.txtuser.Location = new System.Drawing.Point(73, 89);
            this.txtuser.Name = "txtuser";
            this.txtuser.ReadOnly = true;
            this.txtuser.Size = new System.Drawing.Size(100, 21);
            this.txtuser.TabIndex = 10;
            // 
            // lbluser
            // 
            this.lbluser.AutoSize = true;
            this.lbluser.Location = new System.Drawing.Point(26, 92);
            this.lbluser.Name = "lbluser";
            this.lbluser.Size = new System.Drawing.Size(41, 12);
            this.lbluser.TabIndex = 9;
            this.lbluser.Text = "用户名";
            // 
            // txtduankou
            // 
            this.txtduankou.BackColor = System.Drawing.SystemColors.Window;
            this.txtduankou.Location = new System.Drawing.Point(355, 44);
            this.txtduankou.Name = "txtduankou";
            this.txtduankou.ReadOnly = true;
            this.txtduankou.Size = new System.Drawing.Size(61, 21);
            this.txtduankou.TabIndex = 8;
            // 
            // lblduankou
            // 
            this.lblduankou.AutoSize = true;
            this.lblduankou.Location = new System.Drawing.Point(320, 47);
            this.lblduankou.Name = "lblduankou";
            this.lblduankou.Size = new System.Drawing.Size(29, 12);
            this.lblduankou.TabIndex = 7;
            this.lblduankou.Text = "端口";
            // 
            // lbldizhi
            // 
            this.lbldizhi.AutoSize = true;
            this.lbldizhi.Location = new System.Drawing.Point(179, 47);
            this.lbldizhi.Name = "lbldizhi";
            this.lbldizhi.Size = new System.Drawing.Size(29, 12);
            this.lbldizhi.TabIndex = 5;
            this.lbldizhi.Text = "地址";
            // 
            // lblleixin
            // 
            this.lblleixin.AutoSize = true;
            this.lblleixin.Location = new System.Drawing.Point(38, 47);
            this.lblleixin.Name = "lblleixin";
            this.lblleixin.Size = new System.Drawing.Size(29, 12);
            this.lblleixin.TabIndex = 3;
            this.lblleixin.Text = "类型";
            // 
            // btnEdit
            // 
            this.btnEdit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(162)))), ((int)(((byte)(216)))));
            this.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnEdit.FlatAppearance.BorderSize = 0;
            this.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEdit.Image = global::myQQ.Properties.Resources.退出;
            this.btnEdit.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnEdit.Location = new System.Drawing.Point(401, 0);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(34, 25);
            this.btnEdit.TabIndex = 13;
            this.btnEdit.UseCompatibleTextRendering = true;
            this.btnEdit.UseVisualStyleBackColor = false;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnChange
            // 
            this.btnChange.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(162)))), ((int)(((byte)(216)))));
            this.btnChange.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnChange.FlatAppearance.BorderSize = 0;
            this.btnChange.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnChange.Image = global::myQQ.Properties.Resources.最小化1;
            this.btnChange.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnChange.Location = new System.Drawing.Point(369, 0);
            this.btnChange.Name = "btnChange";
            this.btnChange.Size = new System.Drawing.Size(35, 26);
            this.btnChange.TabIndex = 12;
            this.btnChange.UseCompatibleTextRendering = true;
            this.btnChange.UseVisualStyleBackColor = false;
            this.btnChange.Click += new System.EventHandler(this.btnChange_Click);
            // 
            // FrmDesiginer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(434, 335);
            this.ControlBox = false;
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmDesiginer";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "设置";
            this.Load += new System.EventHandler(this.FrmDesiginer_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.FrmDesiginer_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.FrmDesiginer_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.FrmDesiginer_MouseUp);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnok;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Button btnChange;
        private System.Windows.Forms.ComboBox cboleixin;
        private System.Windows.Forms.TextBox txtare;
        private System.Windows.Forms.Label lblare;
        private System.Windows.Forms.TextBox txtpwd;
        private System.Windows.Forms.Label lblpwd;
        private System.Windows.Forms.TextBox txtuser;
        private System.Windows.Forms.Label lbluser;
        private System.Windows.Forms.TextBox txtduankou;
        private System.Windows.Forms.Label lblduankou;
        private System.Windows.Forms.Label lbldizhi;
        private System.Windows.Forms.Label lblleixin;
        private System.Windows.Forms.ComboBox cbologindizhi;
        private System.Windows.Forms.ComboBox cbologinleixin;
        private System.Windows.Forms.TextBox txtloginduankou;
        private System.Windows.Forms.Label lbllduankou;
        private System.Windows.Forms.Label lblldizhi;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblwlsz;
        private System.Windows.Forms.Label lbldlfwq;
        private System.Windows.Forms.Button btntext;
        private System.Windows.Forms.TextBox txtdizhi;
    }
}