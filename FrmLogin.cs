﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace myQQ
{
    public partial class FrmLogin : Form
    {
        public FrmLogin()
        {
            InitializeComponent();
        }
        public static int qq;
        public static Image head;
        public static Icon Ico;
        private void FrmLogin_Load(object sender, EventArgs e)
        {//加载
            cbouserload();
            if (FrmZhuce.QQ != 0)
            {
                cbouser.Text = FrmZhuce.QQ.ToString();
                txtpwd.Text = FrmZhuce.pwd;
                string sql = " select headImage,Sex from [dbo].[UserInfo] where qq=" + cbouser.Text;
                DataSet ds = DatabaseHelper.executeDataSet(sql);
                //从数据库中读取图像路径
                picturehead.BackgroundImage = Image.FromFile(ds.Tables[0].Rows[0]["headImage"].ToString());
                head = picturehead.BackgroundImage;
            }
            login.Icon = iconShow("男");
        }

       private Icon iconShow(string sex)
        {
            if (sex == "男")
                    {
                        Icon = new Icon(@"D:\专业\project\myQQ\myQQ\img\ico\32\GG\imonline.ico");
                return Icon;
                    }
                    else
                    {
                        Icon = new Icon(@"D:\专业\project\myQQ\myQQ\img\ico\32\MM\imonline.ico");
                return Icon;
            }
                }

        private void cbouserload()
        {
            //加载记住密码的用户
            string sql = "select qq,pwd from [dbo].[remenber]";
            DataSet use = DatabaseHelper.executeDataSet(sql);
            try
            {
                for (int i = 0; i < use.Tables[0].Rows.Count; i++)
                {
                    cbouser.Items.Add(use.Tables[0].Rows[i]["qq"].ToString());
                    if (cbouser.Text == use.Tables[0].Rows[i]["qq"].ToString())
                    {
                        txtpwd.Text = use.Tables[0].Rows[i]["pwd"].ToString();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void btnDesigine_Click(object sender, EventArgs e)
        {
            //打开设置窗口
            FrmDesiginer des = new FrmDesiginer();
            des.Show();
        }

        private void btnEdit_Click_1(object sender, EventArgs e)
        {
            //关闭程序
            Application.Exit();
        }

        private void btnChange_Click(object sender, EventArgs e)
        {//最小化
            this.WindowState = FormWindowState.Minimized;
        }

        private void lblAdUse_Click(object sender, EventArgs e)
        {
            //点击注册，跳转到注册窗体
            this.Hide();
            FrmZhuce zhuce = new FrmZhuce();
            zhuce.Show();

        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            lbltishi.Text = "";
            if (ValiInput())
            {
                if (ValiLogin())
                {//通过判断后，跳转到登录中窗体
                    login.Visible = false;
                    this.Visible = false;
                    qq = int.Parse(cbouser.Text);
                    FrmLogining ing = new FrmLogining();
                    ing.Show();
                }
                else
                {
                    lbltishi.Text = "账号或者密码错误";
                }
            }
        }

        private bool ValiInput()
        {//非空判断
            lbltishi.Text = "";
            if (cbouser.Text.Trim() == "")
            {
                lbltishi.Text = "用户名/账号不能为空";
                cbouser.Focus();
                return false;
            }

            if (txtpwd.Text.Trim() == "")
            {
                lbltishi.Text = "密码不能为空";
                txtpwd.Focus();
                return false;
            }
            return true;
        }
        private bool ValiLogin()
        {  //判断数据库里是否有数据
            int num = 0;
            string sql = string.Format("select * from [dbo].[UserInfo] where qq='{0}'and password='{1}'",
            cbouser.Text, txtpwd.Text);
            try
            {
                DatabaseHelper.connection.Open();
                SqlCommand command = new SqlCommand(sql, DatabaseHelper.connection);
                num = (int)command.ExecuteScalar();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
            finally
            {
                DatabaseHelper.connection.Close();
            }
            return num > 0;

        }
        Point mouseOff;//鼠标移动位置变量
        bool leftFlag;//标记是否为左键


        private void FrmLogin_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                mouseOff = new Point(-e.X, -e.Y);//得到变量的值
                leftFlag = true;                  //点击左键按下时标注为true;
            }
        }

        private void FrmLogin_MouseMove(object sender, MouseEventArgs e)
        {
            if (leftFlag)
            {
                Point mouseSet = Control.MousePosition;
                mouseSet.Offset(mouseOff.X, mouseOff.Y);  //设置移动后的位置
                Location = mouseSet;
            }
        }

        private void FrmLogin_MouseUp(object sender, MouseEventArgs e)
        {

            if (leftFlag)
            {
                leftFlag = false;//释放鼠标后标注为false;
            }

        }

        private void cbouser_TextChanged(object sender, EventArgs e)
        {
            picturehead.BackgroundImage = Image.FromFile(@"D:\专业\project\myQQ\myQQ\Head\40.PNG");

            if (cbouser.Text.Length < 6 && cbouser.Text.Length > 0)
            {
                this.picturehead.BackgroundImage = Image.FromFile(@"D:\专业\project\myQQ\myQQ\Head\40.PNG");
            }
            if (cbouser.Text.Length == 6)
            {
                try
                {
                    Convert.ToInt32(cbouser.Text);
                    string sql = " select headImage,Sex  from UserInfo where qq=" + cbouser.Text;
                    DataSet ds = DatabaseHelper.executeDataSet(sql);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        picturehead.BackgroundImage = Image.FromFile(ds.Tables[0].Rows[0]["headImage"].ToString());
                        head = picturehead.BackgroundImage;
                        login.Icon = iconShow(ds.Tables[0].Rows[0]["Sex"].ToString());
                        login.Text = login.Text + cbouser.Text;
                        string sql1 = " select * from [dbo].[remenber] where qq=" + cbouser.Text;
                            DataSet d = DatabaseHelper.executeDataSet(sql1);
                            txtpwd.Text = d.Tables[0].Rows[0]["pwd"].ToString();
                            cbRemenber.Checked = true;
                      
                    }
                }
                catch (Exception)
                {
                }

            }
        }

        private void cbRemenber_CheckedChanged(object sender, EventArgs e)
        {
            if (this.cbRemenber.Checked)
            {//
                string sql = " select * from [dbo].[UserInfo] where qq=" + cbouser.Text;
                DataSet ds = DatabaseHelper.executeDataSet(sql);
                string sql1 = " select * from [dbo].[remenber] where qq=" + cbouser.Text;
                DataSet d = DatabaseHelper.executeDataSet(sql1);
                if (ds.Tables[0].Rows.Count > 0 && d.Tables[0].Rows.Count <= 0)
                {
                    string sqls = string.Format("  insert into [dbo].[remenber] values({0},'{1}') ", cbouser.Text, txtpwd.Text);
                    int num = DatabaseHelper.executeNonQuery(sqls);
                    if (num > 0)
                    {
                        cbouser.Items.Clear();
                        cbouserload();
                    }
                }
            }
        }

        private void lblFind_Click(object sender, EventArgs e)
        {
            FrmLogin.qq = Convert.ToInt32(cbouser.Text);
            FrmFind f = new FrmFind();
            f.Show();
        }
    }
}
