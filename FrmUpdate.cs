﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace myQQ
{
    public partial class FrmUpdate : Form
    {
        public FrmUpdate()
        {
            InitializeComponent();
        }
        private void txtadd_TextChanged(object sender, EventArgs e)
        {

            if (txtadd.Text == "")
            {
                btncancel.Visible = true;
                btnsure.Visible = false;
            }
            else
            {
                btncancel.Visible = false;
                btnsure.Visible = true;
            }
        }

        private void FrmUpdate_Load(object sender, EventArgs e)
        {

            if (FrmMain.cms == "tsmiaddg")
            {
                paddgroup.Visible = true;
                pdeleteg.Visible = false;
            }
            if (FrmMain.cms == "tsmideleteg")
            {
                cmdegload();
                paddgroup.Visible = false;
                pdeleteg.Visible = true;
            }
            if (FrmMain.cms == "tmsireg")
            {
                paddgroup.Visible = false;
                pdeleteg.Visible = false;
                pReg.Visible = true;
                cbRegload();
            }
            if (FrmMain.cms == "Refriend")
            {
                paddgroup.Visible = false;
                pdeleteg.Visible = false;
                pReg.Visible = false;
                pRefriend.Visible = true;
            }
            if (FrmMain.cms == "tsmimfriend")
            {
                paddgroup.Visible = false;
                pdeleteg.Visible = false;
                pReg.Visible = false;
                pRefriend.Visible = false;
                pmovefri.Visible = true;
                cbmovefriload();
            }
        }

        private void cbmovefriload()
        {
            string sql2 = "select Groupname,id from  [dbo].[Group] where Zqq=" + FrmMain.Lqq;
            DataSet fz = DatabaseHelper.executeDataSet(sql2);

            for (int i = 0; i < fz.Tables[0].Rows.Count; i++)
            {//分组
                cbmovefri.Items.Add(fz.Tables[0].Rows[i]["Groupname"].ToString());
            }
        }

        private void cbRegload()
        {
            string sql2 = "select Groupname,id from  [dbo].[Group] where Zqq=" + FrmMain.Lqq;
            DataSet fz = DatabaseHelper.executeDataSet(sql2);

            for (int i = 0; i < fz.Tables[0].Rows.Count; i++)
            {//分组
                cbReg.Items.Add(fz.Tables[0].Rows[i]["Groupname"].ToString());
            }
        }

        private void cmdegload()
        {
            string sql2 = "select Groupname,id from  [dbo].[Group] where Zqq=" + FrmMain.Lqq;
            DataSet fz = DatabaseHelper.executeDataSet(sql2);

            for (int i = 0; i < fz.Tables[0].Rows.Count; i++)
            {//分组
                cbdeletegname.Items.Add(fz.Tables[0].Rows[i]["Groupname"].ToString());
            }
        }

        private void btncancel_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void btnsure_Click(object sender, EventArgs e)
        {
            addgroup();
        }
        private void addgroup()
        {
            string gname = txtadd.Text;
            string sql1 = string.Format("select * from [dbo].[Group] where Zqq={0} and Groupname='{1}'", FrmMain.Lqq, gname);
            DataSet g = DatabaseHelper.executeDataSet(sql1);
            if (g.Tables[0].Rows.Count <= 0)
            {
                //ChatListItem add = new ChatListItem(gname);
                //chatList.Items.Add(add);
                string sql = string.Format(" insert into [dbo].[Group] (Zqq,Groupname) values({0},'{1}')", FrmMain.Lqq, gname);
                int num = DatabaseHelper.executeNonQuery(sql);
                txtadd.Visible = false;
                btnsure.Visible = false;
                if (num > 0)
                {
                    this.Hide();
                }
            }
            else
            {
                DialogResult r = MessageBox.Show("该组名已在使用，是否重新输入组名", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (r == DialogResult.Yes)
                {
                    txtadd.Focus();
                }
                else
                {
                    txtadd.Visible = false;
                    btnsure.Visible = false;
                }
            }
        }
        Point mouseOff;//鼠标移动位置变量
        bool leftFlag;//标记是否为左键


        private void FrmUpdate_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                mouseOff = new Point(-e.X, -e.Y);//得到变量的值
                leftFlag = true;                  //点击左键按下时标注为true;
            }
        }

        private void FrmUpdate_MouseMove(object sender, MouseEventArgs e)
        {
            if (leftFlag)
            {
                Point mouseSet = Control.MousePosition;
                mouseSet.Offset(mouseOff.X, mouseOff.Y);  //设置移动后的位置
                Location = mouseSet;
            }
        }

        private void FrmUpdate_MouseUp(object sender, MouseEventArgs e)
        {

            if (leftFlag)
            {
                leftFlag = false;//释放鼠标后标注为false;
            }

        }

        private void cbdeletegname_TextChanged(object sender, EventArgs e)
        {
            cbmoveg.Items.Clear();
            foreach (string item in cbdeletegname.Items)
            {
                if (item == cbdeletegname.Text)
                {
                    continue;
                }
                cbmoveg.Items.Add(item);
            }
        }

        private void btndegok_Click(object sender, EventArgs e)
        {
            lbldegtishi.Visible = false;
            lblmovetishi.Visible = false;
            if (cbdeletegname.Text == "")
            {
                lbldegtishi.Visible = true;
                cbdeletegname.Focus();
                return;
            }
            if (cbmoveg.Text == "")
            {
                lblmovetishi.Visible = true;
                cbmoveg.Focus();
                return;
            }
            try
            {
                string sql = string.Format("select fqq  from Fridens where Groupid=(select id from[dbo].[Group]  where Zqq = {0} and Groupname = '{1}')", FrmMain.Lqq, cbdeletegname.Text);
                DataSet fqqs = DatabaseHelper.executeDataSet(sql);
                if (fqqs.Tables[0].Rows.Count > 0)
                {
                    string sqlup = string.Format("update Fridens set Groupid = (select  id from[dbo].[Group]  where Zqq = {0} and Groupname = '{1}') where Zqq = {0} and Groupid = (select id from[dbo].[Group]  where Zqq = {0} and Groupname = '{2}')", FrmMain.Lqq, cbmoveg.Text, cbdeletegname.Text);
                    int numup = DatabaseHelper.executeNonQuery(sqlup);
                    if (numup >= 0)
                    {
                        MessageBox.Show("好友移动分组成功！");
                    }
                }
            }
            finally
            {
                string sqlde = string.Format("delete [dbo].[Group] where Zqq={0} and Groupname='{1}'", FrmMain.Lqq, cbdeletegname.Text);
                int numde = DatabaseHelper.executeNonQuery(sqlde);
                if (numde > 0)
                {
                    MessageBox.Show("删除改分组成功！");
                    this.Hide();
                }
            }



        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void btnReok_Click(object sender, EventArgs e)
        {
            lblregtishi.Visible = false;
            lblregtishi2.Visible = false;
            if (cbReg.Text == "")
            {
                lblregtishi.Visible = true;
                cbReg.Focus();
                return;
            }
            if (txtnewname.Text == "")
            {
                lblregtishi2.Visible = true;
                txtnewname.Focus();
                return;
            }
            string gname = txtnewname.Text;
            string sql1 = string.Format("select * from [dbo].[Group] where Zqq={0} and Groupname='{1}'", FrmMain.Lqq, gname);
            DataSet g = DatabaseHelper.executeDataSet(sql1);
            if (g.Tables[0].Rows.Count <= 0)
            {

                string sql = string.Format(" update [dbo].[Group] set Groupname='{0}' where Zqq={1} and Groupname='{2}'", gname, FrmMain.Lqq, cbReg.Text);
                int num = DatabaseHelper.executeNonQuery(sql);
                if (num >= 0)
                {
                    MessageBox.Show("分组重命名成功！");
                    this.Hide();
                }
            }
            else
            {
                DialogResult r = MessageBox.Show("该组名已在使用，是否重新输入组名", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (r == DialogResult.Yes)
                {
                    txtnewname.Focus();
                }
                else
                {
                    this.Hide();
                }
            }
        }

        private void txtrefriend_TextChanged(object sender, EventArgs e)
        {
            if (txtrefriend.Text == "")
            {
                btnRefricancel.Visible = true;
                btnRefriok.Visible = false;
            }
            else
            {
                btnRefricancel.Visible = false;
                btnRefriok.Visible = true;
            }
        }

        private void btnRefriok_Click(object sender, EventArgs e)
        {
            string sql = string.Format("  update Fridens set  remark='{0}' where  fqq={1} and Zqq={2} ", txtrefriend.Text, FrmMain.reId, FrmMain.Lqq);
            int i = DatabaseHelper.executeNonQuery(sql);
            if (i >= 0)
            {
                MessageBox.Show("好友重命名成功！");
                this.Hide();
            }
        }

        private void btnmfriok_Click(object sender, EventArgs e)
        {
            string sqlup = string.Format("update Fridens set Groupid = (select  id from [dbo].[Group]  where Zqq = {0} and Groupname = '{1}') where Zqq = {0} and fqq={2}", FrmMain.Lqq, cbmovefri.Text,FrmMain.reId);
            int numup = DatabaseHelper.executeNonQuery(sqlup);
            if (numup >= 0)
            {
                MessageBox.Show("好友移动分组成功！");
                this.Hide();
            }
        }
    }
}
