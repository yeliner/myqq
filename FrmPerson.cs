﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace myQQ
{
    public partial class FrmPerson : Form
    {
        public FrmPerson()
        {
            InitializeComponent();
        }

        private void btnChange_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Hide();
            FrmMain main = new FrmMain();
            main.Show();
        }
        public static int qq;
        private void FrmPerson_Load(object sender, EventArgs e)
        {
            qq = FrmLogin.qq;
            lblQQ.Text = qq.ToString();
            lblQQ2.Text = qq.ToString();
            string sql1 = "  select Niname,Sex,Birth,Address,Autograph,headImage from UserInfo where qq=" + qq;
            DataSet ds = DatabaseHelper.executeDataSet(sql1);
            lblniname.Text = ds.Tables[0].Rows[0]["Niname"].ToString();
            lblniname2.Text = ds.Tables[0].Rows[0]["Niname"].ToString();
            lblsex.Text= ds.Tables[0].Rows[0]["Sex"].ToString();
            lblbrith.Text = ds.Tables[0].Rows[0]["Birth"].ToString();
            lblAddress2 .Text = ds.Tables[0].Rows[0]["Address"].ToString();
            txtAutograph.Text = ds.Tables[0].Rows[0]["Autograph"].ToString();
            picturehead.BackgroundImage=Image.FromFile(ds.Tables[0].Rows[0]["headImage"].ToString());


        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            this.Hide();
            FrmEditPerson edit = new FrmEditPerson();
            edit.Show();
        }
        Point mouseOff;//鼠标移动位置变量
        bool leftFlag;//标记是否为左键


        private void FrmPerson_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                mouseOff = new Point(-e.X, -e.Y);//得到变量的值
                leftFlag = true;                  //点击左键按下时标注为true;
            }
        }

        private void FrmPerson_MouseMove(object sender, MouseEventArgs e)
        {
            if (leftFlag)
            {
                Point mouseSet = Control.MousePosition;
                mouseSet.Offset(mouseOff.X, mouseOff.Y);  //设置移动后的位置
                Location = mouseSet;
            }
        }

        private void FrmPerson_MouseUp(object sender, MouseEventArgs e)
        {

            if (leftFlag)
            {
                leftFlag = false;//释放鼠标后标注为false;
            }

        }
    }
}
