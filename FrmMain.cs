﻿using myQQ.chat;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using _CUSTOM_CONTROLS._ChatListBox;
namespace myQQ
{
    public partial class FrmMain : Form
    {
        public FrmMain()
        {
            InitializeComponent();
        }
        ChatListSubItem.UserStatus Userstate, fUserstate;
        public static int Lqq, askqq, sendqq, chatqq, shanqq,reId;
        public static string asknews, cms, ntime;
        public static string reState;
        public static int gcount, Ncount, num ;
        public static string Sex;
        public static string S, Fs;
        
        private void btnExit_Click(object sender, EventArgs e)
        {//退出登录
            DialogResult r = MessageBox.Show("确定退出QQ？", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            if (r == DialogResult.OK)
            {
                Userstate = ChatListSubItem.UserStatus.OffLine;
                updateuseStste();
                Application.Exit();
            }
        }
        private void btnChange_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        private void FrmMain_Load(object sender, EventArgs e)
        {
            Lqq = FrmLogin.qq;
            btnLine.Image = tsmiOnline.Image;
            Userstate = ChatListSubItem.UserStatus.Online;
            updateuseStste(); //状态
            Useinfo(); //显示
            mIcon.Icon = iconShow(Sex, Userstate);
            this.Icon = mIcon.Icon;
            mIcon.Text = mIcon.Text + lblniname;
            gruopshow();//分组
            newsShow();
        }

        private Icon iconShow(string sex, ChatListSubItem.UserStatus s)
        {
            if (sex == "女")
            {
                if (s == ChatListSubItem.UserStatus.Online)
                {
                    Icon = new Icon(@"D:\专业\project\myQQ\myQQ\img\ico\32\MM\imonline.ico");
                }
                if (s == ChatListSubItem.UserStatus.Busy)
                {
                    Icon = new Icon(@"D:\专业\project\myQQ\myQQ\img\ico\32\MM\busy.ico");
                }
                if (s == ChatListSubItem.UserStatus.DontDisturb)
                {
                    Icon = new Icon(@"D:\专业\project\myQQ\myQQ\img\ico\32\MM\mute.ico");
                }
            if (s == ChatListSubItem.UserStatus.OffLine)
                 {
                     Icon = new Icon(@"D:\专业\project\myQQ\myQQ\img\ico\32\MM\invisible.ico");
                 }
                if (s == ChatListSubItem.UserStatus.Away)
                {
                    Icon = new Icon(@"D:\专业\project\myQQ\myQQ\img\ico\32\MM\away.ico");
                }
                if (s == ChatListSubItem.UserStatus.OffLine)
                {
                    Icon = new Icon(@"D:\专业\project\myQQ\myQQ\img\ico\32\MM\offline.ico");
                }
                if (s == ChatListSubItem.UserStatus.QMe)
                {
                    Icon = new Icon(@"D:\专业\project\myQQ\myQQ\img\ico\32\MM\Qme.ico");
                }
                return Icon;
            }
            else
            {

                if (s == ChatListSubItem.UserStatus.Online)
                {
                    Icon = new Icon(@"D:\专业\project\myQQ\myQQ\img\ico\32\GG\imonline.ico");
                }
                if (s == ChatListSubItem.UserStatus.Busy)
                {
                    Icon = new Icon(@"D:\专业\project\myQQ\myQQ\img\ico\32\GG\busy.ico");
                }
                if (s == ChatListSubItem.UserStatus.DontDisturb)
                {
                    Icon = new Icon(@"D:\专业\project\myQQ\myQQ\img\ico\32\GG\mute.ico");
                }
                if (s == ChatListSubItem.UserStatus.OffLine)
                {
                    Icon = new Icon(@"D:\专业\project\myQQ\myQQ\img\ico\32\GG\invisible.ico");
                }
                if (s == ChatListSubItem.UserStatus.Away)
                {
                    Icon = new Icon(@"D:\专业\project\myQQ\myQQ\img\ico\32\GG\away.ico");
                }
                if (s == ChatListSubItem.UserStatus.OffLine)
                {
                    Icon = new Icon(@"D:\专业\project\myQQ\myQQ\img\ico\32\GG\offline.ico");
                }
                if (s == ChatListSubItem.UserStatus.QMe)
                {
                    Icon = new Icon(@"D:\专业\project\myQQ\myQQ\img\ico\32\GG\Qme.ico");
                }
                return Icon;
            }
        }

        private void picturehead_Click(object sender, EventArgs e)
        {//更换头像
            OpenFileDialog fimage = new OpenFileDialog();
            fimage.InitialDirectory = (@"D:\专业\project\myQQ\myQQ\Head");
            fimage.Filter = "图片|*.png";
            fimage.ShowDialog();
            if (fimage.FileName!="")
            {
                string sql = string.Format(" update [dbo].[UserInfo] set headImage='{0}' where qq={1}", fimage.FileName, Lqq);
                int num = DatabaseHelper.executeNonQuery(sql);
                if (num > 0)
                {
                    string sql2 = "select headImage from [dbo].[UserInfo] where qq=" + Lqq;
                    DataSet ds = DatabaseHelper.executeDataSet(sql2);
                    picturehead.BackgroundImage = Image.FromFile(ds.Tables[0].Rows[0]["headImage"].ToString());
                }
            }
            
        }
        private void dgvNews_CellClick(object sender, DataGridViewCellEventArgs e)
        {//消息
            if (dgvNews.SelectedRows.Count > 0)
            {
                asknews = dgvNews.SelectedRows[0].Cells["askNews"].Value.ToString();
                sendqq = Convert.ToInt32(dgvNews.SelectedRows[0].Cells["Sendqq"].Value.ToString());//信息传递

                if (dgvNews.SelectedRows[0].Cells["NewsType"].Value.ToString().Trim() == "验证")
                {
                    reState = dgvNews.SelectedRows[0].Cells["RState"].Value.ToString();
                    FrmNews s = new FrmNews();
                    s.Show();
                }
                huifutype();
               /* if (dgvNews.SelectedRows[0].Cells["NewsType"].Value.ToString().Trim() == "chat")
                {
                    reState = dgvNews.SelectedRows[0].Cells["RState"].Value.ToString();
                    FrmMain.chatqq = FrmMain.sendqq;
                    FrmChat c = new FrmChat();
                    c.Show();
                }*/
            }
        }
        private void tsmiAboutme_Click(object sender, EventArgs e)
        {//资料
            this.Hide();
            FrmPerson person = new FrmPerson();
            person.Show();
        }
        private void Useinfo()
        {
            string sql1 = "select Niname,Autograph,headImage,Sex from [dbo].[UserInfo] where qq=" + Lqq;
            DataSet ds = DatabaseHelper.executeDataSet(sql1);
            lblniname.Text = ds.Tables[0].Rows[0]["Niname"].ToString();
            txtAutograph.Text = ds.Tables[0].Rows[0]["Autograph"].ToString();
            picturehead.BackgroundImage = Image.FromFile(ds.Tables[0].Rows[0]["headImage"].ToString());
            Sex = ds.Tables[0].Rows[0]["Sex"].ToString();
        }
        private void updateuseStste()
        {
            string sql = string.Format("update [dbo].[UserInfo] set qqState='{0}' where qq={1}", Userstate, Lqq);
            int i = (int)DatabaseHelper.executeNonQuery(sql);
        }
        private void huifutype()
        {//回复
            if (dgvNews.SelectedRows[0].Cells["NewsType"].Value.ToString().Trim() == "回复")
            {
                //1.查询好友表，判断主被动
                string sqlf = string.Format("select * from [dbo].[Friends] where fqq={0} and Zqq={1}", FrmMain.sendqq, Lqq);
                DataSet f = DatabaseHelper.executeDataSet(sqlf);
                if (f.Tables[0].Rows.Count <= 0)
                {
                    //2.没有，主动，查询临时表
                    string sqll = string.Format("  select remark,gname  from [dbo].[linshi] where Zqq={0} and askqq={1}", Lqq, FrmMain.sendqq);
                    DataSet linhsi = DatabaseHelper.executeDataSet(sqll);
                    //3.添加到好友表
                    string sql2 = string.Format("insert into [dbo].[Friends] (fqq,remark,Groupid,Zqq) values ({0},'{1}',(select id from [dbo].[Group] where Zqq={2} and Groupname='{3}'),{2})",
           FrmMain.sendqq, linhsi.Tables[0].Rows[0]["remark"].ToString(), Lqq, linhsi.Tables[0].Rows[0]["gname"].ToString());
                    int addf = DatabaseHelper.executeNonQuery(sql2);
                    //4.修改，接收消息状态
                    string sql3 = string.Format(" update[dbo].[News] set RState = '已读'where Sendqq={0} and Reciveqq={1} and NewsType = '回复'", FrmMain.sendqq, Lqq);
                    int num3 = DatabaseHelper.executeNonQuery(sql3);
                    if (addf > 0 && num3 > 0)
                    {
                        reState = "已读";
                        FrmNews n = new FrmNews();
                        n.Show();
                    }

                }
                else
                {//有，被动，修改消息接收状态
                    string sql3 = string.Format(" update[dbo].[News] set RState = '已读'where Sendqq={0} and Reciveqq={1} and NewsType = '回复'", FrmMain.sendqq, Lqq);
                    int num3 = DatabaseHelper.executeNonQuery(sql3);
                    if (num3 > 0)
                    {
                        reState = "已读";
                        FrmNews n = new FrmNews();
                        n.Show();
                    }
                }
            }
        }
        private void newsShow()
        {//绑定消息表
            string sql =string.Format( " select Sendqq,NewsType,Context,SState,RState,Time,asknews from [dbo].[News] where NewsType not like 'chat' and  Reciveqq={0} order by Time desc",Lqq);
            DataSet news = DatabaseHelper.executeDataSet(sql);
           
            try
            {
                ntime = news.Tables[0].Rows[0]["Time"].ToString();
                dgvNews.DataSource = news.Tables[0];
            }
            catch (Exception)
            {  
            }
        }
        private void gruopshow()
        {//1.查找分组表
            string sql2 = "select Groupname,id from  [dbo].[Group] where Zqq=" + Lqq;
            DataSet fz = DatabaseHelper.executeDataSet(sql2);
            //2.加载分组个数
            chatList.Items.Clear();
            gcount = fz.Tables[0].Rows.Count;
            for (int i = 0; i < fz.Tables[0].Rows.Count; i++)
            {
                ChatListItem lb = new ChatListItem(fz.Tables[0].Rows[i]["Groupname"].ToString());
                //3.加载分组下的好友
                string sql3 = string.Format("select * from [dbo].[Friends] where Groupid={0} and Zqq={1}", Convert.ToInt32(fz.Tables[0].Rows[i]["id"].ToString()), Lqq);
                DataSet fl = DatabaseHelper.executeDataSet(sql3);
              
                    for (int j = 0; j < fl.Tables[0].Rows.Count; j++)
                    {
                        string sql4 = "select * from [dbo].[UserInfo] where qq=" + Convert.ToInt32(fl.Tables[0].Rows[j]["fqq"].ToString());
                        DataSet info = DatabaseHelper.executeDataSet(sql4);
                        ChatListSubItem xx = new ChatListSubItem(fl.Tables[0].Rows[j]["remark"].ToString(), info.Tables[0].Rows[0]["Niname"].ToString(), info.Tables[0].Rows[0]["Autograph"].ToString());
                        xx.ID = Convert.ToInt32(info.Tables[0].Rows[0]["qq"].ToString());
                        string fstate = info.Tables[0].Rows[0]["qqState"].ToString();
                        xx.Status = boolfState(fstate);
                        xx.HeadImage = Image.FromFile(info.Tables[0].Rows[0]["headImage"].ToString());
                        lb.SubItems.Add(xx);
                        if (xx.ID == shanqq)
                        {
                            xx.IsTwinkle = !xx.IsTwinkle;

                            // chatListBox1.Items[5].SubItems[5].IsTwinkle = !chatListBox1.Items[5].SubItems[5].IsTwinkle;
                        }
                        S = S + xx.Status.ToString();
                    }
                    //lb.SubItems.Sort();
                    //添加项到控件中（添加分组）
                    chatList.Items.Add(lb);
                
                }
        }

        private ChatListSubItem.UserStatus boolfState(string fstate)
        {
            if (fstate =="QMe")
            {
                fUserstate = ChatListSubItem.UserStatus.QMe;
            }
            if (fstate =="Online")
            {
                fUserstate = ChatListSubItem.UserStatus.Online;
            }
            if (fstate =="Away")
            {
                fUserstate = ChatListSubItem.UserStatus.Away;
            }
            if (fstate == "Busy")
            {
                fUserstate = ChatListSubItem.UserStatus.Busy;
            }
            if (fstate == "DontDisturb")
            {
                fUserstate = ChatListSubItem.UserStatus.DontDisturb;
            }
            if (fstate == "OffLine" )
            {
                fUserstate = ChatListSubItem.UserStatus.OffLine;
               
            }
            return fUserstate;
        }

        private void shan()
        {
            string sql = " select Sendqq,NewsType,Context,SState,RState,Time,asknews from [dbo].[News] where NewsType='chat' and RState='未读'  and  Reciveqq=" + Lqq;
            DataSet news = DatabaseHelper.executeDataSet(sql);
            try
            {
                if (news.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < news.Tables[0].Rows.Count; i++)
                    {
                        shanqq = Convert.ToInt32(news.Tables[0].Rows[i]["Sendqq"].ToString());
                        gruopshow();
                    }
                        
                }
            }
            catch (Exception)
            {
                shanqq = 0;
            }
        }
        private void txtAutograph_TextChanged(object sender, EventArgs e)
        {
            string sql = string.Format("update [dbo].[UserInfo] set Autograph='{0}' where qq={1}", txtAutograph.Text, Lqq);
            int i = (int)DatabaseHelper.executeNonQuery(sql);
        }

        private void btnSerch_Click(object sender, EventArgs e)
        {//加好友
           
            if (txtsosuo.Text!="")
            {
                try
                {
                    Convert.ToInt32(txtsosuo.Text);
                    askqq = Convert.ToInt32(txtsosuo.Text);
                    FrmAddfriend f = new FrmAddfriend();
                    f.Show();
                }
                catch (Exception)
                {

                    MessageBox.Show("输入格式不正确！");
                }
            }
            else
	{
                MessageBox.Show("搜索不能为空！");
                }
            
          
        }

        private void chatList_DoubleClickSubItem(object sender, ChatListEventArgs e)
        {//显示聊天窗体
            if (chatList.SelectSubItem != null && chatList.SelectSubItem.OwnerListItem != null)
            {
                chatList.OnFouceSubItem.IsTwinkle = !chatList.OnFouceSubItem.IsTwinkle;
                shanqq = 0;
                chatqq = chatList.SelectSubItem.ID;
                FrmChat m = new FrmChat();
                m.Show();
            }
                
           
        }
        private void tsmiadd_Click(object sender, EventArgs e)
        {
            cms ="tsmiaddg";
            FrmUpdate u = new FrmUpdate();
            u.ShowDialog();

        }
        private void tsmidelete_Click(object sender, EventArgs e)
        {
            string sql2 = "select Groupname,id from  [dbo].[Group] where Zqq=" + FrmMain.Lqq;
            DataSet fz = DatabaseHelper.executeDataSet(sql2);
            if (fz.Tables[0].Rows.Count>1)
            {
                cms = "tsmideleteg";
                FrmUpdate u = new FrmUpdate();
                u.ShowDialog();
            }
            else
            {
                MessageBox.Show("您当前只有一个分组，不能进行删除分组操作!","提示",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
        }
        private void tsmiOnline_Click(object sender, EventArgs e)
        {//0
            btnLine.Image = tsmiOnline.Image;
            Userstate = ChatListSubItem.UserStatus.Online;
            stateUpdae();
        }
        private void stateUpdae()
        {
            string sql = string.Format("update UserInfo set qqState='{0}' where qq={1}", Userstate, Lqq);
            int i = (int)DatabaseHelper.executeNonQuery(sql);
            if (i > 0)
            {
                mIcon.Icon = iconShow(Sex, Userstate);
                if (i > 0)
                {
                    mIcon.Icon = iconShow(Sex, Userstate);
                }
            }
        }
        private void tsmiQme_Click(object sender, EventArgs e)
        {//1
            btnLine.Image = tsmiQme.Image;
            Userstate = ChatListSubItem.UserStatus.QMe;
            stateUpdae();
        }

        private void tsmiDont_Click(object sender, EventArgs e)
        {//2
            btnLine.Image = tsmiDontDisturb.Image;
            Userstate = ChatListSubItem.UserStatus.DontDisturb;
            stateUpdae();
        }
        private void tsmibusy_Click(object sender, EventArgs e)
        {//3
            btnLine.Image = tsmiBusy.Image;
            Userstate = ChatListSubItem.UserStatus.Busy;
            stateUpdae();
        }
        private void tsmiinivisble_Click(object sender, EventArgs e)
        {//4
            btnLine.Image = tsmiinivisble.Image;
            Userstate = ChatListSubItem.UserStatus.OffLine;
            stateUpdae();
        }
        private void tsmiLeave_Click(object sender, EventArgs e)
        {//5
            btnLine.Image = tsmiLeave.Image;
            Userstate = ChatListSubItem.UserStatus.Away;
            stateUpdae();
        }
        private void tsmiOff_Click(object sender, EventArgs e)
        {//6
            btnLine.Image = tsmiOff.Image;
            Userstate = ChatListSubItem.UserStatus.OffLine;
            stateUpdae();
        }

        private void tsmidfriend_Click(object sender, EventArgs e)
        {
            if (chatList.SelectSubItem != null && chatList.OnFouceSubItem != null)
            {
              DialogResult r= MessageBox.Show("是否确定删除该好友？(删除后，对方列表中将自动把你移除)","提示",MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (r==DialogResult.Yes)
                {
                    string sql =string.Format( "delete [dbo].[Fridens] where (Zqq={0} and fqq={1})or (Zqq={1} and fqq={0})",FrmMain.Lqq, chatList.SelectSubItem.ID);
                    int num = DatabaseHelper.executeNonQuery(sql);
                    if (num==2)
                    {
                        MessageBox.Show("删除好友成功！");
                        gruopshow();
                    }
                }
                
            }
        }

        private void tsmire_Click(object sender, EventArgs e)
        {
            cms = "tmsireg";
            FrmUpdate u = new FrmUpdate();
            u.ShowDialog();
        }

        private void tsmimfriend_Click(object sender, EventArgs e)
        {
            if (chatList.OnFouceSubItem != null&& chatList.SelectSubItem!=null)
            {
                cms = "Refriend";
                reId = chatList.SelectSubItem.ID;
                cms = "tsmimfriend";
                FrmUpdate u = new FrmUpdate();
                u.ShowDialog();
            }
        }
        

        private void tsmiEfriend_Click(object sender, EventArgs e)
        {
            if (chatList.SelectSubItem != null && chatList.OnFouceSubItem != null)
            {
                cms = "Refriend";
                reId=chatList.OnFouceSubItem.ID;
                FrmUpdate u = new FrmUpdate();
                u.ShowDialog();
            }
        }

        Point mouseOff;//鼠标移动位置变量
        bool leftFlag;//标记是否为左键


        private void FrmMain_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                mouseOff = new Point(-e.X, -e.Y);//得到变量的值
                leftFlag = true;                  //点击左键按下时标注为true;
            }
        }

        private void FrmMain_MouseMove(object sender, MouseEventArgs e)
        {
            if (leftFlag)
            {
                Point mouseSet = Control.MousePosition;
                mouseSet.Offset(mouseOff.X, mouseOff.Y);  //设置移动后的位置
                Location = mouseSet;
            }
        }

        private void FrmMain_MouseUp(object sender, MouseEventArgs e)
        {

            if (leftFlag)
            {
                leftFlag = false;//释放鼠标后标注为false;
            }

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            string sql =string.Format( " select Sendqq,NewsType,Context,SState,RState,Time,asknews from [dbo].[News] where NewsType not like 'chat' and  Reciveqq={0} and Time>'{1}'  order by Time desc" , Lqq,ntime);
            DataSet news = DatabaseHelper.executeDataSet(sql);
            if (news.Tables[0].Rows.Count>0)
            {
                newsShow();
            }
           
            string sql1 = "select Groupname,id from  [dbo].[Group] where Zqq=" + Lqq;
            DataSet fz = DatabaseHelper.executeDataSet(sql1);
            string  sql2= " select * from [Fridens] where Zqq=" + Lqq;
            DataSet adfz = DatabaseHelper.executeDataSet(sql2);
            try
            {
                if (adfz.Tables[0].Rows.Count != num)
                {
                    num = adfz.Tables[0].Rows.Count;
                    gruopshow();
                }
            }
            catch (Exception) { }
            try
            {
                if (fz.Tables[0].Rows.Count != gcount)
                {
                    gruopshow();
                }
            }
            catch (Exception) { }

            shan();
            fstate();

        }

        private void fstate()
        {
            string sqlfqq = " select fqq from Fridens where Zqq="+Lqq;
            DataSet fqqs = DatabaseHelper.executeDataSet(sqlfqq);
            try
            {
                if (fqqs.Tables[0].Rows.Count>0)
                {
                    for (int i = 0; i < fqqs.Tables[0].Rows.Count; i++)
                    {
                        string sqlfs = "  select * from UserInfo where qq="+fqqs.Tables[0].Rows[i]["fqq"];
                        DataSet fss = DatabaseHelper.executeDataSet(sqlfs);
                        Fs = Fs + fss.Tables[0].Rows[0]["qqState"].ToString();
                    }
                }
            }
            catch (Exception)  {  }
            if (Fs!=S)
            {
                gruopshow();
            }
        }

        private void tsmisafe_Click(object sender, EventArgs e)
        {
            FrmSafe s = new FrmSafe();
            s.Show();
        }

        private void cmsgroup_Opening(object sender, CancelEventArgs e)
        {
            if (chatList.SelectSubItem !=null && chatList.SelectSubItem.OwnerListItem!=null)
            {
                e.Cancel = true;
                chatList.ContextMenuStrip = cmsfriend;
            }
            else
            {
                chatList.ContextMenuStrip = cmsgroup;
            }
        }
    }
}
