﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace myQQ
{
    public partial class FrmEditPerson : Form
    {
        public FrmEditPerson()
        {
            InitializeComponent();
        }
        public static int  qq;

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Hide();
            FrmPerson per = new FrmPerson();
            per.Show();
        }

        private void btnChange_Click(object sender, EventArgs e)
        {

            this.WindowState = FormWindowState.Minimized;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Hide();
            FrmPerson per = new FrmPerson();
            per.Show();
        }

        private void btnsave_Click(object sender, EventArgs e)
        {
            if (boolnull())
            {
                string sql = string.Format("  update UserInfo set Niname='{0}',Sex='{1}',Birth='{2}',Address='{3}',Autograph='{4}' where qq={5};",
              txteditname.Text, cbsex.Text, dtpbrith.Value.ToShortDateString(),txteditaddress.Text,txtAutograph.Text,qq);
                int num = DatabaseHelper.executeNonQuery(sql);
                if (num > 0)
                {//显示编辑成功后窗体
                    this.Hide();
                    FrmMain main = new FrmMain();
                    main.Show();
                }
            }
        }
        private bool boolnull()
        {//判断
            
            if (txteditname.Text.Trim() == "")
            {
                MessageBox.Show( "昵称不能为空！请输入昵称");
                txteditname.Focus();
                return false;
            }

            if (cbsex.Text.Trim() == "")
            {
                MessageBox.Show("请选择性别");
                cbsex.Focus();
                return false;
            }
            
            if (txteditaddress.Text.Trim() == "")
            {
                MessageBox.Show("请输入地址！");
                txteditaddress.Focus();
                return false;
            }

            return true;
        }

        private void paneltop_Paint(object sender, PaintEventArgs e)
        {

        }

        private void FrmEditPerson_Load(object sender, EventArgs e)
        {
            qq = FrmLogin.qq;
            lblQQ.Text = qq.ToString();
            string sql1 = "  select Niname,Sex,Birth,Address,Autograph,headImage from UserInfo where qq=" + qq;
            DataSet ds = DatabaseHelper.executeDataSet(sql1);
            lblniname.Text = ds.Tables[0].Rows[0]["Niname"].ToString();
            txteditname.Text = ds.Tables[0].Rows[0]["Niname"].ToString();
            cbsex.Text = ds.Tables[0].Rows[0]["Sex"].ToString();
            dtpbrith.Text = ds.Tables[0].Rows[0]["Birth"].ToString();
            txteditaddress.Text = ds.Tables[0].Rows[0]["Address"].ToString();
            lblAutoGraph.Text = ds.Tables[0].Rows[0]["Autograph"].ToString();
            txtAutograph.Text = ds.Tables[0].Rows[0]["Autograph"].ToString();
            picturehead.BackgroundImage = Image.FromFile(ds.Tables[0].Rows[0]["headImage"].ToString());

        }
        Point mouseOff;//鼠标移动位置变量
        bool leftFlag;//标记是否为左键


        private void FrmEditPerson_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                mouseOff = new Point(-e.X, -e.Y);//得到变量的值
                leftFlag = true;                  //点击左键按下时标注为true;
            }
        }

        private void FrmEditPerson_MouseMove(object sender, MouseEventArgs e)
        {
            if (leftFlag)
            {
                Point mouseSet = Control.MousePosition;
                mouseSet.Offset(mouseOff.X, mouseOff.Y);  //设置移动后的位置
                Location = mouseSet;
            }
        }

        private void FrmEditPerson_MouseUp(object sender, MouseEventArgs e)
        {

            if (leftFlag)
            {
                leftFlag = false;//释放鼠标后标注为false;
            }

        }

        private void picturehead_Click(object sender, EventArgs e)
        {
            OpenFileDialog fimage = new OpenFileDialog();
            fimage.InitialDirectory = (@"D:\专业\C#\myQQ\myQQ\Head");
            fimage.Filter = "图片|*.png";
            fimage.ShowDialog();
            string sql = string.Format(" update [dbo].[UserInfo] set headImage='{0}' where qq={1}", fimage.FileName.ToString(), FrmLogin.qq);
            int num = DatabaseHelper.executeNonQuery(sql);
            if (num > 0)
            {
                picturehead.BackgroundImage = Image.FromFile(fimage.FileName.ToString());
            }
        }
    }
}
