﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace myQQ
{
    public partial class FrmLogining : Form
    {
        public FrmLogining()
        {
            InitializeComponent();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {//退出
            Application.Exit();
        }

        private void btnChange_Click(object sender, EventArgs e)
        {//最小化
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnDesigine_Click(object sender, EventArgs e)
        {
            //转到设置窗体
            FrmDesiginer des = new FrmDesiginer();
            des.ShowDialog();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            //取消
            Application.Exit();
        }
       
        private void timer1_Tick(object sender, EventArgs e)
        {
            timerchange.Enabled = false;
            timer1.Enabled = false;
            logining.Visible = false;
            loging1.Visible = false;
            this.Visible = false;
            FrmMain main = new FrmMain();
            
                main.Show();
           
        }
        Point mouseOff;//鼠标移动位置变量
        bool leftFlag;//标记是否为左键

        private void FrmLogining_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                mouseOff = new Point(-e.X, -e.Y); //得到变量的值
                leftFlag = true;                  //点击左键按下时标注为true;
            }
        }

        private void FrmLogining_MouseMove(object sender, MouseEventArgs e)
        {
            if (leftFlag)
            {
                Point mouseSet = Control.MousePosition;
                mouseSet.Offset(mouseOff.X, mouseOff.Y);  //设置移动后的位置
                Location = mouseSet;
            }
        }

        private void FrmLogining_MouseUp(object sender, MouseEventArgs e)
        {
            if (leftFlag)
            {
                leftFlag = false;//释放鼠标后标注为false;
            }
        }

        private void FrmLogining_Load(object sender, EventArgs e)
        {
            this.picturehead.BackgroundImage = FrmLogin.head;
            this.Icon = logining.Icon;
            logining.Visible = true;
        }

        private void timerchange_Tick(object sender, EventArgs e)
        {
            if (logining.Visible==true)
            {
            logining.Visible = false;

                this.Icon = loging1.Icon;
                loging1.Visible = true;
            }
            else
            {
                logining.Visible =true;

                this.Icon = logining.Icon;
                loging1.Visible = false;

            }
        }
    }
}
