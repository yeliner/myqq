﻿namespace myQQ
{
    partial class FrmAddfriend
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbltishi = new System.Windows.Forms.Label();
            this.pgroup = new System.Windows.Forms.Panel();
            this.lbladdugrop = new System.Windows.Forms.Label();
            this.txtremark = new System.Windows.Forms.TextBox();
            this.btnCompelete = new System.Windows.Forms.Button();
            this.cbgroup = new System.Windows.Forms.ComboBox();
            this.lblremark = new System.Windows.Forms.Label();
            this.lblgroup = new System.Windows.Forms.Label();
            this.pleft = new System.Windows.Forms.Panel();
            this.lblasknews = new System.Windows.Forms.Label();
            this.txtask = new System.Windows.Forms.TextBox();
            this.puse = new System.Windows.Forms.Panel();
            this.lblsex = new System.Windows.Forms.Label();
            this.lblQQ = new System.Windows.Forms.Label();
            this.lblniname = new System.Windows.Forms.Label();
            this.picturehead = new System.Windows.Forms.PictureBox();
            this.lblAddress2 = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnChange = new System.Windows.Forms.Button();
            this.btnclose = new System.Windows.Forms.Button();
            this.btnnext = new System.Windows.Forms.Button();
            this.pgroup.SuspendLayout();
            this.pleft.SuspendLayout();
            this.puse.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picturehead)).BeginInit();
            this.SuspendLayout();
            // 
            // lbltishi
            // 
            this.lbltishi.AutoSize = true;
            this.lbltishi.BackColor = System.Drawing.Color.Transparent;
            this.lbltishi.Font = new System.Drawing.Font("幼圆", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbltishi.ForeColor = System.Drawing.Color.MediumBlue;
            this.lbltishi.Location = new System.Drawing.Point(176, 83);
            this.lbltishi.Name = "lbltishi";
            this.lbltishi.Size = new System.Drawing.Size(239, 76);
            this.lbltishi.TabIndex = 31;
            this.lbltishi.Text = "\r\n\r\n 很抱歉，没有找到该好友\r\n\r\n";
            this.lbltishi.Visible = false;
            // 
            // pgroup
            // 
            this.pgroup.Controls.Add(this.lbladdugrop);
            this.pgroup.Controls.Add(this.txtremark);
            this.pgroup.Controls.Add(this.btnCompelete);
            this.pgroup.Controls.Add(this.cbgroup);
            this.pgroup.Controls.Add(this.lblremark);
            this.pgroup.Controls.Add(this.lblgroup);
            this.pgroup.Location = new System.Drawing.Point(6, 35);
            this.pgroup.Name = "pgroup";
            this.pgroup.Size = new System.Drawing.Size(322, 171);
            this.pgroup.TabIndex = 31;
            this.pgroup.Visible = false;
            this.pgroup.MouseMove += new System.Windows.Forms.MouseEventHandler(this.FrmAddfriend_MouseMove);
            this.pgroup.MouseUp += new System.Windows.Forms.MouseEventHandler(this.FrmAddfriend_MouseUp);
            // 
            // lbladdugrop
            // 
            this.lbladdugrop.AutoSize = true;
            this.lbladdugrop.ForeColor = System.Drawing.Color.Blue;
            this.lbladdugrop.Location = new System.Drawing.Point(253, 107);
            this.lbladdugrop.Name = "lbladdugrop";
            this.lbladdugrop.Size = new System.Drawing.Size(53, 12);
            this.lbladdugrop.TabIndex = 41;
            this.lbladdugrop.Text = "新建分组";
            this.lbladdugrop.Click += new System.EventHandler(this.lbladdugrop_Click);
            // 
            // txtremark
            // 
            this.txtremark.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtremark.Location = new System.Drawing.Point(83, 64);
            this.txtremark.Name = "txtremark";
            this.txtremark.Size = new System.Drawing.Size(164, 21);
            this.txtremark.TabIndex = 37;
            // 
            // btnCompelete
            // 
            this.btnCompelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCompelete.ForeColor = System.Drawing.Color.Blue;
            this.btnCompelete.Location = new System.Drawing.Point(244, 143);
            this.btnCompelete.Name = "btnCompelete";
            this.btnCompelete.Size = new System.Drawing.Size(75, 23);
            this.btnCompelete.TabIndex = 40;
            this.btnCompelete.Text = "完成";
            this.btnCompelete.UseVisualStyleBackColor = true;
            this.btnCompelete.Click += new System.EventHandler(this.btnCompelete_Click);
            // 
            // cbgroup
            // 
            this.cbgroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbgroup.FormattingEnabled = true;
            this.cbgroup.Location = new System.Drawing.Point(83, 104);
            this.cbgroup.Name = "cbgroup";
            this.cbgroup.Size = new System.Drawing.Size(164, 20);
            this.cbgroup.TabIndex = 39;
            // 
            // lblremark
            // 
            this.lblremark.AutoSize = true;
            this.lblremark.BackColor = System.Drawing.Color.Transparent;
            this.lblremark.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblremark.ForeColor = System.Drawing.Color.Black;
            this.lblremark.Location = new System.Drawing.Point(10, 64);
            this.lblremark.Name = "lblremark";
            this.lblremark.Size = new System.Drawing.Size(79, 20);
            this.lblremark.TabIndex = 36;
            this.lblremark.Text = "备注姓名：";
            // 
            // lblgroup
            // 
            this.lblgroup.AutoSize = true;
            this.lblgroup.BackColor = System.Drawing.Color.Transparent;
            this.lblgroup.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblgroup.ForeColor = System.Drawing.Color.Black;
            this.lblgroup.Location = new System.Drawing.Point(10, 101);
            this.lblgroup.Name = "lblgroup";
            this.lblgroup.Size = new System.Drawing.Size(79, 20);
            this.lblgroup.TabIndex = 38;
            this.lblgroup.Text = "分       组：";
            // 
            // pleft
            // 
            this.pleft.BackColor = System.Drawing.Color.White;
            this.pleft.Controls.Add(this.pgroup);
            this.pleft.Controls.Add(this.lblasknews);
            this.pleft.Controls.Add(this.txtask);
            this.pleft.Location = new System.Drawing.Point(174, 34);
            this.pleft.Name = "pleft";
            this.pleft.Size = new System.Drawing.Size(328, 206);
            this.pleft.TabIndex = 34;
            this.pleft.Visible = false;
            this.pleft.MouseDown += new System.Windows.Forms.MouseEventHandler(this.FrmAddfriend_MouseDown);
            this.pleft.MouseMove += new System.Windows.Forms.MouseEventHandler(this.FrmAddfriend_MouseMove);
            this.pleft.MouseUp += new System.Windows.Forms.MouseEventHandler(this.FrmAddfriend_MouseUp);
            // 
            // lblasknews
            // 
            this.lblasknews.AutoSize = true;
            this.lblasknews.BackColor = System.Drawing.Color.Transparent;
            this.lblasknews.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblasknews.ForeColor = System.Drawing.Color.Black;
            this.lblasknews.Location = new System.Drawing.Point(3, 43);
            this.lblasknews.Name = "lblasknews";
            this.lblasknews.Size = new System.Drawing.Size(121, 20);
            this.lblasknews.TabIndex = 29;
            this.lblasknews.Text = "请输入验证消息：";
            // 
            // txtask
            // 
            this.txtask.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtask.Location = new System.Drawing.Point(7, 66);
            this.txtask.Multiline = true;
            this.txtask.Name = "txtask";
            this.txtask.Size = new System.Drawing.Size(286, 53);
            this.txtask.TabIndex = 30;
            // 
            // puse
            // 
            this.puse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(251)))));
            this.puse.Controls.Add(this.lblsex);
            this.puse.Controls.Add(this.lblQQ);
            this.puse.Controls.Add(this.lblniname);
            this.puse.Controls.Add(this.picturehead);
            this.puse.Controls.Add(this.lblAddress2);
            this.puse.Location = new System.Drawing.Point(26, 34);
            this.puse.Name = "puse";
            this.puse.Size = new System.Drawing.Size(148, 206);
            this.puse.TabIndex = 33;
            this.puse.Visible = false;
            this.puse.MouseDown += new System.Windows.Forms.MouseEventHandler(this.FrmAddfriend_MouseDown);
            // 
            // lblsex
            // 
            this.lblsex.AutoSize = true;
            this.lblsex.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblsex.ForeColor = System.Drawing.Color.Silver;
            this.lblsex.Location = new System.Drawing.Point(4, 137);
            this.lblsex.Name = "lblsex";
            this.lblsex.Size = new System.Drawing.Size(32, 17);
            this.lblsex.TabIndex = 46;
            this.lblsex.Text = "性别";
            // 
            // lblQQ
            // 
            this.lblQQ.AutoSize = true;
            this.lblQQ.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblQQ.ForeColor = System.Drawing.Color.Black;
            this.lblQQ.Location = new System.Drawing.Point(7, 117);
            this.lblQQ.Name = "lblQQ";
            this.lblQQ.Size = new System.Drawing.Size(29, 20);
            this.lblQQ.TabIndex = 45;
            this.lblQQ.Text = "qQ";
            // 
            // lblniname
            // 
            this.lblniname.AutoSize = true;
            this.lblniname.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblniname.ForeColor = System.Drawing.Color.Black;
            this.lblniname.Location = new System.Drawing.Point(7, 97);
            this.lblniname.Name = "lblniname";
            this.lblniname.Size = new System.Drawing.Size(29, 20);
            this.lblniname.TabIndex = 44;
            this.lblniname.Text = "nic";
            // 
            // picturehead
            // 
            this.picturehead.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.picturehead.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.picturehead.Location = new System.Drawing.Point(37, 32);
            this.picturehead.Name = "picturehead";
            this.picturehead.Size = new System.Drawing.Size(63, 62);
            this.picturehead.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picturehead.TabIndex = 43;
            this.picturehead.TabStop = false;
            // 
            // lblAddress2
            // 
            this.lblAddress2.AutoSize = true;
            this.lblAddress2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblAddress2.ForeColor = System.Drawing.Color.Silver;
            this.lblAddress2.Location = new System.Drawing.Point(4, 158);
            this.lblAddress2.Name = "lblAddress2";
            this.lblAddress2.Size = new System.Drawing.Size(32, 17);
            this.lblAddress2.TabIndex = 47;
            this.lblAddress2.Text = "地址";
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.Transparent;
            this.btnExit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnExit.FlatAppearance.BorderSize = 0;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Image = global::myQQ.Properties.Resources.退出;
            this.btnExit.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnExit.Location = new System.Drawing.Point(476, 2);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(26, 26);
            this.btnExit.TabIndex = 36;
            this.btnExit.UseCompatibleTextRendering = true;
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnChange
            // 
            this.btnChange.BackColor = System.Drawing.Color.Transparent;
            this.btnChange.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnChange.FlatAppearance.BorderSize = 0;
            this.btnChange.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnChange.Image = global::myQQ.Properties.Resources.最小化;
            this.btnChange.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnChange.Location = new System.Drawing.Point(444, 2);
            this.btnChange.Name = "btnChange";
            this.btnChange.Size = new System.Drawing.Size(26, 26);
            this.btnChange.TabIndex = 35;
            this.btnChange.UseCompatibleTextRendering = true;
            this.btnChange.UseVisualStyleBackColor = false;
            this.btnChange.Click += new System.EventHandler(this.btnChange_Click);
            // 
            // btnclose
            // 
            this.btnclose.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.btnclose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnclose.Location = new System.Drawing.Point(427, 246);
            this.btnclose.Name = "btnclose";
            this.btnclose.Size = new System.Drawing.Size(75, 23);
            this.btnclose.TabIndex = 38;
            this.btnclose.Text = "关闭";
            this.btnclose.UseVisualStyleBackColor = true;
            this.btnclose.Click += new System.EventHandler(this.btnclose_Click);
            // 
            // btnnext
            // 
            this.btnnext.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.btnnext.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnnext.Location = new System.Drawing.Point(346, 246);
            this.btnnext.Name = "btnnext";
            this.btnnext.Size = new System.Drawing.Size(75, 23);
            this.btnnext.TabIndex = 37;
            this.btnnext.Text = "下一步";
            this.btnnext.UseVisualStyleBackColor = true;
            this.btnnext.Click += new System.EventHandler(this.btnnext_Click);
            // 
            // FrmAddfriend
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(234)))), ((int)(((byte)(246)))));
            this.ClientSize = new System.Drawing.Size(530, 272);
            this.Controls.Add(this.btnclose);
            this.Controls.Add(this.btnnext);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnChange);
            this.Controls.Add(this.pleft);
            this.Controls.Add(this.puse);
            this.Controls.Add(this.lbltishi);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmAddfriend";
            this.Text = "FrmAdd";
            this.Load += new System.EventHandler(this.FrmAdd_Load);
            this.pgroup.ResumeLayout(false);
            this.pgroup.PerformLayout();
            this.pleft.ResumeLayout(false);
            this.pleft.PerformLayout();
            this.puse.ResumeLayout(false);
            this.puse.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picturehead)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbltishi;
        private System.Windows.Forms.Panel pgroup;
        private System.Windows.Forms.Label lbladdugrop;
        private System.Windows.Forms.TextBox txtremark;
        private System.Windows.Forms.Button btnCompelete;
        private System.Windows.Forms.ComboBox cbgroup;
        private System.Windows.Forms.Label lblremark;
        private System.Windows.Forms.Label lblgroup;
        private System.Windows.Forms.Panel pleft;
        private System.Windows.Forms.Label lblasknews;
        private System.Windows.Forms.TextBox txtask;
        private System.Windows.Forms.Panel puse;
        private System.Windows.Forms.Label lblsex;
        private System.Windows.Forms.Label lblQQ;
        private System.Windows.Forms.Label lblniname;
        private System.Windows.Forms.PictureBox picturehead;
        private System.Windows.Forms.Label lblAddress2;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnChange;
        private System.Windows.Forms.Button btnclose;
        private System.Windows.Forms.Button btnnext;
    }
}