﻿namespace myQQ
{
    partial class FrmLogining
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmLogining));
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnChange = new System.Windows.Forms.Button();
            this.btnDesigine = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.picturehead = new System.Windows.Forms.PictureBox();
            this.picturegif = new System.Windows.Forms.PictureBox();
            this.logining = new System.Windows.Forms.NotifyIcon(this.components);
            this.timerchange = new System.Windows.Forms.Timer(this.components);
            this.loging1 = new System.Windows.Forms.NotifyIcon(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.picturehead)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picturegif)).BeginInit();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 2000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // btnEdit
            // 
            this.btnEdit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(124)))), ((int)(((byte)(210)))));
            this.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnEdit.FlatAppearance.BorderSize = 0;
            this.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEdit.Image = global::myQQ.Properties.Resources.退出;
            this.btnEdit.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnEdit.Location = new System.Drawing.Point(403, -1);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(29, 26);
            this.btnEdit.TabIndex = 20;
            this.btnEdit.UseCompatibleTextRendering = true;
            this.btnEdit.UseVisualStyleBackColor = false;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnChange
            // 
            this.btnChange.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(124)))), ((int)(((byte)(210)))));
            this.btnChange.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnChange.FlatAppearance.BorderSize = 0;
            this.btnChange.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnChange.Image = global::myQQ.Properties.Resources.最小化;
            this.btnChange.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnChange.Location = new System.Drawing.Point(377, -1);
            this.btnChange.Name = "btnChange";
            this.btnChange.Size = new System.Drawing.Size(26, 26);
            this.btnChange.TabIndex = 19;
            this.btnChange.UseCompatibleTextRendering = true;
            this.btnChange.UseVisualStyleBackColor = false;
            this.btnChange.Click += new System.EventHandler(this.btnChange_Click);
            // 
            // btnDesigine
            // 
            this.btnDesigine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(124)))), ((int)(((byte)(210)))));
            this.btnDesigine.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnDesigine.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btnDesigine.FlatAppearance.BorderSize = 0;
            this.btnDesigine.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btnDesigine.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btnDesigine.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDesigine.Image = global::myQQ.Properties.Resources.设置图标;
            this.btnDesigine.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnDesigine.Location = new System.Drawing.Point(351, -1);
            this.btnDesigine.Name = "btnDesigine";
            this.btnDesigine.Size = new System.Drawing.Size(26, 26);
            this.btnDesigine.TabIndex = 18;
            this.btnDesigine.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnDesigine.UseCompatibleTextRendering = true;
            this.btnDesigine.UseVisualStyleBackColor = false;
            this.btnDesigine.Click += new System.EventHandler(this.btnDesigine_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnCancel.Cursor = System.Windows.Forms.Cursors.AppStarting;
            this.btnCancel.FlatAppearance.BorderSize = 0;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCancel.Font = new System.Drawing.Font("宋体", 10.5F);
            this.btnCancel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnCancel.Location = new System.Drawing.Point(118, 291);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(196, 33);
            this.btnCancel.TabIndex = 17;
            this.btnCancel.Text = "取消";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // picturehead
            // 
            this.picturehead.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.picturehead.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.picturehead.Location = new System.Drawing.Point(173, 193);
            this.picturehead.Name = "picturehead";
            this.picturehead.Size = new System.Drawing.Size(84, 83);
            this.picturehead.TabIndex = 16;
            this.picturehead.TabStop = false;
            // 
            // picturegif
            // 
            this.picturegif.Image = ((System.Drawing.Image)(resources.GetObject("picturegif.Image")));
            this.picturegif.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.picturegif.Location = new System.Drawing.Point(1, 0);
            this.picturegif.Name = "picturegif";
            this.picturegif.Size = new System.Drawing.Size(431, 181);
            this.picturegif.TabIndex = 15;
            this.picturegif.TabStop = false;
            this.picturegif.MouseDown += new System.Windows.Forms.MouseEventHandler(this.FrmLogining_MouseDown);
            this.picturegif.MouseMove += new System.Windows.Forms.MouseEventHandler(this.FrmLogining_MouseMove);
            this.picturegif.MouseUp += new System.Windows.Forms.MouseEventHandler(this.FrmLogining_MouseUp);
            // 
            // logining
            // 
            this.logining.Icon = ((System.Drawing.Icon)(resources.GetObject("logining.Icon")));
            this.logining.Text = "myQQ";
            this.logining.Visible = true;
            // 
            // timerchange
            // 
            this.timerchange.Enabled = true;
            this.timerchange.Interval = 500;
            this.timerchange.Tick += new System.EventHandler(this.timerchange_Tick);
            // 
            // loging1
            // 
            this.loging1.Icon = ((System.Drawing.Icon)(resources.GetObject("loging1.Icon")));
            this.loging1.Text = "myQQ";
            this.loging1.Visible = true;
            // 
            // FrmLogining
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(235)))), ((int)(((byte)(253)))));
            this.ClientSize = new System.Drawing.Size(432, 326);
            this.Controls.Add(this.btnEdit);
            this.Controls.Add(this.btnChange);
            this.Controls.Add(this.btnDesigine);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.picturehead);
            this.Controls.Add(this.picturegif);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmLogining";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmLogining";
            this.Load += new System.EventHandler(this.FrmLogining_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.FrmLogining_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.FrmLogining_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.FrmLogining_MouseUp);
            ((System.ComponentModel.ISupportInitialize)(this.picturehead)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picturegif)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Button btnChange;
        private System.Windows.Forms.Button btnDesigine;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.PictureBox picturehead;
        private System.Windows.Forms.PictureBox picturegif;
        private System.Windows.Forms.NotifyIcon logining;
        private System.Windows.Forms.Timer timerchange;
        private System.Windows.Forms.NotifyIcon loging1;
    }
}