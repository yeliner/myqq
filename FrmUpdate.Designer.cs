﻿namespace myQQ
{
    partial class FrmUpdate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnsure = new System.Windows.Forms.Button();
            this.txtadd = new System.Windows.Forms.TextBox();
            this.btncancel = new System.Windows.Forms.Button();
            this.lbladdgroup = new System.Windows.Forms.Label();
            this.paddgroup = new System.Windows.Forms.Panel();
            this.cbdeletegname = new System.Windows.Forms.ComboBox();
            this.lbldeleteg = new System.Windows.Forms.Label();
            this.pdeleteg = new System.Windows.Forms.Panel();
            this.lblmovetishi = new System.Windows.Forms.Label();
            this.lbldegtishi = new System.Windows.Forms.Label();
            this.btncanceldeg = new System.Windows.Forms.Button();
            this.btndeleteg = new System.Windows.Forms.Button();
            this.lblmoveg = new System.Windows.Forms.Label();
            this.cbmoveg = new System.Windows.Forms.ComboBox();
            this.btnExit = new System.Windows.Forms.Button();
            this.pReg = new System.Windows.Forms.Panel();
            this.txtnewname = new System.Windows.Forms.TextBox();
            this.lblregtishi2 = new System.Windows.Forms.Label();
            this.lblregtishi = new System.Windows.Forms.Label();
            this.btnrecancel = new System.Windows.Forms.Button();
            this.btnReok = new System.Windows.Forms.Button();
            this.lblreg2 = new System.Windows.Forms.Label();
            this.lblreg = new System.Windows.Forms.Label();
            this.cbReg = new System.Windows.Forms.ComboBox();
            this.pRefriend = new System.Windows.Forms.Panel();
            this.btnRefriok = new System.Windows.Forms.Button();
            this.txtrefriend = new System.Windows.Forms.TextBox();
            this.btnRefricancel = new System.Windows.Forms.Button();
            this.pmovefri = new System.Windows.Forms.Panel();
            this.btnmfricancel = new System.Windows.Forms.Button();
            this.btnmfriok = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.cbmovefri = new System.Windows.Forms.ComboBox();
            this.paddgroup.SuspendLayout();
            this.pdeleteg.SuspendLayout();
            this.pReg.SuspendLayout();
            this.pRefriend.SuspendLayout();
            this.pmovefri.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnsure
            // 
            this.btnsure.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnsure.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnsure.Font = new System.Drawing.Font("华文宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnsure.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnsure.Location = new System.Drawing.Point(173, 38);
            this.btnsure.Name = "btnsure";
            this.btnsure.Size = new System.Drawing.Size(48, 28);
            this.btnsure.TabIndex = 6;
            this.btnsure.Text = "确定";
            this.btnsure.UseVisualStyleBackColor = true;
            this.btnsure.Visible = false;
            this.btnsure.Click += new System.EventHandler(this.btnsure_Click);
            // 
            // txtadd
            // 
            this.txtadd.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtadd.Font = new System.Drawing.Font("华文楷体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtadd.ForeColor = System.Drawing.Color.Black;
            this.txtadd.Location = new System.Drawing.Point(15, 38);
            this.txtadd.Name = "txtadd";
            this.txtadd.Size = new System.Drawing.Size(161, 28);
            this.txtadd.TabIndex = 5;
            this.txtadd.TextChanged += new System.EventHandler(this.txtadd_TextChanged);
            // 
            // btncancel
            // 
            this.btncancel.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btncancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btncancel.Font = new System.Drawing.Font("华文宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btncancel.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.btncancel.Location = new System.Drawing.Point(173, 38);
            this.btncancel.Name = "btncancel";
            this.btncancel.Size = new System.Drawing.Size(48, 28);
            this.btncancel.TabIndex = 7;
            this.btncancel.Text = "取消";
            this.btncancel.UseVisualStyleBackColor = true;
            this.btncancel.Click += new System.EventHandler(this.btncancel_Click);
            // 
            // lbladdgroup
            // 
            this.lbladdgroup.AutoSize = true;
            this.lbladdgroup.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.lbladdgroup.Location = new System.Drawing.Point(13, 23);
            this.lbladdgroup.Name = "lbladdgroup";
            this.lbladdgroup.Size = new System.Drawing.Size(77, 12);
            this.lbladdgroup.TabIndex = 8;
            this.lbladdgroup.Text = "请输入分组名";
            // 
            // paddgroup
            // 
            this.paddgroup.Controls.Add(this.lbladdgroup);
            this.paddgroup.Controls.Add(this.btnsure);
            this.paddgroup.Controls.Add(this.txtadd);
            this.paddgroup.Controls.Add(this.btncancel);
            this.paddgroup.Location = new System.Drawing.Point(72, 89);
            this.paddgroup.Name = "paddgroup";
            this.paddgroup.Size = new System.Drawing.Size(237, 95);
            this.paddgroup.TabIndex = 9;
            // 
            // cbdeletegname
            // 
            this.cbdeletegname.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cbdeletegname.FormattingEnabled = true;
            this.cbdeletegname.Location = new System.Drawing.Point(55, 43);
            this.cbdeletegname.Name = "cbdeletegname";
            this.cbdeletegname.Size = new System.Drawing.Size(176, 28);
            this.cbdeletegname.TabIndex = 10;
            this.cbdeletegname.TextChanged += new System.EventHandler(this.cbdeletegname_TextChanged);
            // 
            // lbldeleteg
            // 
            this.lbldeleteg.AutoSize = true;
            this.lbldeleteg.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbldeleteg.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.lbldeleteg.Location = new System.Drawing.Point(53, 19);
            this.lbldeleteg.Name = "lbldeleteg";
            this.lbldeleteg.Size = new System.Drawing.Size(135, 20);
            this.lbldeleteg.TabIndex = 11;
            this.lbldeleteg.Text = "请选择要删除的分组";
            // 
            // pdeleteg
            // 
            this.pdeleteg.Controls.Add(this.lblmovetishi);
            this.pdeleteg.Controls.Add(this.lbldegtishi);
            this.pdeleteg.Controls.Add(this.btncanceldeg);
            this.pdeleteg.Controls.Add(this.btndeleteg);
            this.pdeleteg.Controls.Add(this.lblmoveg);
            this.pdeleteg.Controls.Add(this.cbmoveg);
            this.pdeleteg.Controls.Add(this.lbldeleteg);
            this.pdeleteg.Controls.Add(this.cbdeletegname);
            this.pdeleteg.Location = new System.Drawing.Point(58, 47);
            this.pdeleteg.Name = "pdeleteg";
            this.pdeleteg.Size = new System.Drawing.Size(294, 190);
            this.pdeleteg.TabIndex = 12;
            this.pdeleteg.Visible = false;
            // 
            // lblmovetishi
            // 
            this.lblmovetishi.AutoSize = true;
            this.lblmovetishi.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblmovetishi.ForeColor = System.Drawing.Color.Red;
            this.lblmovetishi.Location = new System.Drawing.Point(55, 140);
            this.lblmovetishi.Name = "lblmovetishi";
            this.lblmovetishi.Size = new System.Drawing.Size(125, 12);
            this.lblmovetishi.TabIndex = 17;
            this.lblmovetishi.Text = "未选择要移动的分组！";
            this.lblmovetishi.Visible = false;
            // 
            // lbldegtishi
            // 
            this.lbldegtishi.AutoSize = true;
            this.lbldegtishi.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbldegtishi.ForeColor = System.Drawing.Color.Red;
            this.lbldegtishi.Location = new System.Drawing.Point(55, 72);
            this.lbldegtishi.Name = "lbldegtishi";
            this.lbldegtishi.Size = new System.Drawing.Size(125, 12);
            this.lbldegtishi.TabIndex = 16;
            this.lbldegtishi.Text = "未选择要删除的分组！";
            this.lbldegtishi.Visible = false;
            // 
            // btncanceldeg
            // 
            this.btncanceldeg.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btncanceldeg.FlatAppearance.BorderSize = 0;
            this.btncanceldeg.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btncanceldeg.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btncanceldeg.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.btncanceldeg.Location = new System.Drawing.Point(234, 159);
            this.btncanceldeg.Name = "btncanceldeg";
            this.btncanceldeg.Size = new System.Drawing.Size(48, 28);
            this.btncanceldeg.TabIndex = 15;
            this.btncanceldeg.Text = "取消";
            this.btncanceldeg.UseVisualStyleBackColor = true;
            // 
            // btndeleteg
            // 
            this.btndeleteg.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btndeleteg.FlatAppearance.BorderSize = 0;
            this.btndeleteg.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btndeleteg.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btndeleteg.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.btndeleteg.Location = new System.Drawing.Point(179, 159);
            this.btndeleteg.Name = "btndeleteg";
            this.btndeleteg.Size = new System.Drawing.Size(52, 28);
            this.btndeleteg.TabIndex = 14;
            this.btndeleteg.Text = "确定";
            this.btndeleteg.UseVisualStyleBackColor = true;
            this.btndeleteg.Click += new System.EventHandler(this.btndegok_Click);
            // 
            // lblmoveg
            // 
            this.lblmoveg.AutoSize = true;
            this.lblmoveg.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblmoveg.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.lblmoveg.Location = new System.Drawing.Point(53, 88);
            this.lblmoveg.Name = "lblmoveg";
            this.lblmoveg.Size = new System.Drawing.Size(149, 20);
            this.lblmoveg.TabIndex = 13;
            this.lblmoveg.Text = "请选择好友移至的分组";
            // 
            // cbmoveg
            // 
            this.cbmoveg.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cbmoveg.FormattingEnabled = true;
            this.cbmoveg.Location = new System.Drawing.Point(57, 111);
            this.cbmoveg.Name = "cbmoveg";
            this.cbmoveg.Size = new System.Drawing.Size(176, 28);
            this.cbmoveg.TabIndex = 12;
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.Transparent;
            this.btnExit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnExit.FlatAppearance.BorderSize = 0;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Image = global::myQQ.Properties.Resources.退出;
            this.btnExit.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnExit.Location = new System.Drawing.Point(342, 3);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(26, 26);
            this.btnExit.TabIndex = 37;
            this.btnExit.UseCompatibleTextRendering = true;
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // pReg
            // 
            this.pReg.Controls.Add(this.txtnewname);
            this.pReg.Controls.Add(this.lblregtishi2);
            this.pReg.Controls.Add(this.lblregtishi);
            this.pReg.Controls.Add(this.btnrecancel);
            this.pReg.Controls.Add(this.btnReok);
            this.pReg.Controls.Add(this.lblreg2);
            this.pReg.Controls.Add(this.lblreg);
            this.pReg.Controls.Add(this.cbReg);
            this.pReg.Location = new System.Drawing.Point(49, 35);
            this.pReg.Name = "pReg";
            this.pReg.Size = new System.Drawing.Size(319, 208);
            this.pReg.TabIndex = 38;
            this.pReg.Visible = false;
            // 
            // txtnewname
            // 
            this.txtnewname.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.txtnewname.Location = new System.Drawing.Point(35, 93);
            this.txtnewname.Name = "txtnewname";
            this.txtnewname.Size = new System.Drawing.Size(174, 26);
            this.txtnewname.TabIndex = 26;
            // 
            // lblregtishi2
            // 
            this.lblregtishi2.AutoSize = true;
            this.lblregtishi2.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblregtishi2.ForeColor = System.Drawing.Color.Red;
            this.lblregtishi2.Location = new System.Drawing.Point(33, 122);
            this.lblregtishi2.Name = "lblregtishi2";
            this.lblregtishi2.Size = new System.Drawing.Size(101, 12);
            this.lblregtishi2.TabIndex = 25;
            this.lblregtishi2.Text = "分组名不能为空！";
            this.lblregtishi2.Visible = false;
            // 
            // lblregtishi
            // 
            this.lblregtishi.AutoSize = true;
            this.lblregtishi.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblregtishi.ForeColor = System.Drawing.Color.Red;
            this.lblregtishi.Location = new System.Drawing.Point(33, 54);
            this.lblregtishi.Name = "lblregtishi";
            this.lblregtishi.Size = new System.Drawing.Size(137, 12);
            this.lblregtishi.TabIndex = 24;
            this.lblregtishi.Text = "未选择要重命名的分组！";
            this.lblregtishi.Visible = false;
            // 
            // btnrecancel
            // 
            this.btnrecancel.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnrecancel.FlatAppearance.BorderSize = 0;
            this.btnrecancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnrecancel.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnrecancel.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.btnrecancel.Location = new System.Drawing.Point(212, 141);
            this.btnrecancel.Name = "btnrecancel";
            this.btnrecancel.Size = new System.Drawing.Size(48, 28);
            this.btnrecancel.TabIndex = 23;
            this.btnrecancel.Text = "取消";
            this.btnrecancel.UseVisualStyleBackColor = true;
            this.btnrecancel.Click += new System.EventHandler(this.btncancel_Click);
            // 
            // btnReok
            // 
            this.btnReok.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnReok.FlatAppearance.BorderSize = 0;
            this.btnReok.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReok.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnReok.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.btnReok.Location = new System.Drawing.Point(157, 141);
            this.btnReok.Name = "btnReok";
            this.btnReok.Size = new System.Drawing.Size(52, 28);
            this.btnReok.TabIndex = 22;
            this.btnReok.Text = "确定";
            this.btnReok.UseVisualStyleBackColor = true;
            this.btnReok.Click += new System.EventHandler(this.btnReok_Click);
            // 
            // lblreg2
            // 
            this.lblreg2.AutoSize = true;
            this.lblreg2.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblreg2.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.lblreg2.Location = new System.Drawing.Point(31, 70);
            this.lblreg2.Name = "lblreg2";
            this.lblreg2.Size = new System.Drawing.Size(135, 20);
            this.lblreg2.TabIndex = 21;
            this.lblreg2.Text = "请输入该分组新名称";
            // 
            // lblreg
            // 
            this.lblreg.AutoSize = true;
            this.lblreg.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblreg.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.lblreg.Location = new System.Drawing.Point(31, 1);
            this.lblreg.Name = "lblreg";
            this.lblreg.Size = new System.Drawing.Size(149, 20);
            this.lblreg.TabIndex = 19;
            this.lblreg.Text = "请选择要重命名的分组";
            // 
            // cbReg
            // 
            this.cbReg.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cbReg.FormattingEnabled = true;
            this.cbReg.Location = new System.Drawing.Point(33, 25);
            this.cbReg.Name = "cbReg";
            this.cbReg.Size = new System.Drawing.Size(176, 28);
            this.cbReg.TabIndex = 18;
            // 
            // pRefriend
            // 
            this.pRefriend.Controls.Add(this.btnRefriok);
            this.pRefriend.Controls.Add(this.txtrefriend);
            this.pRefriend.Controls.Add(this.btnRefricancel);
            this.pRefriend.Location = new System.Drawing.Point(27, 97);
            this.pRefriend.Name = "pRefriend";
            this.pRefriend.Size = new System.Drawing.Size(348, 68);
            this.pRefriend.TabIndex = 40;
            this.pRefriend.Visible = false;
            // 
            // btnRefriok
            // 
            this.btnRefriok.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnRefriok.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRefriok.Font = new System.Drawing.Font("华文宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnRefriok.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnRefriok.Location = new System.Drawing.Point(217, 15);
            this.btnRefriok.Name = "btnRefriok";
            this.btnRefriok.Size = new System.Drawing.Size(48, 28);
            this.btnRefriok.TabIndex = 10;
            this.btnRefriok.Text = "确定";
            this.btnRefriok.UseVisualStyleBackColor = true;
            this.btnRefriok.Visible = false;
            this.btnRefriok.Click += new System.EventHandler(this.btnRefriok_Click);
            // 
            // txtrefriend
            // 
            this.txtrefriend.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtrefriend.Font = new System.Drawing.Font("华文楷体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtrefriend.ForeColor = System.Drawing.Color.Black;
            this.txtrefriend.Location = new System.Drawing.Point(60, 15);
            this.txtrefriend.Name = "txtrefriend";
            this.txtrefriend.Size = new System.Drawing.Size(161, 28);
            this.txtrefriend.TabIndex = 9;
            this.txtrefriend.TextChanged += new System.EventHandler(this.txtrefriend_TextChanged);
            // 
            // btnRefricancel
            // 
            this.btnRefricancel.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnRefricancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRefricancel.Font = new System.Drawing.Font("华文宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnRefricancel.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnRefricancel.Location = new System.Drawing.Point(218, 15);
            this.btnRefricancel.Name = "btnRefricancel";
            this.btnRefricancel.Size = new System.Drawing.Size(48, 28);
            this.btnRefricancel.TabIndex = 11;
            this.btnRefricancel.Text = "取消";
            this.btnRefricancel.UseVisualStyleBackColor = true;
            this.btnRefricancel.Click += new System.EventHandler(this.btncancel_Click);
            // 
            // pmovefri
            // 
            this.pmovefri.Controls.Add(this.btnmfricancel);
            this.pmovefri.Controls.Add(this.btnmfriok);
            this.pmovefri.Controls.Add(this.label1);
            this.pmovefri.Controls.Add(this.cbmovefri);
            this.pmovefri.Location = new System.Drawing.Point(27, 35);
            this.pmovefri.Name = "pmovefri";
            this.pmovefri.Size = new System.Drawing.Size(356, 220);
            this.pmovefri.TabIndex = 12;
            this.pmovefri.Visible = false;
            // 
            // btnmfricancel
            // 
            this.btnmfricancel.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnmfricancel.FlatAppearance.BorderSize = 0;
            this.btnmfricancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnmfricancel.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnmfricancel.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.btnmfricancel.Location = new System.Drawing.Point(225, 121);
            this.btnmfricancel.Name = "btnmfricancel";
            this.btnmfricancel.Size = new System.Drawing.Size(48, 28);
            this.btnmfricancel.TabIndex = 25;
            this.btnmfricancel.Text = "取消";
            this.btnmfricancel.UseVisualStyleBackColor = true;
            this.btnmfricancel.Click += new System.EventHandler(this.btncancel_Click);
            // 
            // btnmfriok
            // 
            this.btnmfriok.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnmfriok.FlatAppearance.BorderSize = 0;
            this.btnmfriok.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnmfriok.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnmfriok.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.btnmfriok.Location = new System.Drawing.Point(170, 121);
            this.btnmfriok.Name = "btnmfriok";
            this.btnmfriok.Size = new System.Drawing.Size(52, 28);
            this.btnmfriok.TabIndex = 24;
            this.btnmfriok.Text = "确定";
            this.btnmfriok.UseVisualStyleBackColor = true;
            this.btnmfriok.Click += new System.EventHandler(this.btnmfriok_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.label1.Location = new System.Drawing.Point(73, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(135, 20);
            this.label1.TabIndex = 21;
            this.label1.Text = "请选择要移至的分组";
            // 
            // cbmovefri
            // 
            this.cbmovefri.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cbmovefri.FormattingEnabled = true;
            this.cbmovefri.Location = new System.Drawing.Point(75, 72);
            this.cbmovefri.Name = "cbmovefri";
            this.cbmovefri.Size = new System.Drawing.Size(176, 28);
            this.cbmovefri.TabIndex = 20;
            // 
            // FrmUpdate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(246)))), ((int)(((byte)(251)))));
            this.ClientSize = new System.Drawing.Size(390, 270);
            this.Controls.Add(this.pmovefri);
            this.Controls.Add(this.pRefriend);
            this.Controls.Add(this.pReg);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.pdeleteg);
            this.Controls.Add(this.paddgroup);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmUpdate";
            this.Text = "FrmUpdate";
            this.Load += new System.EventHandler(this.FrmUpdate_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.FrmUpdate_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.FrmUpdate_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.FrmUpdate_MouseUp);
            this.paddgroup.ResumeLayout(false);
            this.paddgroup.PerformLayout();
            this.pdeleteg.ResumeLayout(false);
            this.pdeleteg.PerformLayout();
            this.pReg.ResumeLayout(false);
            this.pReg.PerformLayout();
            this.pRefriend.ResumeLayout(false);
            this.pRefriend.PerformLayout();
            this.pmovefri.ResumeLayout(false);
            this.pmovefri.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnsure;
        private System.Windows.Forms.TextBox txtadd;
        private System.Windows.Forms.Button btncancel;
        private System.Windows.Forms.Label lbladdgroup;
        private System.Windows.Forms.Panel paddgroup;
        private System.Windows.Forms.ComboBox cbdeletegname;
        private System.Windows.Forms.Label lbldeleteg;
        private System.Windows.Forms.Panel pdeleteg;
        private System.Windows.Forms.Label lblmoveg;
        private System.Windows.Forms.ComboBox cbmoveg;
        private System.Windows.Forms.Button btncanceldeg;
        private System.Windows.Forms.Button btndeleteg;
        private System.Windows.Forms.Label lblmovetishi;
        private System.Windows.Forms.Label lbldegtishi;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Panel pReg;
        private System.Windows.Forms.TextBox txtnewname;
        private System.Windows.Forms.Label lblregtishi2;
        private System.Windows.Forms.Label lblregtishi;
        private System.Windows.Forms.Button btnrecancel;
        private System.Windows.Forms.Button btnReok;
        private System.Windows.Forms.Label lblreg2;
        private System.Windows.Forms.Label lblreg;
        private System.Windows.Forms.ComboBox cbReg;
        private System.Windows.Forms.Panel pRefriend;
        private System.Windows.Forms.Button btnRefriok;
        private System.Windows.Forms.TextBox txtrefriend;
        private System.Windows.Forms.Button btnRefricancel;
        private System.Windows.Forms.Panel pmovefri;
        private System.Windows.Forms.Button btnmfricancel;
        private System.Windows.Forms.Button btnmfriok;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbmovefri;
    }
}