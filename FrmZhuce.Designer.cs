﻿namespace myQQ
{
    partial class FrmZhuce
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblsure = new System.Windows.Forms.Label();
            this.lbltishipwd = new System.Windows.Forms.Label();
            this.lblniname = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.lblAddress = new System.Windows.Forms.Label();
            this.dtpbrith = new System.Windows.Forms.DateTimePicker();
            this.lblBrith = new System.Windows.Forms.Label();
            this.rbogirl = new System.Windows.Forms.RadioButton();
            this.rdoBoy = new System.Windows.Forms.RadioButton();
            this.lblSex = new System.Windows.Forms.Label();
            this.txtpwdagin = new System.Windows.Forms.TextBox();
            this.lblpwdagin = new System.Windows.Forms.Label();
            this.txtPwd = new System.Windows.Forms.TextBox();
            this.lblpwd = new System.Windows.Forms.Label();
            this.txtNiname = new System.Windows.Forms.TextBox();
            this.lblni = new System.Windows.Forms.Label();
            this.lblZhuce = new System.Windows.Forms.Label();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.lblsure);
            this.panel2.Controls.Add(this.lbltishipwd);
            this.panel2.Controls.Add(this.lblniname);
            this.panel2.Location = new System.Drawing.Point(360, 89);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(193, 160);
            this.panel2.TabIndex = 41;
            // 
            // lblsure
            // 
            this.lblsure.AutoSize = true;
            this.lblsure.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblsure.ForeColor = System.Drawing.Color.Red;
            this.lblsure.Location = new System.Drawing.Point(17, 124);
            this.lblsure.Name = "lblsure";
            this.lblsure.Size = new System.Drawing.Size(0, 14);
            this.lblsure.TabIndex = 3;
            // 
            // lbltishipwd
            // 
            this.lbltishipwd.AutoSize = true;
            this.lbltishipwd.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbltishipwd.ForeColor = System.Drawing.Color.Red;
            this.lbltishipwd.Location = new System.Drawing.Point(17, 71);
            this.lbltishipwd.Name = "lbltishipwd";
            this.lbltishipwd.Size = new System.Drawing.Size(0, 14);
            this.lbltishipwd.TabIndex = 1;
            // 
            // lblniname
            // 
            this.lblniname.AutoSize = true;
            this.lblniname.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblniname.ForeColor = System.Drawing.Color.Red;
            this.lblniname.Location = new System.Drawing.Point(17, 13);
            this.lblniname.Name = "lblniname";
            this.lblniname.Size = new System.Drawing.Size(0, 14);
            this.lblniname.TabIndex = 0;
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(235)))), ((int)(((byte)(253)))));
            this.btnExit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnExit.FlatAppearance.BorderSize = 0;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Image = global::myQQ.Properties.Resources.退出;
            this.btnExit.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnExit.Location = new System.Drawing.Point(524, 3);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(29, 26);
            this.btnExit.TabIndex = 40;
            this.btnExit.UseCompatibleTextRendering = true;
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnSubmit
            // 
            this.btnSubmit.BackgroundImage = global::myQQ.Properties.Resources.提交注册;
            this.btnSubmit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSubmit.FlatAppearance.BorderSize = 0;
            this.btnSubmit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSubmit.Location = new System.Drawing.Point(99, 392);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(255, 51);
            this.btnSubmit.TabIndex = 39;
            this.btnSubmit.UseVisualStyleBackColor = true;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // txtAddress
            // 
            this.txtAddress.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtAddress.Location = new System.Drawing.Point(99, 332);
            this.txtAddress.MaxLength = 100;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(255, 26);
            this.txtAddress.TabIndex = 38;
            // 
            // lblAddress
            // 
            this.lblAddress.AutoSize = true;
            this.lblAddress.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblAddress.Location = new System.Drawing.Point(58, 337);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(35, 14);
            this.lblAddress.TabIndex = 37;
            this.lblAddress.Text = "地址";
            // 
            // dtpbrith
            // 
            this.dtpbrith.AllowDrop = true;
            this.dtpbrith.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.dtpbrith.Location = new System.Drawing.Point(99, 290);
            this.dtpbrith.Name = "dtpbrith";
            this.dtpbrith.Size = new System.Drawing.Size(174, 26);
            this.dtpbrith.TabIndex = 36;
            this.dtpbrith.Value = new System.DateTime(2016, 4, 22, 15, 6, 18, 0);
            // 
            // lblBrith
            // 
            this.lblBrith.AutoSize = true;
            this.lblBrith.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblBrith.Location = new System.Drawing.Point(58, 294);
            this.lblBrith.Name = "lblBrith";
            this.lblBrith.Size = new System.Drawing.Size(35, 14);
            this.lblBrith.TabIndex = 35;
            this.lblBrith.Text = "生日";
            // 
            // rbogirl
            // 
            this.rbogirl.AutoSize = true;
            this.rbogirl.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.rbogirl.Location = new System.Drawing.Point(170, 252);
            this.rbogirl.Name = "rbogirl";
            this.rbogirl.Size = new System.Drawing.Size(42, 20);
            this.rbogirl.TabIndex = 34;
            this.rbogirl.TabStop = true;
            this.rbogirl.Text = "女";
            this.rbogirl.UseVisualStyleBackColor = true;
            // 
            // rdoBoy
            // 
            this.rdoBoy.AutoSize = true;
            this.rdoBoy.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.rdoBoy.Checked = true;
            this.rdoBoy.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.rdoBoy.Location = new System.Drawing.Point(99, 254);
            this.rdoBoy.Name = "rdoBoy";
            this.rdoBoy.Size = new System.Drawing.Size(42, 20);
            this.rdoBoy.TabIndex = 33;
            this.rdoBoy.TabStop = true;
            this.rdoBoy.Text = "男";
            this.rdoBoy.UseVisualStyleBackColor = false;
            // 
            // lblSex
            // 
            this.lblSex.AutoSize = true;
            this.lblSex.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblSex.Location = new System.Drawing.Point(58, 254);
            this.lblSex.Name = "lblSex";
            this.lblSex.Size = new System.Drawing.Size(35, 14);
            this.lblSex.TabIndex = 32;
            this.lblSex.Text = "性别";
            // 
            // txtpwdagin
            // 
            this.txtpwdagin.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtpwdagin.Location = new System.Drawing.Point(99, 195);
            this.txtpwdagin.MaxLength = 16;
            this.txtpwdagin.Name = "txtpwdagin";
            this.txtpwdagin.PasswordChar = '●';
            this.txtpwdagin.Size = new System.Drawing.Size(255, 26);
            this.txtpwdagin.TabIndex = 31;
            // 
            // lblpwdagin
            // 
            this.lblpwdagin.AutoSize = true;
            this.lblpwdagin.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblpwdagin.Location = new System.Drawing.Point(30, 205);
            this.lblpwdagin.Name = "lblpwdagin";
            this.lblpwdagin.Size = new System.Drawing.Size(63, 14);
            this.lblpwdagin.TabIndex = 30;
            this.lblpwdagin.Text = "确认密码";
            // 
            // txtPwd
            // 
            this.txtPwd.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtPwd.Location = new System.Drawing.Point(99, 147);
            this.txtPwd.MaxLength = 16;
            this.txtPwd.Name = "txtPwd";
            this.txtPwd.PasswordChar = '●';
            this.txtPwd.Size = new System.Drawing.Size(255, 26);
            this.txtPwd.TabIndex = 29;
            // 
            // lblpwd
            // 
            this.lblpwd.AutoSize = true;
            this.lblpwd.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblpwd.Location = new System.Drawing.Point(58, 152);
            this.lblpwd.Name = "lblpwd";
            this.lblpwd.Size = new System.Drawing.Size(35, 14);
            this.lblpwd.TabIndex = 28;
            this.lblpwd.Text = "密码";
            // 
            // txtNiname
            // 
            this.txtNiname.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtNiname.Location = new System.Drawing.Point(99, 89);
            this.txtNiname.MaxLength = 100;
            this.txtNiname.Name = "txtNiname";
            this.txtNiname.Size = new System.Drawing.Size(255, 26);
            this.txtNiname.TabIndex = 27;
            // 
            // lblni
            // 
            this.lblni.AutoSize = true;
            this.lblni.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblni.Location = new System.Drawing.Point(58, 94);
            this.lblni.Name = "lblni";
            this.lblni.Size = new System.Drawing.Size(35, 14);
            this.lblni.TabIndex = 26;
            this.lblni.Text = "昵称";
            // 
            // lblZhuce
            // 
            this.lblZhuce.AutoSize = true;
            this.lblZhuce.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblZhuce.Location = new System.Drawing.Point(18, 55);
            this.lblZhuce.Name = "lblZhuce";
            this.lblZhuce.Size = new System.Drawing.Size(85, 19);
            this.lblZhuce.TabIndex = 25;
            this.lblZhuce.Text = "注册账号";
            // 
            // FrmZhuce
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(235)))), ((int)(((byte)(253)))));
            this.ClientSize = new System.Drawing.Size(558, 555);
            this.ControlBox = false;
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.txtAddress);
            this.Controls.Add(this.lblAddress);
            this.Controls.Add(this.dtpbrith);
            this.Controls.Add(this.lblBrith);
            this.Controls.Add(this.rbogirl);
            this.Controls.Add(this.rdoBoy);
            this.Controls.Add(this.lblSex);
            this.Controls.Add(this.txtpwdagin);
            this.Controls.Add(this.lblpwdagin);
            this.Controls.Add(this.txtPwd);
            this.Controls.Add(this.lblpwd);
            this.Controls.Add(this.txtNiname);
            this.Controls.Add(this.lblni);
            this.Controls.Add(this.lblZhuce);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmZhuce";
            this.Text = "注册";
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.FrmZhuce_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.FrmZhuce_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.FrmZhuce_MouseUp);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lblsure;
        private System.Windows.Forms.Label lbltishipwd;
        private System.Windows.Forms.Label lblniname;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.TextBox txtAddress;
        private System.Windows.Forms.Label lblAddress;
        private System.Windows.Forms.DateTimePicker dtpbrith;
        private System.Windows.Forms.Label lblBrith;
        private System.Windows.Forms.RadioButton rbogirl;
        private System.Windows.Forms.RadioButton rdoBoy;
        private System.Windows.Forms.Label lblSex;
        private System.Windows.Forms.TextBox txtpwdagin;
        private System.Windows.Forms.Label lblpwdagin;
        private System.Windows.Forms.TextBox txtPwd;
        private System.Windows.Forms.Label lblpwd;
        private System.Windows.Forms.TextBox txtNiname;
        private System.Windows.Forms.Label lblni;
        private System.Windows.Forms.Label lblZhuce;
    }
}