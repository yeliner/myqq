﻿namespace myQQ
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.paneltop = new System.Windows.Forms.Panel();
            this.txtAutograph = new System.Windows.Forms.TextBox();
            this.btnLine = new System.Windows.Forms.Button();
            this.cmsline = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmiOnline = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiQme = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiDontDisturb = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiBusy = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiinivisble = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiLeave = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiOff = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiAboutme = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmisafe = new System.Windows.Forms.ToolStripMenuItem();
            this.lblniname = new System.Windows.Forms.Label();
            this.picturehead = new System.Windows.Forms.PictureBox();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnChange = new System.Windows.Forms.Button();
            this.txtsosuo = new System.Windows.Forms.TextBox();
            this.tpLianxi = new System.Windows.Forms.TabPage();
            this.chatList = new _CUSTOM_CONTROLS.ChatListBox();
            this.cmsgroup = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmire = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmidelete = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiadd = new System.Windows.Forms.ToolStripMenuItem();
            this.tcnews = new System.Windows.Forms.TabControl();
            this.tpnews = new System.Windows.Forms.TabPage();
            this.dgvNews = new System.Windows.Forms.DataGridView();
            this.Sendqq = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Reciveqq = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NewsType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Context = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RState = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.askNews = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SState = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Time = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.chatListBox1 = new _CUSTOM_CONTROLS.ChatListBox();
            this.ilnews = new System.Windows.Forms.ImageList(this.components);
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.mIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.cmsfriend = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmirefriend = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmidfriend = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmimfriend = new System.Windows.Forms.ToolStripMenuItem();
            this.btnSerch = new System.Windows.Forms.Button();
            this.paneltop.SuspendLayout();
            this.cmsline.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picturehead)).BeginInit();
            this.tpLianxi.SuspendLayout();
            this.cmsgroup.SuspendLayout();
            this.tcnews.SuspendLayout();
            this.tpnews.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvNews)).BeginInit();
            this.cmsfriend.SuspendLayout();
            this.SuspendLayout();
            // 
            // paneltop
            // 
            this.paneltop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(154)))), ((int)(((byte)(218)))));
            this.paneltop.Controls.Add(this.txtAutograph);
            this.paneltop.Controls.Add(this.btnLine);
            this.paneltop.Controls.Add(this.lblniname);
            this.paneltop.Controls.Add(this.picturehead);
            this.paneltop.Controls.Add(this.btnExit);
            this.paneltop.Controls.Add(this.btnChange);
            this.paneltop.Dock = System.Windows.Forms.DockStyle.Top;
            this.paneltop.Location = new System.Drawing.Point(0, 0);
            this.paneltop.Name = "paneltop";
            this.paneltop.Size = new System.Drawing.Size(276, 114);
            this.paneltop.TabIndex = 0;
            this.paneltop.MouseDown += new System.Windows.Forms.MouseEventHandler(this.FrmMain_MouseDown);
            this.paneltop.MouseMove += new System.Windows.Forms.MouseEventHandler(this.FrmMain_MouseMove);
            this.paneltop.MouseUp += new System.Windows.Forms.MouseEventHandler(this.FrmMain_MouseUp);
            // 
            // txtAutograph
            // 
            this.txtAutograph.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(154)))), ((int)(((byte)(218)))));
            this.txtAutograph.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtAutograph.Enabled = false;
            this.txtAutograph.ForeColor = System.Drawing.Color.White;
            this.txtAutograph.Location = new System.Drawing.Point(72, 57);
            this.txtAutograph.Name = "txtAutograph";
            this.txtAutograph.ReadOnly = true;
            this.txtAutograph.Size = new System.Drawing.Size(100, 14);
            this.txtAutograph.TabIndex = 41;
            // 
            // btnLine
            // 
            this.btnLine.BackgroundImage = global::myQQ.Properties.Resources.line;
            this.btnLine.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnLine.ContextMenuStrip = this.cmsline;
            this.btnLine.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.btnLine.FlatAppearance.BorderSize = 0;
            this.btnLine.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLine.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnLine.Location = new System.Drawing.Point(72, 75);
            this.btnLine.Name = "btnLine";
            this.btnLine.Size = new System.Drawing.Size(43, 19);
            this.btnLine.TabIndex = 39;
            this.btnLine.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnLine.UseVisualStyleBackColor = true;
            // 
            // cmsline
            // 
            this.cmsline.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiOnline,
            this.tsmiQme,
            this.tsmiDontDisturb,
            this.tsmiBusy,
            this.tsmiinivisble,
            this.tsmiLeave,
            this.tsmiOff,
            this.tsmiAboutme,
            this.tsmisafe});
            this.cmsline.Name = "cmsline";
            this.cmsline.Size = new System.Drawing.Size(153, 224);
            // 
            // tsmiOnline
            // 
            this.tsmiOnline.Font = new System.Drawing.Font("Microsoft YaHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tsmiOnline.Image = global::myQQ.Properties.Resources.imonline;
            this.tsmiOnline.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsmiOnline.Name = "tsmiOnline";
            this.tsmiOnline.Size = new System.Drawing.Size(152, 22);
            this.tsmiOnline.Text = "我在线上";
            this.tsmiOnline.Click += new System.EventHandler(this.tsmiOnline_Click);
            // 
            // tsmiQme
            // 
            this.tsmiQme.Image = global::myQQ.Properties.Resources.Qme;
            this.tsmiQme.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsmiQme.Name = "tsmiQme";
            this.tsmiQme.Size = new System.Drawing.Size(152, 22);
            this.tsmiQme.Text = "Q我吧";
            this.tsmiQme.Click += new System.EventHandler(this.tsmiQme_Click);
            // 
            // tsmiDontDisturb
            // 
            this.tsmiDontDisturb.Image = global::myQQ.Properties.Resources.dontDisturb;
            this.tsmiDontDisturb.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsmiDontDisturb.Name = "tsmiDontDisturb";
            this.tsmiDontDisturb.Size = new System.Drawing.Size(152, 22);
            this.tsmiDontDisturb.Text = "请勿打扰";
            this.tsmiDontDisturb.Click += new System.EventHandler(this.tsmiDont_Click);
            // 
            // tsmiBusy
            // 
            this.tsmiBusy.Image = global::myQQ.Properties.Resources.busy;
            this.tsmiBusy.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsmiBusy.Name = "tsmiBusy";
            this.tsmiBusy.Size = new System.Drawing.Size(152, 22);
            this.tsmiBusy.Text = "忙碌";
            this.tsmiBusy.Click += new System.EventHandler(this.tsmibusy_Click);
            // 
            // tsmiinivisble
            // 
            this.tsmiinivisble.Image = global::myQQ.Properties.Resources.invisible;
            this.tsmiinivisble.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsmiinivisble.Name = "tsmiinivisble";
            this.tsmiinivisble.Size = new System.Drawing.Size(152, 22);
            this.tsmiinivisble.Text = "隐身";
            this.tsmiinivisble.Click += new System.EventHandler(this.tsmiinivisble_Click);
            // 
            // tsmiLeave
            // 
            this.tsmiLeave.Image = global::myQQ.Properties.Resources.leave;
            this.tsmiLeave.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsmiLeave.Name = "tsmiLeave";
            this.tsmiLeave.Size = new System.Drawing.Size(152, 22);
            this.tsmiLeave.Text = "离开";
            this.tsmiLeave.Click += new System.EventHandler(this.tsmiLeave_Click);
            // 
            // tsmiOff
            // 
            this.tsmiOff.Image = global::myQQ.Properties.Resources.imoffline;
            this.tsmiOff.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsmiOff.Name = "tsmiOff";
            this.tsmiOff.Size = new System.Drawing.Size(152, 22);
            this.tsmiOff.Text = "离线";
            this.tsmiOff.Click += new System.EventHandler(this.tsmiOff_Click);
            // 
            // tsmiAboutme
            // 
            this.tsmiAboutme.Name = "tsmiAboutme";
            this.tsmiAboutme.Size = new System.Drawing.Size(152, 22);
            this.tsmiAboutme.Text = "我的资料";
            this.tsmiAboutme.Click += new System.EventHandler(this.tsmiAboutme_Click);
            // 
            // tsmisafe
            // 
            this.tsmisafe.Name = "tsmisafe";
            this.tsmisafe.Size = new System.Drawing.Size(152, 22);
            this.tsmisafe.Text = "安全密保";
            this.tsmisafe.Click += new System.EventHandler(this.tsmisafe_Click);
            // 
            // lblniname
            // 
            this.lblniname.AutoSize = true;
            this.lblniname.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblniname.Location = new System.Drawing.Point(81, 42);
            this.lblniname.Name = "lblniname";
            this.lblniname.Size = new System.Drawing.Size(0, 12);
            this.lblniname.TabIndex = 26;
            // 
            // picturehead
            // 
            this.picturehead.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.picturehead.ContextMenuStrip = this.cmsline;
            this.picturehead.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.picturehead.Location = new System.Drawing.Point(3, 32);
            this.picturehead.Name = "picturehead";
            this.picturehead.Size = new System.Drawing.Size(63, 62);
            this.picturehead.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picturehead.TabIndex = 23;
            this.picturehead.TabStop = false;
            this.picturehead.Click += new System.EventHandler(this.picturehead_Click);
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(154)))), ((int)(((byte)(218)))));
            this.btnExit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnExit.FlatAppearance.BorderSize = 0;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Image = global::myQQ.Properties.Resources.退出;
            this.btnExit.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnExit.Location = new System.Drawing.Point(254, 0);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(22, 26);
            this.btnExit.TabIndex = 22;
            this.btnExit.UseCompatibleTextRendering = true;
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnChange
            // 
            this.btnChange.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(154)))), ((int)(((byte)(218)))));
            this.btnChange.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnChange.FlatAppearance.BorderSize = 0;
            this.btnChange.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnChange.Image = global::myQQ.Properties.Resources.最小化;
            this.btnChange.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnChange.Location = new System.Drawing.Point(228, 1);
            this.btnChange.Name = "btnChange";
            this.btnChange.Size = new System.Drawing.Size(26, 26);
            this.btnChange.TabIndex = 21;
            this.btnChange.UseCompatibleTextRendering = true;
            this.btnChange.UseVisualStyleBackColor = false;
            this.btnChange.Click += new System.EventHandler(this.btnChange_Click);
            // 
            // txtsosuo
            // 
            this.txtsosuo.BackColor = System.Drawing.SystemColors.HighlightText;
            this.txtsosuo.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtsosuo.Location = new System.Drawing.Point(0, 113);
            this.txtsosuo.Name = "txtsosuo";
            this.txtsosuo.Size = new System.Drawing.Size(276, 26);
            this.txtsosuo.TabIndex = 24;
            // 
            // tpLianxi
            // 
            this.tpLianxi.Controls.Add(this.chatList);
            this.tpLianxi.Location = new System.Drawing.Point(4, 23);
            this.tpLianxi.Name = "tpLianxi";
            this.tpLianxi.Padding = new System.Windows.Forms.Padding(3);
            this.tpLianxi.Size = new System.Drawing.Size(268, 473);
            this.tpLianxi.TabIndex = 1;
            this.tpLianxi.Text = "联系人";
            this.tpLianxi.UseVisualStyleBackColor = true;
            // 
            // chatList
            // 
            this.chatList.BackColor = System.Drawing.Color.White;
            this.chatList.ContextMenuStrip = this.cmsgroup;
            this.chatList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chatList.ForeColor = System.Drawing.Color.Black;
            this.chatList.Location = new System.Drawing.Point(3, 3);
            this.chatList.Margin = new System.Windows.Forms.Padding(0);
            this.chatList.Name = "chatList";
            this.chatList.Size = new System.Drawing.Size(262, 467);
            this.chatList.TabIndex = 3;
            this.chatList.Text = "chatListBox3";
            this.chatList.DoubleClickSubItem += new _CUSTOM_CONTROLS.ChatListBox.ChatListEventHandler(this.chatList_DoubleClickSubItem);
            // 
            // cmsgroup
            // 
            this.cmsgroup.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmire,
            this.tsmidelete,
            this.tsmiadd});
            this.cmsgroup.Name = "cmsgroup";
            this.cmsgroup.Size = new System.Drawing.Size(137, 70);
            this.cmsgroup.Opening += new System.ComponentModel.CancelEventHandler(this.cmsgroup_Opening);
            // 
            // tsmire
            // 
            this.tsmire.Name = "tsmire";
            this.tsmire.Size = new System.Drawing.Size(136, 22);
            this.tsmire.Text = "重命名";
            this.tsmire.Click += new System.EventHandler(this.tsmire_Click);
            // 
            // tsmidelete
            // 
            this.tsmidelete.Name = "tsmidelete";
            this.tsmidelete.Size = new System.Drawing.Size(136, 22);
            this.tsmidelete.Text = "删除该分组";
            this.tsmidelete.Click += new System.EventHandler(this.tsmidelete_Click);
            // 
            // tsmiadd
            // 
            this.tsmiadd.Name = "tsmiadd";
            this.tsmiadd.Size = new System.Drawing.Size(136, 22);
            this.tsmiadd.Text = "添加分组";
            this.tsmiadd.Click += new System.EventHandler(this.tsmiadd_Click);
            // 
            // tcnews
            // 
            this.tcnews.Controls.Add(this.tpnews);
            this.tcnews.Controls.Add(this.tpLianxi);
            this.tcnews.Font = new System.Drawing.Font("华文楷体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(234)));
            this.tcnews.HotTrack = true;
            this.tcnews.ItemSize = new System.Drawing.Size(200, 19);
            this.tcnews.Location = new System.Drawing.Point(0, 139);
            this.tcnews.Margin = new System.Windows.Forms.Padding(0);
            this.tcnews.Name = "tcnews";
            this.tcnews.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tcnews.SelectedIndex = 0;
            this.tcnews.Size = new System.Drawing.Size(276, 500);
            this.tcnews.TabIndex = 25;
            // 
            // tpnews
            // 
            this.tpnews.BackColor = System.Drawing.Color.White;
            this.tpnews.Controls.Add(this.dgvNews);
            this.tpnews.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(170)));
            this.tpnews.Location = new System.Drawing.Point(4, 23);
            this.tpnews.Name = "tpnews";
            this.tpnews.Padding = new System.Windows.Forms.Padding(3);
            this.tpnews.Size = new System.Drawing.Size(268, 473);
            this.tpnews.TabIndex = 0;
            this.tpnews.Text = "消息";
            // 
            // dgvNews
            // 
            this.dgvNews.AllowUserToAddRows = false;
            this.dgvNews.AllowUserToDeleteRows = false;
            this.dgvNews.AllowUserToResizeColumns = false;
            this.dgvNews.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("微软雅黑", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.dgvNews.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvNews.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvNews.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dgvNews.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvNews.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvNews.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgvNews.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvNews.ColumnHeadersVisible = false;
            this.dgvNews.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Sendqq,
            this.Reciveqq,
            this.NewsType,
            this.Context,
            this.RState,
            this.askNews,
            this.SState,
            this.Time});
            this.dgvNews.Location = new System.Drawing.Point(3, 3);
            this.dgvNews.Margin = new System.Windows.Forms.Padding(0);
            this.dgvNews.MultiSelect = false;
            this.dgvNews.Name = "dgvNews";
            this.dgvNews.ReadOnly = true;
            this.dgvNews.RowHeadersVisible = false;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("微软雅黑", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            this.dgvNews.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvNews.RowTemplate.Height = 50;
            this.dgvNews.RowTemplate.ReadOnly = true;
            this.dgvNews.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvNews.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvNews.ShowEditingIcon = false;
            this.dgvNews.Size = new System.Drawing.Size(268, 473);
            this.dgvNews.TabIndex = 2;
            this.dgvNews.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvNews_CellClick);
            // 
            // Sendqq
            // 
            this.Sendqq.DataPropertyName = "Sendqq";
            this.Sendqq.HeaderText = "Sendqq";
            this.Sendqq.Name = "Sendqq";
            this.Sendqq.ReadOnly = true;
            this.Sendqq.Visible = false;
            // 
            // Reciveqq
            // 
            this.Reciveqq.DataPropertyName = "Reciveqq";
            this.Reciveqq.HeaderText = "Reciveqq";
            this.Reciveqq.Name = "Reciveqq";
            this.Reciveqq.ReadOnly = true;
            this.Reciveqq.Visible = false;
            // 
            // NewsType
            // 
            this.NewsType.DataPropertyName = "NewsType";
            this.NewsType.FillWeight = 59.80394F;
            this.NewsType.HeaderText = "NewsType";
            this.NewsType.Name = "NewsType";
            this.NewsType.ReadOnly = true;
            // 
            // Context
            // 
            this.Context.DataPropertyName = "Context";
            this.Context.FillWeight = 152.2843F;
            this.Context.HeaderText = "Context";
            this.Context.Name = "Context";
            this.Context.ReadOnly = true;
            // 
            // RState
            // 
            this.RState.DataPropertyName = "RState";
            this.RState.HeaderText = "RState";
            this.RState.Name = "RState";
            this.RState.ReadOnly = true;
            // 
            // askNews
            // 
            this.askNews.DataPropertyName = "asknews";
            this.askNews.HeaderText = "asknews";
            this.askNews.Name = "askNews";
            this.askNews.ReadOnly = true;
            this.askNews.Visible = false;
            // 
            // SState
            // 
            this.SState.DataPropertyName = "SState";
            this.SState.HeaderText = "SState";
            this.SState.Name = "SState";
            this.SState.ReadOnly = true;
            this.SState.Visible = false;
            // 
            // Time
            // 
            this.Time.DataPropertyName = "Time";
            this.Time.FillWeight = 87.91179F;
            this.Time.HeaderText = "Time";
            this.Time.Name = "Time";
            this.Time.ReadOnly = true;
            // 
            // chatListBox1
            // 
            this.chatListBox1.BackColor = System.Drawing.Color.White;
            this.chatListBox1.ForeColor = System.Drawing.Color.DarkOrange;
            this.chatListBox1.Location = new System.Drawing.Point(0, 0);
            this.chatListBox1.Name = "chatListBox1";
            this.chatListBox1.Size = new System.Drawing.Size(150, 250);
            this.chatListBox1.TabIndex = 0;
            // 
            // ilnews
            // 
            this.ilnews.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
            this.ilnews.ImageSize = new System.Drawing.Size(16, 16);
            this.ilnews.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 5000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // mIcon
            // 
            this.mIcon.ContextMenuStrip = this.cmsline;
            this.mIcon.Text = "MyQQ";
            this.mIcon.Visible = true;
            // 
            // cmsfriend
            // 
            this.cmsfriend.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmirefriend,
            this.tsmidfriend,
            this.tsmimfriend});
            this.cmsfriend.Name = "cmsgroup";
            this.cmsfriend.Size = new System.Drawing.Size(125, 70);
            // 
            // tsmirefriend
            // 
            this.tsmirefriend.Name = "tsmirefriend";
            this.tsmirefriend.Size = new System.Drawing.Size(124, 22);
            this.tsmirefriend.Text = "修改备注";
            this.tsmirefriend.Click += new System.EventHandler(this.tsmiEfriend_Click);
            // 
            // tsmidfriend
            // 
            this.tsmidfriend.Name = "tsmidfriend";
            this.tsmidfriend.Size = new System.Drawing.Size(124, 22);
            this.tsmidfriend.Text = "删除好友";
            this.tsmidfriend.Click += new System.EventHandler(this.tsmidfriend_Click);
            // 
            // tsmimfriend
            // 
            this.tsmimfriend.Name = "tsmimfriend";
            this.tsmimfriend.Size = new System.Drawing.Size(124, 22);
            this.tsmimfriend.Text = "移动好友";
            this.tsmimfriend.Click += new System.EventHandler(this.tsmimfriend_Click);
            // 
            // btnSerch
            // 
            this.btnSerch.BackgroundImage = global::myQQ.Properties.Resources.查找;
            this.btnSerch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSerch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSerch.Location = new System.Drawing.Point(249, 113);
            this.btnSerch.Name = "btnSerch";
            this.btnSerch.Size = new System.Drawing.Size(27, 26);
            this.btnSerch.TabIndex = 26;
            this.btnSerch.UseVisualStyleBackColor = true;
            this.btnSerch.Click += new System.EventHandler(this.btnSerch_Click);
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(276, 638);
            this.Controls.Add(this.btnSerch);
            this.Controls.Add(this.tcnews);
            this.Controls.Add(this.txtsosuo);
            this.Controls.Add(this.paneltop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "主窗体";
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.FrmMain_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.FrmMain_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.FrmMain_MouseUp);
            this.paneltop.ResumeLayout(false);
            this.paneltop.PerformLayout();
            this.cmsline.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picturehead)).EndInit();
            this.tpLianxi.ResumeLayout(false);
            this.cmsgroup.ResumeLayout(false);
            this.tcnews.ResumeLayout(false);
            this.tpnews.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvNews)).EndInit();
            this.cmsfriend.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel paneltop;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnChange;
        private System.Windows.Forms.PictureBox picturehead;
        private System.Windows.Forms.TextBox txtsosuo;
        private System.Windows.Forms.Label lblniname;
        private System.Windows.Forms.Button btnLine;
        private System.Windows.Forms.ContextMenuStrip cmsline;
        private System.Windows.Forms.ToolStripMenuItem tsmiOnline;
        private System.Windows.Forms.ToolStripMenuItem tsmiBusy;
        private System.Windows.Forms.ToolStripMenuItem tsmiinivisble;
        private System.Windows.Forms.ToolStripMenuItem tsmiOff;
        private System.Windows.Forms.ToolStripMenuItem tsmiAboutme;
        private System.Windows.Forms.ToolStripMenuItem tsmiQme;
        private System.Windows.Forms.ToolStripMenuItem tsmiDontDisturb;
        private System.Windows.Forms.ToolStripMenuItem tsmiLeave;
        private System.Windows.Forms.TabPage tpLianxi;
        private System.Windows.Forms.TabControl tcnews;
        private System.Windows.Forms.TextBox txtAutograph;
        private System.Windows.Forms.Button btnSerch;
        private _CUSTOM_CONTROLS.ChatListBox chatListBox1;
        private System.Windows.Forms.ContextMenuStrip cmsgroup;
        private System.Windows.Forms.ToolStripMenuItem tsmire;
        private System.Windows.Forms.ToolStripMenuItem tsmidelete;
        private System.Windows.Forms.ToolStripMenuItem tsmiadd;
        private System.Windows.Forms.ImageList ilnews;
        private System.Windows.Forms.TabPage tpnews;
        private System.Windows.Forms.DataGridView dgvNews;
        private System.Windows.Forms.DataGridViewTextBoxColumn Sendqq;
        private System.Windows.Forms.DataGridViewTextBoxColumn Reciveqq;
        private System.Windows.Forms.DataGridViewTextBoxColumn NewsType;
        private System.Windows.Forms.DataGridViewTextBoxColumn Context;
        private System.Windows.Forms.DataGridViewTextBoxColumn RState;
        private System.Windows.Forms.DataGridViewTextBoxColumn askNews;
        private System.Windows.Forms.DataGridViewTextBoxColumn SState;
        private System.Windows.Forms.DataGridViewTextBoxColumn Time;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ToolStripMenuItem tsmisafe;
        private _CUSTOM_CONTROLS.ChatListBox chatList;
        private System.Windows.Forms.NotifyIcon mIcon;
        private System.Windows.Forms.ContextMenuStrip cmsfriend;
        private System.Windows.Forms.ToolStripMenuItem tsmidfriend;
        private System.Windows.Forms.ToolStripMenuItem tsmirefriend;
        private System.Windows.Forms.ToolStripMenuItem tsmimfriend;
        // private System.Windows.Forms.DataGridViewTextBoxColumn NewsType;
        //private System.Windows.Forms.DataGridViewTextBoxColumn Time;
        //  private System.Windows.Forms.DataGridViewTextBoxColumn Context;
    }
}