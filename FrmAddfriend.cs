﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace myQQ
{
    public partial class FrmAddfriend : Form
    {
        public FrmAddfriend()
        {
            InitializeComponent();
        }

        public static int askqq;
        private void FrmAdd_Load(object sender, EventArgs e)
        {
            askqq = FrmMain.askqq;
            if (askqq == FrmMain.Lqq)
            {
                lbltishi.Text = "用户不能加自己为好友！";
                lbltishi.Visible = true;
                btnnext.Visible = false;
                btnclose.Visible = false;
            }
            else
            {
                groupload();
                string sql = " select qq,Niname,Sex,Address,headImage from UserInfo where qq=" + askqq;
                DataSet friend = DatabaseHelper.executeDataSet(sql);
                if (friend.Tables[0].Rows.Count > 0)
                {
                    string sqlf = string.Format("select * from [dbo].[Friends] where fqq={0} and Zqq={1}", FrmMain.askqq, FrmLogin.qq);
                    DataSet f = DatabaseHelper.executeDataSet(sqlf);
                    if (f.Tables[0].Rows.Count > 0)
                    {
                        lbltishi.Text = "该用户已是您的好友！";
                        lbltishi.Visible = true;
                        btnnext.Visible = false;
                        btnclose.Visible = false;
                    }
                    else
                    {
                        string sql3 = string.Format("  select remark,gname  from [dbo].[linshi] where Zqq={0} and askqq={1}", FrmLogin.qq, FrmMain.askqq);
                        DataSet linhsi = DatabaseHelper.executeDataSet(sql3);
                        if (linhsi.Tables[0].Rows.Count > 0)
                        {
                            lbltishi.Visible = true;
                            lbltishi.Text = "您已向该用户发送请求，正在等待对方验证！";
                            btnnext.Visible = false;
                            btnclose.Visible = false;
                        }
                        else
                        {
                            puse.Visible = true;
                            picturehead.BackgroundImage = Image.FromFile(friend.Tables[0].Rows[0]["headImage"].ToString());
                            lblQQ.Text = friend.Tables[0].Rows[0]["qq"].ToString();
                            lblniname.Text = friend.Tables[0].Rows[0]["Niname"].ToString();
                            lblAddress2.Text = string.Format("地址：" + friend.Tables[0].Rows[0]["Address"].ToString());
                            lblsex.Text = string.Format("性别：" + friend.Tables[0].Rows[0]["Sex"].ToString());
                            pleft.Visible = true;
                        }
                    }
                }
                else
                {
                    lbltishi.Visible = true;
                    btnnext.Visible = false;
                    btnclose.Visible = false;
                }
            }
        }
        private void btnnext_Click(object sender, EventArgs e)
        {
            pgroup.Visible = true;
            btnnext.Visible = false;
            btnclose.Visible = false;
            txtremark.Text = lblniname.Text;
        }

        private void lbladdugrop_Click(object sender, EventArgs e)
        {

            this.cbgroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
            cbgroup.Focus();
            lbladdugrop.Text = "";
        }

        private void btnCompelete_Click(object sender, EventArgs e)
        {
            addgroup();
            string sql = string.Format("insert into [dbo].[News] (Sendqq,Reciveqq ,NewsType,Context,SState,RState,Time,asknews) values({0},{1},'验证','{2}','已发送','未读','{3}','{4}')",
           FrmLogin.qq, askqq, FrmLogin.qq + "想加你为好友", DateTime.Now, txtask.Text);
            int num = DatabaseHelper.executeNonQuery(sql);
            string sqll = string.Format("  insert into [dbo].[linshi] (Zqq,askqq,remark,gname) values({0},{1},'{2}','{3}')", FrmLogin.qq, askqq, txtremark.Text, cbgroup.Text);
            int numl = DatabaseHelper.executeNonQuery(sqll);
            if (num > 0 && numl > 0)
            {
                pgroup.Visible = false;
                pleft.Visible = false;
                puse.Visible = false;
                lbltishi.Text = "发送请求消息成功！请等待对方验证";
                lbltishi.Visible = true;
            }

        }

        private void addgroup()
        {
            if (cbgroup.Text == "")
            {
                cbgroup.Text = "我的同学";
            }
            string sql = string.Format("select * from [dbo].[Group] where Zqq = {0} and Groupname = '{1}'", FrmLogin.qq, cbgroup.Text);
            DataSet gname = DatabaseHelper.executeDataSet(sql);
            if (gname.Tables[0].Rows.Count <= 0)
            {
                string sql1 = string.Format(" insert into [dbo].[Group](Zqq,Groupname) values({0},'{1}')", FrmLogin.qq, cbgroup.Text);
                int num = DatabaseHelper.executeNonQuery(sql1);
                if (num > 0)
                {
                    MessageBox.Show("新建分组成功！");
                }
            }
            else
            {
                DialogResult r = MessageBox.Show("该组名已在使用，是否重新输入组名", "提示", MessageBoxButtons.OKCancel);
                if (r == DialogResult.OK)
                {
                    this.cbgroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
                    cbgroup.Focus();
                }
                if (r == DialogResult.Cancel)
                {
                    this.cbgroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
                    lbladdugrop.Text = "创建分组";
                }
            }
            groupload();
        }

        private void groupload()
        {
            string sql2 = "select Groupname,id from  [dbo].[Group] where Zqq=" + FrmLogin.qq;
            DataSet fz = DatabaseHelper.executeDataSet(sql2);
            for (int i = 0; i < fz.Tables[0].Rows.Count; i++)
            {//分组
                cbgroup.Items.Add(fz.Tables[0].Rows[i]["Groupname"].ToString());
            }
        }

        private void btnChange_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void btnclose_Click(object sender, EventArgs e)
        {
            this.Hide();
        }


        private void btnok_Click(object sender, EventArgs e)
        {
            addgroup();
        }
        Point mouseOff;//鼠标移动位置变量
        bool leftFlag;//标记是否为左键


        private void FrmAddfriend_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                mouseOff = new Point(-e.X, -e.Y);//得到变量的值
                leftFlag = true;                  //点击左键按下时标注为true;
            }
        }

        private void FrmAddfriend_MouseMove(object sender, MouseEventArgs e)
        {
            if (leftFlag)
            {
                Point mouseSet = Control.MousePosition;
                mouseSet.Offset(mouseOff.X, mouseOff.Y);  //设置移动后的位置
                Location = mouseSet;
            }
        }

        private void FrmAddfriend_MouseUp(object sender, MouseEventArgs e)
        {

            if (leftFlag)
            {
                leftFlag = false;//释放鼠标后标注为false;
            }

        }
    }
}
