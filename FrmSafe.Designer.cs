﻿namespace myQQ
{
    partial class FrmSafe
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblhead = new System.Windows.Forms.Label();
            this.Lblask1 = new System.Windows.Forms.Label();
            this.Answer1 = new System.Windows.Forms.Label();
            this.cbAsk1 = new System.Windows.Forms.ComboBox();
            this.txtAnswer1 = new System.Windows.Forms.TextBox();
            this.txtAnswer2 = new System.Windows.Forms.TextBox();
            this.cbAsk2 = new System.Windows.Forms.ComboBox();
            this.lblanswer2 = new System.Windows.Forms.Label();
            this.lblask2 = new System.Windows.Forms.Label();
            this.txtAnswer3 = new System.Windows.Forms.TextBox();
            this.cbAsk3 = new System.Windows.Forms.ComboBox();
            this.lblanswer3 = new System.Windows.Forms.Label();
            this.lblask3 = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnChange = new System.Windows.Forms.Button();
            this.lbl1 = new System.Windows.Forms.Label();
            this.lbl2 = new System.Windows.Forms.Label();
            this.lbl3 = new System.Windows.Forms.Label();
            this.btnnext = new System.Windows.Forms.Button();
            this.btncancel = new System.Windows.Forms.Button();
            this.lbla1 = new System.Windows.Forms.Label();
            this.pnext = new System.Windows.Forms.Panel();
            this.lblnone = new System.Windows.Forms.Label();
            this.lblt3 = new System.Windows.Forms.Label();
            this.lblt1 = new System.Windows.Forms.Label();
            this.lblt2 = new System.Windows.Forms.Label();
            this.txta3 = new System.Windows.Forms.TextBox();
            this.txta2 = new System.Windows.Forms.TextBox();
            this.txta1 = new System.Windows.Forms.TextBox();
            this.lbla3 = new System.Windows.Forms.Label();
            this.lbla2 = new System.Windows.Forms.Label();
            this.btnok = new System.Windows.Forms.Button();
            this.pstart = new System.Windows.Forms.Panel();
            this.btnyes = new System.Windows.Forms.Button();
            this.btnno = new System.Windows.Forms.Button();
            this.pnext.SuspendLayout();
            this.pstart.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblhead
            // 
            this.lblhead.AutoSize = true;
            this.lblhead.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblhead.Location = new System.Drawing.Point(12, 44);
            this.lblhead.Name = "lblhead";
            this.lblhead.Size = new System.Drawing.Size(499, 20);
            this.lblhead.TabIndex = 0;
            this.lblhead.Text = "为了避免遗忘，请您填写真实信息，这将帮助您以后通过回答问题快速找回密码";
            // 
            // Lblask1
            // 
            this.Lblask1.AutoSize = true;
            this.Lblask1.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Lblask1.Location = new System.Drawing.Point(23, 18);
            this.Lblask1.Name = "Lblask1";
            this.Lblask1.Size = new System.Drawing.Size(65, 20);
            this.Lblask1.TabIndex = 1;
            this.Lblask1.Text = "问题一：";
            // 
            // Answer1
            // 
            this.Answer1.AutoSize = true;
            this.Answer1.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Answer1.Location = new System.Drawing.Point(25, 66);
            this.Answer1.Name = "Answer1";
            this.Answer1.Size = new System.Drawing.Size(63, 20);
            this.Answer1.TabIndex = 3;
            this.Answer1.Text = "答   案：";
            // 
            // cbAsk1
            // 
            this.cbAsk1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbAsk1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.cbAsk1.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cbAsk1.FormattingEnabled = true;
            this.cbAsk1.Items.AddRange(new object[] {
            "您母亲的名字是？",
            "您配偶的生日是？",
            "您的学号（工号）是？",
            "您母亲的生日是？",
            "您高中班主任的名字是？",
            "您父亲的名字是？",
            "您小学班主任的名字是？",
            "您父亲的生日是？",
            "您配偶的名字是？",
            "您初中班主任的名字是？",
            "您最熟悉的童年好友名字是？",
            "您最熟悉的学校宿舍室友名字是？",
            "对您影响最大的人名字是？",
            ""});
            this.cbAsk1.Location = new System.Drawing.Point(91, 15);
            this.cbAsk1.Name = "cbAsk1";
            this.cbAsk1.Size = new System.Drawing.Size(276, 28);
            this.cbAsk1.TabIndex = 4;
            this.cbAsk1.TextChanged += new System.EventHandler(this.cbAsk1_TextChanged);
            // 
            // txtAnswer1
            // 
            this.txtAnswer1.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtAnswer1.Location = new System.Drawing.Point(91, 63);
            this.txtAnswer1.Name = "txtAnswer1";
            this.txtAnswer1.Size = new System.Drawing.Size(276, 26);
            this.txtAnswer1.TabIndex = 5;
            this.txtAnswer1.TextChanged += new System.EventHandler(this.txtAnswer1_TextChanged);
            // 
            // txtAnswer2
            // 
            this.txtAnswer2.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtAnswer2.Location = new System.Drawing.Point(91, 152);
            this.txtAnswer2.Name = "txtAnswer2";
            this.txtAnswer2.Size = new System.Drawing.Size(276, 26);
            this.txtAnswer2.TabIndex = 9;
            this.txtAnswer2.TextChanged += new System.EventHandler(this.txtAnswer2_TextChanged);
            // 
            // cbAsk2
            // 
            this.cbAsk2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbAsk2.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cbAsk2.FormattingEnabled = true;
            this.cbAsk2.Location = new System.Drawing.Point(91, 107);
            this.cbAsk2.Name = "cbAsk2";
            this.cbAsk2.Size = new System.Drawing.Size(276, 28);
            this.cbAsk2.TabIndex = 8;
            this.cbAsk2.TextChanged += new System.EventHandler(this.cbAsk2_TextChanged);
            // 
            // lblanswer2
            // 
            this.lblanswer2.AutoSize = true;
            this.lblanswer2.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblanswer2.Location = new System.Drawing.Point(23, 155);
            this.lblanswer2.Name = "lblanswer2";
            this.lblanswer2.Size = new System.Drawing.Size(67, 20);
            this.lblanswer2.TabIndex = 7;
            this.lblanswer2.Text = "答    案：";
            // 
            // lblask2
            // 
            this.lblask2.AutoSize = true;
            this.lblask2.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblask2.Location = new System.Drawing.Point(23, 111);
            this.lblask2.Name = "lblask2";
            this.lblask2.Size = new System.Drawing.Size(65, 20);
            this.lblask2.TabIndex = 6;
            this.lblask2.Text = "问题二：";
            // 
            // txtAnswer3
            // 
            this.txtAnswer3.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtAnswer3.Location = new System.Drawing.Point(91, 245);
            this.txtAnswer3.Name = "txtAnswer3";
            this.txtAnswer3.Size = new System.Drawing.Size(276, 26);
            this.txtAnswer3.TabIndex = 13;
            this.txtAnswer3.TextChanged += new System.EventHandler(this.txtAnswer3_TextChanged);
            // 
            // cbAsk3
            // 
            this.cbAsk3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbAsk3.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cbAsk3.FormattingEnabled = true;
            this.cbAsk3.Location = new System.Drawing.Point(91, 196);
            this.cbAsk3.Name = "cbAsk3";
            this.cbAsk3.Size = new System.Drawing.Size(276, 28);
            this.cbAsk3.TabIndex = 12;
            this.cbAsk3.TextChanged += new System.EventHandler(this.cbAsk3_TextChanged);
            // 
            // lblanswer3
            // 
            this.lblanswer3.AutoSize = true;
            this.lblanswer3.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblanswer3.Location = new System.Drawing.Point(22, 248);
            this.lblanswer3.Name = "lblanswer3";
            this.lblanswer3.Size = new System.Drawing.Size(67, 20);
            this.lblanswer3.TabIndex = 11;
            this.lblanswer3.Text = "答    案：";
            // 
            // lblask3
            // 
            this.lblask3.AutoSize = true;
            this.lblask3.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblask3.Location = new System.Drawing.Point(23, 200);
            this.lblask3.Name = "lblask3";
            this.lblask3.Size = new System.Drawing.Size(65, 20);
            this.lblask3.TabIndex = 10;
            this.lblask3.Text = "问题三：";
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.Transparent;
            this.btnExit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnExit.FlatAppearance.BorderSize = 0;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Image = global::myQQ.Properties.Resources.退出;
            this.btnExit.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnExit.Location = new System.Drawing.Point(499, 1);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(26, 26);
            this.btnExit.TabIndex = 24;
            this.btnExit.UseCompatibleTextRendering = true;
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnChange
            // 
            this.btnChange.BackColor = System.Drawing.Color.Transparent;
            this.btnChange.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnChange.FlatAppearance.BorderSize = 0;
            this.btnChange.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnChange.Image = global::myQQ.Properties.Resources.最小化;
            this.btnChange.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnChange.Location = new System.Drawing.Point(471, 1);
            this.btnChange.Name = "btnChange";
            this.btnChange.Size = new System.Drawing.Size(26, 26);
            this.btnChange.TabIndex = 23;
            this.btnChange.UseCompatibleTextRendering = true;
            this.btnChange.UseVisualStyleBackColor = false;
            this.btnChange.Click += new System.EventHandler(this.btnChange_Click);
            // 
            // lbl1
            // 
            this.lbl1.AutoSize = true;
            this.lbl1.ForeColor = System.Drawing.Color.Red;
            this.lbl1.Location = new System.Drawing.Point(95, 48);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(173, 12);
            this.lbl1.TabIndex = 0;
            this.lbl1.Text = "请填写2-19个中文或3-38个英文";
            this.lbl1.Visible = false;
            // 
            // lbl2
            // 
            this.lbl2.AutoSize = true;
            this.lbl2.ForeColor = System.Drawing.Color.Red;
            this.lbl2.Location = new System.Drawing.Point(94, 138);
            this.lbl2.Name = "lbl2";
            this.lbl2.Size = new System.Drawing.Size(173, 12);
            this.lbl2.TabIndex = 26;
            this.lbl2.Text = "请填写2-19个中文或3-38个英文";
            this.lbl2.Visible = false;
            // 
            // lbl3
            // 
            this.lbl3.AutoSize = true;
            this.lbl3.ForeColor = System.Drawing.Color.Red;
            this.lbl3.Location = new System.Drawing.Point(94, 230);
            this.lbl3.Name = "lbl3";
            this.lbl3.Size = new System.Drawing.Size(173, 12);
            this.lbl3.TabIndex = 27;
            this.lbl3.Text = "请填写2-19个中文或3-38个英文";
            this.lbl3.Visible = false;
            // 
            // btnnext
            // 
            this.btnnext.Location = new System.Drawing.Point(279, 381);
            this.btnnext.Name = "btnnext";
            this.btnnext.Size = new System.Drawing.Size(61, 23);
            this.btnnext.TabIndex = 28;
            this.btnnext.Text = "下一步";
            this.btnnext.UseVisualStyleBackColor = true;
            this.btnnext.Visible = false;
            this.btnnext.Click += new System.EventHandler(this.btnnext_Click);
            // 
            // btncancel
            // 
            this.btncancel.Location = new System.Drawing.Point(345, 381);
            this.btncancel.Name = "btncancel";
            this.btncancel.Size = new System.Drawing.Size(73, 23);
            this.btncancel.TabIndex = 29;
            this.btncancel.Text = "取消";
            this.btncancel.UseVisualStyleBackColor = true;
            this.btncancel.Click += new System.EventHandler(this.btncancel_Click);
            // 
            // lbla1
            // 
            this.lbla1.AutoSize = true;
            this.lbla1.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbla1.Location = new System.Drawing.Point(8, 12);
            this.lbla1.Name = "lbla1";
            this.lbla1.Size = new System.Drawing.Size(65, 20);
            this.lbla1.TabIndex = 30;
            this.lbla1.Text = "问题一：";
            // 
            // pnext
            // 
            this.pnext.Controls.Add(this.lblt3);
            this.pnext.Controls.Add(this.lblt1);
            this.pnext.Controls.Add(this.lblt2);
            this.pnext.Controls.Add(this.txta3);
            this.pnext.Controls.Add(this.txta2);
            this.pnext.Controls.Add(this.txta1);
            this.pnext.Controls.Add(this.lbla3);
            this.pnext.Controls.Add(this.lbla2);
            this.pnext.Controls.Add(this.lbla1);
            this.pnext.Location = new System.Drawing.Point(86, 6);
            this.pnext.Name = "pnext";
            this.pnext.Size = new System.Drawing.Size(281, 272);
            this.pnext.TabIndex = 31;
            this.pnext.Visible = false;
            // 
            // lblnone
            // 
            this.lblnone.AutoSize = true;
            this.lblnone.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblnone.ForeColor = System.Drawing.Color.Blue;
            this.lblnone.Location = new System.Drawing.Point(116, 177);
            this.lblnone.Name = "lblnone";
            this.lblnone.Size = new System.Drawing.Size(287, 63);
            this.lblnone.TabIndex = 32;
            this.lblnone.Text = "您好！您已经在此安全中心设置了密保!\r\n\r\n              是否修改密保问题？";
            this.lblnone.Visible = false;
            // 
            // lblt3
            // 
            this.lblt3.AutoSize = true;
            this.lblt3.ForeColor = System.Drawing.Color.Red;
            this.lblt3.Location = new System.Drawing.Point(4, 225);
            this.lblt3.Name = "lblt3";
            this.lblt3.Size = new System.Drawing.Size(101, 12);
            this.lblt3.TabIndex = 38;
            this.lblt3.Text = "两次答案不一致！";
            this.lblt3.Visible = false;
            // 
            // lblt1
            // 
            this.lblt1.AutoSize = true;
            this.lblt1.ForeColor = System.Drawing.Color.Red;
            this.lblt1.Location = new System.Drawing.Point(4, 42);
            this.lblt1.Name = "lblt1";
            this.lblt1.Size = new System.Drawing.Size(101, 12);
            this.lblt1.TabIndex = 37;
            this.lblt1.Text = "两次答案不一致！";
            this.lblt1.Visible = false;
            // 
            // lblt2
            // 
            this.lblt2.AutoSize = true;
            this.lblt2.ForeColor = System.Drawing.Color.Red;
            this.lblt2.Location = new System.Drawing.Point(3, 131);
            this.lblt2.Name = "lblt2";
            this.lblt2.Size = new System.Drawing.Size(101, 12);
            this.lblt2.TabIndex = 36;
            this.lblt2.Text = "两次答案不一致！";
            this.lblt2.Visible = false;
            // 
            // txta3
            // 
            this.txta3.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txta3.Location = new System.Drawing.Point(2, 240);
            this.txta3.Name = "txta3";
            this.txta3.Size = new System.Drawing.Size(276, 26);
            this.txta3.TabIndex = 35;
            // 
            // txta2
            // 
            this.txta2.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txta2.Location = new System.Drawing.Point(2, 146);
            this.txta2.Name = "txta2";
            this.txta2.Size = new System.Drawing.Size(276, 26);
            this.txta2.TabIndex = 34;
            // 
            // txta1
            // 
            this.txta1.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txta1.Location = new System.Drawing.Point(2, 56);
            this.txta1.Name = "txta1";
            this.txta1.Size = new System.Drawing.Size(276, 26);
            this.txta1.TabIndex = 33;
            // 
            // lbla3
            // 
            this.lbla3.AutoSize = true;
            this.lbla3.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbla3.Location = new System.Drawing.Point(7, 194);
            this.lbla3.Name = "lbla3";
            this.lbla3.Size = new System.Drawing.Size(65, 20);
            this.lbla3.TabIndex = 32;
            this.lbla3.Text = "问题三：";
            // 
            // lbla2
            // 
            this.lbla2.AutoSize = true;
            this.lbla2.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbla2.Location = new System.Drawing.Point(8, 105);
            this.lbla2.Name = "lbla2";
            this.lbla2.Size = new System.Drawing.Size(65, 20);
            this.lbla2.TabIndex = 31;
            this.lbla2.Text = "问题二：";
            // 
            // btnok
            // 
            this.btnok.Location = new System.Drawing.Point(264, 381);
            this.btnok.Name = "btnok";
            this.btnok.Size = new System.Drawing.Size(75, 23);
            this.btnok.TabIndex = 36;
            this.btnok.Text = "确认";
            this.btnok.UseVisualStyleBackColor = true;
            this.btnok.Visible = false;
            this.btnok.Click += new System.EventHandler(this.btnok_Click);
            // 
            // pstart
            // 
            this.pstart.Controls.Add(this.pnext);
            this.pstart.Controls.Add(this.lblask2);
            this.pstart.Controls.Add(this.cbAsk1);
            this.pstart.Controls.Add(this.txtAnswer1);
            this.pstart.Controls.Add(this.cbAsk2);
            this.pstart.Controls.Add(this.txtAnswer2);
            this.pstart.Controls.Add(this.cbAsk3);
            this.pstart.Controls.Add(this.txtAnswer3);
            this.pstart.Controls.Add(this.lblanswer3);
            this.pstart.Controls.Add(this.lbl1);
            this.pstart.Controls.Add(this.lblask3);
            this.pstart.Controls.Add(this.lbl2);
            this.pstart.Controls.Add(this.lblanswer2);
            this.pstart.Controls.Add(this.lbl3);
            this.pstart.Controls.Add(this.Answer1);
            this.pstart.Controls.Add(this.Lblask1);
            this.pstart.Location = new System.Drawing.Point(65, 86);
            this.pstart.Name = "pstart";
            this.pstart.Size = new System.Drawing.Size(394, 289);
            this.pstart.TabIndex = 39;
            // 
            // btnyes
            // 
            this.btnyes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnyes.Location = new System.Drawing.Point(249, 311);
            this.btnyes.Name = "btnyes";
            this.btnyes.Size = new System.Drawing.Size(75, 23);
            this.btnyes.TabIndex = 40;
            this.btnyes.Text = "是";
            this.btnyes.UseVisualStyleBackColor = true;
            this.btnyes.Visible = false;
            this.btnyes.Click += new System.EventHandler(this.btnyes_Click);
            // 
            // btnno
            // 
            this.btnno.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnno.Location = new System.Drawing.Point(345, 311);
            this.btnno.Name = "btnno";
            this.btnno.Size = new System.Drawing.Size(75, 23);
            this.btnno.TabIndex = 41;
            this.btnno.Text = "否";
            this.btnno.UseVisualStyleBackColor = true;
            this.btnno.Visible = false;
            // 
            // FrmSafe
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(235)))), ((int)(((byte)(253)))));
            this.ClientSize = new System.Drawing.Size(526, 414);
            this.Controls.Add(this.btnok);
            this.Controls.Add(this.btncancel);
            this.Controls.Add(this.btnnext);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnChange);
            this.Controls.Add(this.lblhead);
            this.Controls.Add(this.pstart);
            this.Controls.Add(this.lblnone);
            this.Controls.Add(this.btnyes);
            this.Controls.Add(this.btnno);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmSafe";
            this.Text = "FrmSafe";
            this.Load += new System.EventHandler(this.FrmSafe_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.FrmSafe_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.FrmSafe_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.FrmSafe_MouseUp);
            this.pnext.ResumeLayout(false);
            this.pnext.PerformLayout();
            this.pstart.ResumeLayout(false);
            this.pstart.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblhead;
        private System.Windows.Forms.Label Lblask1;
        private System.Windows.Forms.Label Answer1;
        private System.Windows.Forms.ComboBox cbAsk1;
        private System.Windows.Forms.TextBox txtAnswer1;
        private System.Windows.Forms.TextBox txtAnswer2;
        private System.Windows.Forms.ComboBox cbAsk2;
        private System.Windows.Forms.Label lblanswer2;
        private System.Windows.Forms.Label lblask2;
        private System.Windows.Forms.TextBox txtAnswer3;
        private System.Windows.Forms.ComboBox cbAsk3;
        private System.Windows.Forms.Label lblanswer3;
        private System.Windows.Forms.Label lblask3;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnChange;
        private System.Windows.Forms.Label lbl1;
        private System.Windows.Forms.Label lbl2;
        private System.Windows.Forms.Label lbl3;
        private System.Windows.Forms.Button btnnext;
        private System.Windows.Forms.Button btncancel;
        private System.Windows.Forms.Label lbla1;
        private System.Windows.Forms.Panel pnext;
        private System.Windows.Forms.TextBox txta3;
        private System.Windows.Forms.TextBox txta2;
        private System.Windows.Forms.TextBox txta1;
        private System.Windows.Forms.Label lbla3;
        private System.Windows.Forms.Label lbla2;
        private System.Windows.Forms.Button btnok;
        private System.Windows.Forms.Label lblt2;
        private System.Windows.Forms.Label lblt3;
        private System.Windows.Forms.Label lblt1;
        private System.Windows.Forms.Label lblnone;
        private System.Windows.Forms.Panel pstart;
        private System.Windows.Forms.Button btnyes;
        private System.Windows.Forms.Button btnno;
    }
}