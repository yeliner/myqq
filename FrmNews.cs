﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace myQQ
{
    public partial class FrmNews : Form
    {
        public FrmNews()
        {
            InitializeComponent();
        }
        private void FrmSystemNews_Load(object sender, EventArgs e)
        {
            groupload();
            panel1.Visible = false;
            btnok.Visible = false;

            if (FrmMain.reState == "已读")
            {
                string sqlf = string.Format("select * from [dbo].[Friends] where fqq={0} and Zqq={1}", FrmMain.sendqq,FrmLogin.qq);
                DataSet f = DatabaseHelper.executeDataSet(sqlf);
                if (f.Tables[0].Rows.Count > 0)
                {
                    btnhulue.Visible = false;
                    btnAgree.Text = "已加为好友";
                    btnAgree.Enabled = false;
                }
                else
                {
                    btnAgree.Visible = false;
                    btnhulue.Text = "已忽略";
                    btnAgree.Enabled = false;
                }
            }
            useload();
            if (txtremark.Text.Trim() == "")
            {
                txtremark.Text = lblniname.Text;
            }
        }

        private void useload()
        {
            lblQQ.Text = FrmMain.sendqq.ToString();
            string sql = "  select Niname,Sex,Address,headImage from [dbo].[UserInfo] where qq=" + FrmMain.sendqq;
            DataSet ds = DatabaseHelper.executeDataSet(sql);

            try
            {
                lblask.Text = "附加消息：" + FrmMain.asknews;
                lblniname.Text = ds.Tables[0].Rows[0]["Niname"].ToString();
                lblsex.Text = ds.Tables[0].Rows[0]["Sex"].ToString();
                lblAddress2.Text = "地址" + ds.Tables[0].Rows[0]["Address"].ToString();
                picturehead.BackgroundImage = Image.FromFile(ds.Tables[0].Rows[0]["headImage"].ToString());

            }
            catch (Exception)
            {

                throw;
            }
        }

        private void picturehead_Click(object sender, EventArgs e)
        {
            FrmMain.Lqq = Convert.ToInt32(lblQQ.Text);
            FrmPerson p = new FrmPerson();
            p.btnEdit.Visible = false;
            p.Show();
        }

        private void btnAgree_Click(object sender, EventArgs e)
        {//跳转
            paneltop.Visible =false;
            btnhulue.Visible = false;
            btnAgree.Visible = false;
            btnok.Visible = true;
            panel1.Visible = true;
        }

 
   
        private void groupload()
        {//分组下拉框
            string sql2 = "select Groupname,id from  [dbo].[Group] where Zqq=" + FrmLogin.qq;
            DataSet fz = DatabaseHelper.executeDataSet(sql2);
          
                for (int i = 0; i < fz.Tables[0].Rows.Count; i++)
                {//分组
                    cbgroup.Items.Add(fz.Tables[0].Rows[i]["Groupname"].ToString());
                }
            
        }

        private void lbladdugrop_Click(object sender, EventArgs e)
        {
            this.cbgroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
            cbgroup.Focus();
            lbladdugrop.Text = "";
        }

        private void addgroup()
        {
            if (cbgroup.Text == "")
            {
                cbgroup.Text = "我的同学";
            }
            DataSet gname = DatabaseHelper.executeDataSet(string.Format("select * from [dbo].[Group] where Zqq = {0} and Groupname = '{1}'", FrmLogin.qq, cbgroup.Text));
            if (gname.Tables[0].Rows.Count <= 0)
            {
                string sql = string.Format(" insert into [dbo].[Group](Zqq,Groupname) values({0},'{1}')", FrmLogin.qq, cbgroup.Text);
                int num = DatabaseHelper.executeNonQuery(sql);
                if (num > 0)
                {
                    MessageBox.Show("新建分组成功！");
                }
            }
            else
            {
                DialogResult r = MessageBox.Show("该组名已在使用，是否重新输入组名", "提示", MessageBoxButtons.OKCancel);
                if (r == DialogResult.OK)
                {
                    this.cbgroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
                    cbgroup.Focus();
                }
                if (r == DialogResult.Cancel)
                {
                    this.cbgroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
                    lbladdugrop.Text = "创建分组";
                }
            }
            groupload();
        }

        private void btnhulue_Click(object sender, EventArgs e)
        {
            string sql1 = string.Format(" update [dbo].[News] set RState='已读' where Sendqq={0} and Reciveqq={1} and Context='{2}'", FrmMain.sendqq, FrmLogin.qq, FrmMain.sendqq + "想加你为好友");
            int num1 = DatabaseHelper.executeNonQuery(sql1);
            btnAgree.Visible = false;
            btnhulue.Text = "已忽略";
            btnhulue.Enabled = false;
           
        }

        private void btnChange_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void btnok_Click(object sender, EventArgs e)
        {
            addgroup();
            if (txtremark.Text.Trim() == "")
            {
                txtremark.Text = lblniname.Text;
            }
            if (cbgroup.Text !="")
            {
                string sql3 = string.Format("  select remark,gname  from [dbo].[linshi] where Zqq={0} and askqq={1}", FrmMain.sendqq, FrmLogin.qq);
                DataSet linhsi = DatabaseHelper.executeDataSet(sql3);
                //3.添加到好友表
                string sql4 = string.Format("insert into [dbo].[Friends] (fqq,remark,Groupid,Zqq) values ({0},'{1}',(select id from [dbo].[Group] where Zqq={2} and Groupname='{3}'),{2})",
       FrmMain.Lqq, linhsi.Tables[0].Rows[0]["remark"].ToString(), FrmMain.sendqq, linhsi.Tables[0].Rows[0]["gname"].ToString());
                int addf = DatabaseHelper.executeNonQuery(sql4);

                string sql2 = string.Format("insert into [dbo].[Friends] (fqq,remark,Groupid,Zqq) values ({0},'{1}',(select id from [dbo].[Group] where Zqq={2} and Groupname='{3}'),{2})",
                FrmMain.sendqq, txtremark.Text, FrmLogin.qq, cbgroup.Text);
                int num2 = DatabaseHelper.executeNonQuery(sql2);
                string sql1 = string.Format(" update [dbo].[News] set RState='已读' where Sendqq={0} and Reciveqq={1} and NewsType='{2}' and Context='{3}'", FrmMain.sendqq, FrmLogin.qq, "验证", FrmMain.sendqq + "想加你为好友");
                int num1 = DatabaseHelper.executeNonQuery(sql1);
                string sql = string.Format("insert into [dbo].[News] (Sendqq,Reciveqq ,NewsType,Context,SState,RState,Time) values({0},{1},'回复','{2}','已发送','未读',GETDATE())",
                 FrmLogin.qq, FrmMain.sendqq, FrmMain.sendqq + "已同意加你为好友");
                int num = DatabaseHelper.executeNonQuery(sql);
                if (num2 > 0&& num1 > 0 && num>0&&addf>0)
                {
                        btnok.Enabled = false;
                        btnok.Text = "已加为好友";
                        paneltop.Visible = true;
                        panel1.Visible = false;

                }
            }
        }
        Point mouseOff;//鼠标移动位置变量
        bool leftFlag;//标记是否为左键


        private void FrmNews_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                mouseOff = new Point(-e.X, -e.Y);//得到变量的值
                leftFlag = true;                  //点击左键按下时标注为true;
            }
        }

        private void FrmFrmNews_MouseMove(object sender, MouseEventArgs e)
        {
            if (leftFlag)
            {
                Point mouseSet = Control.MousePosition;
                mouseSet.Offset(mouseOff.X, mouseOff.Y);  //设置移动后的位置
                Location = mouseSet;
            }
        }

        private void FrmFrmNews_MouseUp(object sender, MouseEventArgs e)
        {

            if (leftFlag)
            {
                leftFlag = false;//释放鼠标后标注为false;
            }

        }
    }
}
