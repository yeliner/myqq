﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace myQQ
{
    public partial class FrmChat : Form
    {
        public FrmChat()
        {
            InitializeComponent();
        }
        public static string lujin;
        DateTime lastTime;
        private void FrmChat_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            useload();
            chatload();
        }

        private void chatload()
        {

            string sqlr = string.Format("select Context,Time,Sendqq,Chattype from [dbo].[News] where NewsType='chat'  and ((Sendqq={0} and Reciveqq={1}) or (Sendqq={1} and Reciveqq={0})) ", FrmMain.Lqq, lblQQ.Text);
            DataSet r = DatabaseHelper.executeDataSet(sqlr);
            if (r.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < r.Tables[0].Rows.Count; i++)
                {
                    string text = r.Tables[0].Rows[i]["Sendqq"].ToString() + "\t" + r.Tables[0].Rows[i]["Time"].ToString() + "\n" + r.Tables[0].Rows[i]["Context"].ToString() + "\n"; ;
                    if (r.Tables[0].Rows[i]["Chattype"].ToString() == "0")
                    {
                        text = r.Tables[0].Rows[i]["Sendqq"].ToString() + "\t" + r.Tables[0].Rows[i]["Time"].ToString() + "\n";
                        if (r.Tables[0].Rows[i]["Sendqq"].ToString() == FrmMain.Lqq.ToString())
                        {
                            rtbInfo.SelectionColor = Color.Blue;
                            rtbInfo.SelectionAlignment = HorizontalAlignment.Right;
                            rtbInfo.AppendText(text+" ");

                            Bitmap bmp = new Bitmap(r.Tables[0].Rows[i]["Context"].ToString());
                            Clipboard.SetDataObject(bmp);
                            DataFormats.Format dataFormat = DataFormats.GetFormat(DataFormats.Bitmap);
                            if (rtbInfo.CanPaste(dataFormat))
                            {
                                rtbInfo.Paste();
                            }
                            
                           /* Clipboard.SetDataObject(bmp, false);//将图片放在剪贴板中
                            if (rtbInfo.CanPaste(DataFormats.GetFormat(DataFormats.Bitmap))) { 
                                rtbInfo.Paste();//粘贴数据
                            }*/
                            rtbInfo.AppendText("\n");
                        }
                        if (r.Tables[0].Rows[i]["Sendqq"].ToString() == lblQQ.Text)
                        {
                            rtbInfo.SelectionColor = Color.Red;
                            rtbInfo.SelectionAlignment = HorizontalAlignment.Left;
                            rtbInfo.AppendText(text+" ");
                            Bitmap bmp = new Bitmap(r.Tables[0].Rows[i]["Context"].ToString());
                            Clipboard.SetDataObject(bmp, false);//将图片放在剪贴板中
                            if (rtbInfo.CanPaste(DataFormats.GetFormat(DataFormats.Bitmap))) { 
                                rtbInfo.Paste();//粘贴数据
                        }
                            rtbInfo.AppendText("\n");
                        }
                    }
                    else
                    {
                        if (r.Tables[0].Rows[i]["Sendqq"].ToString() == FrmMain.Lqq.ToString())
                        {
                            rtbInfo.AppendText("\n");
                            rtbInfo.SelectionColor = Color.Blue;
                            rtbInfo.SelectionAlignment = HorizontalAlignment.Right;
                            rtbInfo.AppendText(text);
                        }
                        if (r.Tables[0].Rows[i]["Sendqq"].ToString() == lblQQ.Text)
                        {
                            rtbInfo.AppendText("\n");
                            rtbInfo.SelectionColor = Color.Red;
                            rtbInfo.SelectionAlignment = HorizontalAlignment.Left;
                            rtbInfo.AppendText(text);
                        }
                    }
                   
                    rtbInfo.Focus();
                    rtbInfo.SelectionStart = rtbInfo.Text.Length;
                    rtbInfo.ScrollToCaret();
                    lastTime = (DateTime)r.Tables[0].Rows[i]["Time"];
                }
                string sqlu = string.Format(" update [dbo].[News] set RState='已读' where  NewsType='chat' and Sendqq={0} and Reciveqq={1} ", lblQQ.Text, FrmMain.Lqq);
                DataSet u = DatabaseHelper.executeDataSet(sqlu);
            }

        }
        
        private void useload()
        {
            lblQQ.Text = FrmMain.chatqq.ToString();
            string sql = "select Niname,Sex,Address,headImage from UserInfo where qq=" + FrmMain.chatqq;
            DataSet ds = DatabaseHelper.executeDataSet(sql);
            try
            {
                lblniname.Text = ds.Tables[0].Rows[0]["Niname"].ToString();
                picturehead.BackgroundImage = Image.FromFile(ds.Tables[0].Rows[0]["headImage"].ToString());

            }
            catch (Exception)
            {

                throw;
            }
        }
        private void btnChange_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Hide();

        }
        Point mouseOff;//鼠标移动位置变量
        bool leftFlag;//标记是否为左键


        private void FrmChat_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                mouseOff = new Point(-e.X, -e.Y);//得到变量的值
                leftFlag = true;                  //点击左键按下时标注为true;
            }
        }

        private void FrmChat_MouseMove(object sender, MouseEventArgs e)
        {
            if (leftFlag)
            {
                Point mouseSet = Control.MousePosition;
                mouseSet.Offset(mouseOff.X, mouseOff.Y);  //设置移动后的位置
                Location = mouseSet;
            }
        }

        private void FrmChat_MouseUp(object sender, MouseEventArgs e)
        {

            if (leftFlag)
            {
                leftFlag = false;//释放鼠标后标注为false;
            }

        }

        private void btnclose_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void picturehead_Click(object sender, EventArgs e)
        {
            FrmPerson p = new FrmPerson();
            p.btnEdit.Visible = false;
            p.Show();
        }

        private void btnsend_Click(object sender, EventArgs e)
        {
            string sql;
            if (lujin==null)
            {
                sql = string.Format(" insert into [dbo].[News](Sendqq,Reciveqq,NewsType,Context,RState,SState,Time,Chattype) values ({0},{1},'chat','{2}','未读','已发送','{3}',1)"
            , FrmMain.Lqq, FrmMain.chatqq, rtxtWritenews.Text, DateTime.Now.ToLocalTime());
            }
            else
            { sql = string.Format(" insert into [dbo].[News](Sendqq,Reciveqq,NewsType,Context,RState,SState,Time,Chattype) values ({0},{1},'chat','{2}','未读','已发送','{3}',{4})"
            , FrmMain.Lqq, FrmMain.chatqq, lujin, DateTime.Now.ToLocalTime(), 0);
            } 
            int num = DatabaseHelper.executeNonQuery(sql);
            if (num > 0)
            {
                rtxtWritenews.ReadOnly = false;
                rtxtWritenews.Text = "";
                rtxtWritenews.Focus();
            }
        }
        private void btnbiaoqin_Click(object sender, EventArgs e)
        {
            OpenFileDialog fimage = new OpenFileDialog();
            fimage.InitialDirectory =@"D:\专业\C#\myQQ\img\Face";
            fimage.ShowDialog();
            Clipboard.SetDataObject(Image.FromFile(fimage.FileName), false);
            rtxtWritenews.Paste();
            lujin = fimage.FileName;
        }

        private void btnfont_Click(object sender, EventArgs e)
        {
            FontDialog font = new FontDialog();
            font.ShowDialog();
            rtxtWritenews.Font = font.Font;
            rtbInfo.Font = font.Font;
        }

        private void btnimg_Click(object sender, EventArgs e)
        {
            OpenFileDialog fimage = new OpenFileDialog();
            fimage.Filter = "图片|*.png";
            fimage.ShowDialog();
            if (fimage.FileName != "")
            {
                Bitmap bmp = new Bitmap(fimage.FileName);
                Clipboard.SetDataObject(bmp, false);//将图片放在剪贴板中
                if (rtxtWritenews.CanPaste(DataFormats.GetFormat(DataFormats.Bitmap)))
                    rtxtWritenews.Paste();//粘贴数据
            }

        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            string sqlt = string.Format("select Context,Sendqq,NewsType,Chattype,Time from [dbo].[News] where NewsType='chat' and ((Sendqq={0} and Reciveqq={1}) or (Sendqq={1} and Reciveqq={0})) and Time>'{2}' order by Time desc ", FrmMain.Lqq, lblQQ.Text, lastTime);
            DataSet times = DatabaseHelper.executeDataSet(sqlt);
            if (times.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < times.Tables[0].Rows.Count; i++)
                {
                    string text = times.Tables[0].Rows[i]["Sendqq"].ToString() + "\t" + times.Tables[0].Rows[i]["Time"].ToString() + "\n" + times.Tables[0].Rows[i]["Context"].ToString() + "\n";
                    if (times.Tables[0].Rows[i]["Chattype"].ToString() == "0")

                    {
                        text = times.Tables[0].Rows[i]["Sendqq"].ToString() + "\t" + times.Tables[0].Rows[i]["Time"].ToString() + "\n";
                        Bitmap bmp = new Bitmap(times.Tables[0].Rows[i]["Context"].ToString());
                        {
                        Clipboard.SetDataObject(bmp, false);//将图片放在剪贴板中
                        }
                        if (rtbInfo.CanPaste(DataFormats.GetFormat(DataFormats.Bitmap)))
                            rtbInfo.Focus();

                    }
                    if (times.Tables[0].Rows[i]["Sendqq"].ToString() == FrmMain.Lqq.ToString())
                    {
                        rtbInfo.AppendText("\n");
                        rtbInfo.SelectionColor = Color.Blue;
                        rtbInfo.AppendText(text);
                    }
                    if (times.Tables[0].Rows[i]["Sendqq"].ToString() == lblQQ.Text)
                    {
                        rtbInfo.AppendText("\n");
                        rtbInfo.SelectionColor = Color.Red;
                        rtbInfo.AppendText(text);
                    }
                    rtbInfo.Focus();
                    rtbInfo.SelectionStart = rtbInfo.Text.Length;
                    rtbInfo.ScrollToCaret(); 
                    lastTime = (DateTime)times.Tables[0].Rows[i]["Time"];
                    string sqlu = string.Format(" update [dbo].[News] set RState='已读' where  NewsType='chat' and Sendqq={0} and Reciveqq={1} ", lblQQ.Text, FrmMain.Lqq);
                    DataSet u = DatabaseHelper.executeDataSet(sqlu);
                }

            }

        }
    }
}
