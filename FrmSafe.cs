﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace myQQ
{
    public partial class FrmSafe : Form
    {
        public FrmSafe()
        {
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Hide();
          
        }

        private void btnChange_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        private void btnnext_Click(object sender, EventArgs e)
        {

            lblhead.Text = "以下是您刚刚设置的密保问题，请依次做出回答，以便进行确认。";
            btnnext.Visible = false;
            btnok.Visible = true;
            pnext.Visible = true;
            lbla1.Text = cbAsk1.Text;
            lbla2.Text = cbAsk2.Text;
            lbla3.Text = cbAsk3.Text;
        }
        private void btncancel_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void btnok_Click(object sender, EventArgs e)
        {
            if (boolanswer())
            {
                string sql =string .Format ( "  insert into safe (qq,Ask1,Answer1, Ask2,Answer2, Ask3,Answer3) values ({0},'{1}','{2}','{3}','{4}','{5}','{6}');"
                    ,FrmMain.Lqq,cbAsk1.Text,txtAnswer1.Text.Trim(),cbAsk2.Text.Trim(),txtAnswer2.Text.Trim(),cbAsk3.Text.Trim(),txtAnswer3.Text.Trim());
                int num = DatabaseHelper.executeNonQuery(sql);
                if (num>0)
                {
                    MessageBox.Show("密保成功","提示");
                    this.Hide();
                }
            }

        }

        private bool boolanswer()
        {
            lblt1.Visible = false; lblt2.Visible = false; lblt3.Visible = false;
            if (txta1.Text!=txtAnswer1.Text)
            {
                lblt1.Visible = true;
                txta1.Focus();
                return false;
            }
            if (txta2.Text != txtAnswer2.Text)
            {
                lblt2.Visible = true;
                txta2.Focus();
                return false;
            }
            if (txta3.Text != txtAnswer3.Text)
            {
                lblt3.Visible = true;
                txta1.Focus();
                return false;
            }
            return true;
        }

        private void cbAsk1_TextChanged(object sender, EventArgs e)
        {
            lbl1.Text = "请填写2-19个中文或3-38个英文";
            lbl1.Visible = true;
            if (cbAsk1.Text== "您配偶的生日是？"|| cbAsk1.Text == "您母亲的生日是？"|| cbAsk1.Text == "您父亲的生日是？")
            {
                lbl1.Text = "请填写日期，例如20080619";
            }
            if (cbAsk1.Text == "您的学号（工号）是？")
            {
                lbl1.Text = "请填写2至16个阿拉伯数字";
            }
            cbAsk2.Items.Clear();
            foreach (string item in cbAsk1.Items)
            {
                if (item==cbAsk1.Text)
                {
                    continue;
                }
                cbAsk2.Items.Add(item);
            }
            
        }

        private void cbAsk2_TextChanged(object sender, EventArgs e)
        {
            lbl2.Text = "请填写2-19个中文或3-38个英文";
            lbl2.Visible = true;
            if (cbAsk2.Text == "您配偶的生日是？" || cbAsk2.Text == "您母亲的生日是？" || cbAsk2.Text == "您父亲的生日是？")
            {
                lbl2.Text = "请填写日期，例如20080619";
            }
            if (cbAsk2.Text == "您的学号（工号）是？")
            {
                lbl2.Text = "请填写2至16个阿拉伯数字";
            }
            cbAsk3.Items.Clear();
            foreach (string item in cbAsk1.Items)
            {
                if (item == cbAsk2.Text|| item == cbAsk1.Text)
                {
                    continue;
                }
                cbAsk3.Items.Add(item);
            }
          
        }

        private void cbAsk3_TextChanged(object sender, EventArgs e)
        {
            
            lbl3.Text = "请填写2-19个中文或3-38个英文";
            lbl3.Visible = true;
            if (cbAsk3.Text == "您配偶的生日是？" || cbAsk3.Text == "您母亲的生日是？" || cbAsk3.Text == "您父亲的生日是？")
            {
                lbl3.Text = "请填写日期，例如20080619";
            }
            if (cbAsk3.Text == "您的学号（工号）是？")
            {
                lbl3.Text = "请填写2至16个阿拉伯数字";
            }
        }

        private void txtAnswer1_TextChanged(object sender, EventArgs e)
        {
            btnnext.Visible = false;
            lbl1.Visible = true;
            if ( (cbAsk1.Text == "您配偶的生日是？" || cbAsk1.Text == "您母亲的生日是？" || cbAsk1.Text == "您父亲的生日是？") && txtAnswer1.Text.Length==8)
            {
                try
                {
                    Convert.ToInt32(txtAnswer1.Text);
                    lbl1.Visible = false;
                }
                catch (Exception)
                {
                    throw;
                }
            }
            if (cbAsk1.Text == "您的学号（工号）是？"&&txtAnswer1.Text.Length<=16&& txtAnswer1.Text.Length >=2)
            {
                try
                {
                    Convert.ToInt32(txtAnswer1.Text);
                    lbl1.Visible = false;
                }
                catch (Exception)
                {
                    throw;
                }
            }
            else
            {
                try
                {
                    Convert.ToInt32(txtAnswer1.Text);
                }
                catch (Exception)
                {

                    if (txtAnswer1.Text.Length > 0 && txtAnswer1.Text.Length <= 19)
                    {
                        lbl1.Visible = false;
                    }
                }
               
            }
        }

        private void FrmSafe_Load(object sender, EventArgs e)
        {
            lblnone.Visible = false;
            string sql = "  select * from [dbo].[safe] where qq="+FrmMain.Lqq;
            DataSet ds = DatabaseHelper.executeDataSet(sql);
            try
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    lblnone.Visible = true;
                    btnyes.Visible = true;
                    btnno.Visible = true;
                    pstart.Visible = false;
                    lblhead.Visible = false;
                    btncancel.Visible = false;
                    btnnext.Visible = false;
                    pnext.Visible = false;
                }
            }
            catch (Exception)
            {
            }
            
        }
        private void btnyes_Click(object sender, EventArgs e)
        {
            string sql = "delete safe where qq=" + FrmMain.Lqq;
            int d = DatabaseHelper.executeNonQuery(sql);
            if (d>0)
            {
                btnno.Visible = false;
                pstart.Visible = true;
                lblhead.Visible = true;
                btncancel.Visible = true;
                btnnext.Visible = true;
            }
        }

        private void txtAnswer2_TextChanged(object sender, EventArgs e)
        {
            btnnext.Visible = false;
            lbl2.Visible = true;
            if ((cbAsk2.Text == "您配偶的生日是？" || cbAsk2.Text == "您母亲的生日是？" || cbAsk2.Text == "您父亲的生日是？") && txtAnswer2.Text.Length == 8)
            {
                try
                {
                    Convert.ToInt32(txtAnswer2.Text);
                    lbl2.Visible = false;
                }
                catch (Exception)
                {
                    throw;
                }
            }
            if (cbAsk2.Text == "您的学号（工号）是？" && txtAnswer2.Text.Length <= 16 && txtAnswer2.Text.Length >= 2)
            {
                try
                {
                    Convert.ToInt32(txtAnswer2.Text);
                    lbl2.Visible = false;
                }
                catch (Exception)
                {
                    throw;
                }
            }
            else
            {
                try
                {
                    Convert.ToInt32(txtAnswer2.Text);
                }
                catch (Exception)
                {

                    if (txtAnswer2.Text.Length > 0 && txtAnswer2.Text.Length <= 19)
                    {
                        lbl2.Visible = false;
                    }
                }
            }
        }

        private void txtAnswer3_TextChanged(object sender, EventArgs e)
        {
            btnnext.Visible = false;
            lbl3.Visible = true;
            if ((cbAsk3.Text == "您配偶的生日是？" || cbAsk3.Text == "您母亲的生日是？" || cbAsk3.Text == "您父亲的生日是？") && txtAnswer3.Text.Length == 8)
            {
                try
                {
                    Convert.ToInt32(txtAnswer3.Text);
                    lbl3.Visible = false;
                    btnnext.Visible = true;
                }
                catch (Exception)
                {
                    throw;
                }
            }
            if (cbAsk3.Text == "您的学号（工号）是？" && txtAnswer3.Text.Length <= 16 && txtAnswer3.Text.Length >= 2)
            {
                try
                {
                    Convert.ToInt32(txtAnswer3.Text);
                    lbl3.Visible = false;
                    btnnext.Visible = true;
                }
                catch (Exception)
                {
                    throw;
                }
            }
            else
            {
                try
                {
                    Convert.ToInt32(txtAnswer3.Text);
                }
                catch (Exception)
                {

                    if (txtAnswer3.Text.Length > 0 && txtAnswer3.Text.Length <= 19)
                    {
                        lbl3.Visible = false;
                        btnnext.Visible = true;
                    }
                }
            }
        }
        Point mouseOff;//鼠标移动位置变量
        bool leftFlag;//标记是否为左键


        private void FrmSafe_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                mouseOff = new Point(-e.X, -e.Y);//得到变量的值
                leftFlag = true;                  //点击左键按下时标注为true;
            }
        }

        private void FrmSafe_MouseMove(object sender, MouseEventArgs e)
        {
            if (leftFlag)
            {
                Point mouseSet = Control.MousePosition;
                mouseSet.Offset(mouseOff.X, mouseOff.Y);  //设置移动后的位置
                Location = mouseSet;
            }
        }

        private void FrmSafe_MouseUp(object sender, MouseEventArgs e)
        {

            if (leftFlag)
            {
                leftFlag = false;//释放鼠标后标注为false;
            }

        }

        
    }
}
