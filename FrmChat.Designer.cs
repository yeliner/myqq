﻿namespace myQQ
{
    partial class FrmChat
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.rtbInfo = new System.Windows.Forms.RichTextBox();
            this.btnbiaoqin = new System.Windows.Forms.Button();
            this.rtxtWritenews = new System.Windows.Forms.RichTextBox();
            this.btnimg = new System.Windows.Forms.Button();
            this.btnfont = new System.Windows.Forms.Button();
            this.btnclose = new System.Windows.Forms.Button();
            this.btnsend = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.cmsfriend = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmiclear = new System.Windows.Forms.ToolStripMenuItem();
            this.paneltop = new System.Windows.Forms.Panel();
            this.lblautograph = new System.Windows.Forms.Label();
            this.lblQQ = new System.Windows.Forms.Label();
            this.lblniname = new System.Windows.Forms.Label();
            this.picturehead = new System.Windows.Forms.PictureBox();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnChange = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.cmsfriend.SuspendLayout();
            this.paneltop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picturehead)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.BackColor = System.Drawing.Color.White;
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.splitContainer1.Location = new System.Drawing.Point(4, 87);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.AutoScroll = true;
            this.splitContainer1.Panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(235)))), ((int)(((byte)(253)))));
            this.splitContainer1.Panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.splitContainer1.Panel1.Controls.Add(this.rtbInfo);
            this.splitContainer1.Panel1.Cursor = System.Windows.Forms.Cursors.Default;
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.AutoScroll = true;
            this.splitContainer1.Panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(235)))), ((int)(((byte)(253)))));
            this.splitContainer1.Panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.splitContainer1.Panel2.Controls.Add(this.btnbiaoqin);
            this.splitContainer1.Panel2.Controls.Add(this.rtxtWritenews);
            this.splitContainer1.Panel2.Controls.Add(this.btnimg);
            this.splitContainer1.Panel2.Controls.Add(this.btnfont);
            this.splitContainer1.Size = new System.Drawing.Size(543, 448);
            this.splitContainer1.SplitterDistance = 234;
            this.splitContainer1.SplitterWidth = 1;
            this.splitContainer1.TabIndex = 3;
            // 
            // rtbInfo
            // 
            this.rtbInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(235)))), ((int)(((byte)(253)))));
            this.rtbInfo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtbInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtbInfo.Location = new System.Drawing.Point(0, 0);
            this.rtbInfo.Name = "rtbInfo";
            this.rtbInfo.ReadOnly = true;
            this.rtbInfo.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.rtbInfo.Size = new System.Drawing.Size(541, 232);
            this.rtbInfo.TabIndex = 0;
            this.rtbInfo.Text = "";
            // 
            // btnbiaoqin
            // 
            this.btnbiaoqin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnbiaoqin.FlatAppearance.BorderSize = 0;
            this.btnbiaoqin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnbiaoqin.Location = new System.Drawing.Point(91, 7);
            this.btnbiaoqin.Name = "btnbiaoqin";
            this.btnbiaoqin.Size = new System.Drawing.Size(40, 20);
            this.btnbiaoqin.TabIndex = 1;
            this.btnbiaoqin.Text = "表情";
            this.btnbiaoqin.UseVisualStyleBackColor = true;
            this.btnbiaoqin.Click += new System.EventHandler(this.btnbiaoqin_Click);
            // 
            // rtxtWritenews
            // 
            this.rtxtWritenews.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(235)))), ((int)(((byte)(253)))));
            this.rtxtWritenews.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtxtWritenews.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.rtxtWritenews.Location = new System.Drawing.Point(0, 54);
            this.rtxtWritenews.Name = "rtxtWritenews";
            this.rtxtWritenews.Size = new System.Drawing.Size(541, 157);
            this.rtxtWritenews.TabIndex = 0;
            this.rtxtWritenews.Text = "";
            // 
            // btnimg
            // 
            this.btnimg.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnimg.FlatAppearance.BorderSize = 0;
            this.btnimg.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnimg.Location = new System.Drawing.Point(45, 7);
            this.btnimg.Name = "btnimg";
            this.btnimg.Size = new System.Drawing.Size(40, 20);
            this.btnimg.TabIndex = 2;
            this.btnimg.Text = "图片";
            this.btnimg.UseVisualStyleBackColor = true;
            this.btnimg.Click += new System.EventHandler(this.btnimg_Click);
            // 
            // btnfont
            // 
            this.btnfont.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnfont.FlatAppearance.BorderSize = 0;
            this.btnfont.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnfont.Location = new System.Drawing.Point(-1, 7);
            this.btnfont.Name = "btnfont";
            this.btnfont.Size = new System.Drawing.Size(40, 20);
            this.btnfont.TabIndex = 3;
            this.btnfont.Text = "字体";
            this.btnfont.UseVisualStyleBackColor = true;
            this.btnfont.Click += new System.EventHandler(this.btnfont_Click);
            // 
            // btnclose
            // 
            this.btnclose.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnclose.BackColor = System.Drawing.Color.PaleTurquoise;
            this.btnclose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnclose.Location = new System.Drawing.Point(378, 540);
            this.btnclose.Name = "btnclose";
            this.btnclose.Size = new System.Drawing.Size(73, 27);
            this.btnclose.TabIndex = 4;
            this.btnclose.Text = "关闭";
            this.btnclose.UseVisualStyleBackColor = false;
            this.btnclose.Click += new System.EventHandler(this.btnclose_Click);
            // 
            // btnsend
            // 
            this.btnsend.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnsend.BackColor = System.Drawing.Color.PaleTurquoise;
            this.btnsend.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnsend.Location = new System.Drawing.Point(470, 540);
            this.btnsend.Name = "btnsend";
            this.btnsend.Size = new System.Drawing.Size(73, 27);
            this.btnsend.TabIndex = 5;
            this.btnsend.Text = "发送";
            this.btnsend.UseVisualStyleBackColor = false;
            this.btnsend.Click += new System.EventHandler(this.btnsend_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.Text = "notifyIcon1";
            this.notifyIcon1.Visible = true;
            // 
            // cmsfriend
            // 
            this.cmsfriend.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiclear});
            this.cmsfriend.Name = "cmsgroup";
            this.cmsfriend.Size = new System.Drawing.Size(149, 26);
            // 
            // tsmiclear
            // 
            this.tsmiclear.Name = "tsmiclear";
            this.tsmiclear.Size = new System.Drawing.Size(148, 22);
            this.tsmiclear.Text = "清空聊天记录";
            // 
            // paneltop
            // 
            this.paneltop.BackColor = System.Drawing.Color.Transparent;
            this.paneltop.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.paneltop.Controls.Add(this.lblautograph);
            this.paneltop.Controls.Add(this.lblQQ);
            this.paneltop.Controls.Add(this.lblniname);
            this.paneltop.Controls.Add(this.picturehead);
            this.paneltop.Controls.Add(this.btnExit);
            this.paneltop.Controls.Add(this.btnChange);
            this.paneltop.Location = new System.Drawing.Point(4, 3);
            this.paneltop.Name = "paneltop";
            this.paneltop.Size = new System.Drawing.Size(543, 81);
            this.paneltop.TabIndex = 2;
            this.paneltop.MouseDown += new System.Windows.Forms.MouseEventHandler(this.FrmChat_MouseDown);
            this.paneltop.MouseMove += new System.Windows.Forms.MouseEventHandler(this.FrmChat_MouseMove);
            this.paneltop.MouseUp += new System.Windows.Forms.MouseEventHandler(this.FrmChat_MouseUp);
            // 
            // lblautograph
            // 
            this.lblautograph.AutoSize = true;
            this.lblautograph.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblautograph.Location = new System.Drawing.Point(71, 40);
            this.lblautograph.Name = "lblautograph";
            this.lblautograph.Size = new System.Drawing.Size(35, 14);
            this.lblautograph.TabIndex = 43;
            this.lblautograph.Text = "个性";
            this.lblautograph.Visible = false;
            // 
            // lblQQ
            // 
            this.lblQQ.AutoSize = true;
            this.lblQQ.Location = new System.Drawing.Point(72, 54);
            this.lblQQ.Name = "lblQQ";
            this.lblQQ.Size = new System.Drawing.Size(0, 12);
            this.lblQQ.TabIndex = 32;
            // 
            // lblniname
            // 
            this.lblniname.AutoSize = true;
            this.lblniname.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblniname.Location = new System.Drawing.Point(71, 12);
            this.lblniname.Name = "lblniname";
            this.lblniname.Size = new System.Drawing.Size(28, 14);
            this.lblniname.TabIndex = 26;
            this.lblniname.Text = "nic";
            // 
            // picturehead
            // 
            this.picturehead.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.picturehead.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.picturehead.Location = new System.Drawing.Point(3, 9);
            this.picturehead.Name = "picturehead";
            this.picturehead.Size = new System.Drawing.Size(63, 62);
            this.picturehead.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picturehead.TabIndex = 23;
            this.picturehead.TabStop = false;
            this.picturehead.Click += new System.EventHandler(this.picturehead_Click);
            // 
            // btnExit
            // 
            this.btnExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExit.BackColor = System.Drawing.Color.Transparent;
            this.btnExit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnExit.FlatAppearance.BorderSize = 0;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Image = global::myQQ.Properties.Resources.退出;
            this.btnExit.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnExit.Location = new System.Drawing.Point(510, 7);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(30, 26);
            this.btnExit.TabIndex = 22;
            this.btnExit.UseCompatibleTextRendering = true;
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnChange
            // 
            this.btnChange.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnChange.BackColor = System.Drawing.Color.Transparent;
            this.btnChange.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnChange.FlatAppearance.BorderSize = 0;
            this.btnChange.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnChange.Image = global::myQQ.Properties.Resources.最小化;
            this.btnChange.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnChange.Location = new System.Drawing.Point(482, 7);
            this.btnChange.Name = "btnChange";
            this.btnChange.Size = new System.Drawing.Size(30, 26);
            this.btnChange.TabIndex = 21;
            this.btnChange.UseCompatibleTextRendering = true;
            this.btnChange.UseVisualStyleBackColor = false;
            this.btnChange.Click += new System.EventHandler(this.btnChange_Click);
            // 
            // FrmChat
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(235)))), ((int)(((byte)(253)))));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(562, 578);
            this.Controls.Add(this.btnclose);
            this.Controls.Add(this.btnsend);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.paneltop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmChat";
            this.Text = "FrmMessage";
            this.Load += new System.EventHandler(this.FrmChat_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.FrmChat_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.FrmChat_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.FrmChat_MouseUp);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.cmsfriend.ResumeLayout(false);
            this.paneltop.ResumeLayout(false);
            this.paneltop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picturehead)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel paneltop;
        private System.Windows.Forms.Label lblautograph;
        private System.Windows.Forms.Label lblniname;
        private System.Windows.Forms.PictureBox picturehead;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnChange;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.RichTextBox rtxtWritenews;
        private System.Windows.Forms.Button btnclose;
        private System.Windows.Forms.Button btnsend;
        private System.Windows.Forms.Button btnbiaoqin;
        private System.Windows.Forms.Button btnfont;
        private System.Windows.Forms.Button btnimg;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.RichTextBox rtbInfo;
        public System.Windows.Forms.Label lblQQ;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.ContextMenuStrip cmsfriend;
        private System.Windows.Forms.ToolStripMenuItem tsmiclear;
    }
}