﻿namespace myQQ
{
    partial class FrmPerson
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.paneltop = new System.Windows.Forms.Panel();
            this.txtAutograph = new System.Windows.Forms.TextBox();
            this.lblQQ = new System.Windows.Forms.Label();
            this.lblniname = new System.Windows.Forms.Label();
            this.picturehead = new System.Windows.Forms.PictureBox();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnChange = new System.Windows.Forms.Button();
            this.lblQQ1 = new System.Windows.Forms.Label();
            this.lblniname1 = new System.Windows.Forms.Label();
            this.lblperson = new System.Windows.Forms.Label();
            this.lblAddress = new System.Windows.Forms.Label();
            this.lblQQ2 = new System.Windows.Forms.Label();
            this.lblAddress2 = new System.Windows.Forms.Label();
            this.lblsex = new System.Windows.Forms.Label();
            this.lblniname2 = new System.Windows.Forms.Label();
            this.lblbrith = new System.Windows.Forms.Label();
            this.btnEdit = new System.Windows.Forms.Button();
            this.paneltop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picturehead)).BeginInit();
            this.SuspendLayout();
            // 
            // paneltop
            // 
            this.paneltop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(154)))), ((int)(((byte)(218)))));
            this.paneltop.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.paneltop.Controls.Add(this.txtAutograph);
            this.paneltop.Controls.Add(this.lblQQ);
            this.paneltop.Controls.Add(this.lblniname);
            this.paneltop.Controls.Add(this.picturehead);
            this.paneltop.Controls.Add(this.btnExit);
            this.paneltop.Controls.Add(this.btnChange);
            this.paneltop.Location = new System.Drawing.Point(0, 0);
            this.paneltop.Name = "paneltop";
            this.paneltop.Size = new System.Drawing.Size(459, 146);
            this.paneltop.TabIndex = 1;
            this.paneltop.MouseDown += new System.Windows.Forms.MouseEventHandler(this.FrmPerson_MouseDown);
            this.paneltop.MouseMove += new System.Windows.Forms.MouseEventHandler(this.FrmPerson_MouseMove);
            this.paneltop.MouseUp += new System.Windows.Forms.MouseEventHandler(this.FrmPerson_MouseUp);
            // 
            // txtAutograph
            // 
            this.txtAutograph.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(154)))), ((int)(((byte)(218)))));
            this.txtAutograph.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtAutograph.ForeColor = System.Drawing.Color.White;
            this.txtAutograph.Location = new System.Drawing.Point(70, 80);
            this.txtAutograph.Name = "txtAutograph";
            this.txtAutograph.Size = new System.Drawing.Size(100, 14);
            this.txtAutograph.TabIndex = 42;
            // 
            // lblQQ
            // 
            this.lblQQ.AutoSize = true;
            this.lblQQ.Location = new System.Drawing.Point(72, 54);
            this.lblQQ.Name = "lblQQ";
            this.lblQQ.Size = new System.Drawing.Size(0, 12);
            this.lblQQ.TabIndex = 32;
            // 
            // lblniname
            // 
            this.lblniname.AutoSize = true;
            this.lblniname.Location = new System.Drawing.Point(81, 42);
            this.lblniname.Name = "lblniname";
            this.lblniname.Size = new System.Drawing.Size(0, 12);
            this.lblniname.TabIndex = 26;
            // 
            // picturehead
            // 
            this.picturehead.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.picturehead.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.picturehead.Location = new System.Drawing.Point(3, 32);
            this.picturehead.Name = "picturehead";
            this.picturehead.Size = new System.Drawing.Size(63, 62);
            this.picturehead.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picturehead.TabIndex = 23;
            this.picturehead.TabStop = false;
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(154)))), ((int)(((byte)(218)))));
            this.btnExit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnExit.FlatAppearance.BorderSize = 0;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Image = global::myQQ.Properties.Resources.退出;
            this.btnExit.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnExit.Location = new System.Drawing.Point(423, 0);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(26, 26);
            this.btnExit.TabIndex = 22;
            this.btnExit.UseCompatibleTextRendering = true;
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnChange
            // 
            this.btnChange.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(154)))), ((int)(((byte)(218)))));
            this.btnChange.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnChange.FlatAppearance.BorderSize = 0;
            this.btnChange.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnChange.Image = global::myQQ.Properties.Resources.最小化;
            this.btnChange.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnChange.Location = new System.Drawing.Point(391, 0);
            this.btnChange.Name = "btnChange";
            this.btnChange.Size = new System.Drawing.Size(26, 26);
            this.btnChange.TabIndex = 21;
            this.btnChange.UseCompatibleTextRendering = true;
            this.btnChange.UseVisualStyleBackColor = false;
            this.btnChange.Click += new System.EventHandler(this.btnChange_Click);
            // 
            // lblQQ1
            // 
            this.lblQQ1.AutoSize = true;
            this.lblQQ1.Location = new System.Drawing.Point(12, 159);
            this.lblQQ1.Name = "lblQQ1";
            this.lblQQ1.Size = new System.Drawing.Size(65, 12);
            this.lblQQ1.TabIndex = 33;
            this.lblQQ1.Text = "账    号：";
            // 
            // lblniname1
            // 
            this.lblniname1.AutoSize = true;
            this.lblniname1.Location = new System.Drawing.Point(12, 181);
            this.lblniname1.Name = "lblniname1";
            this.lblniname1.Size = new System.Drawing.Size(65, 12);
            this.lblniname1.TabIndex = 34;
            this.lblniname1.Text = "昵    称：";
            // 
            // lblperson
            // 
            this.lblperson.AutoSize = true;
            this.lblperson.Location = new System.Drawing.Point(12, 205);
            this.lblperson.Name = "lblperson";
            this.lblperson.Size = new System.Drawing.Size(65, 12);
            this.lblperson.TabIndex = 35;
            this.lblperson.Text = "个    人：";
            // 
            // lblAddress
            // 
            this.lblAddress.AutoSize = true;
            this.lblAddress.Location = new System.Drawing.Point(12, 231);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(65, 12);
            this.lblAddress.TabIndex = 36;
            this.lblAddress.Text = "所 在 地：";
            // 
            // lblQQ2
            // 
            this.lblQQ2.AutoSize = true;
            this.lblQQ2.Location = new System.Drawing.Point(84, 159);
            this.lblQQ2.Name = "lblQQ2";
            this.lblQQ2.Size = new System.Drawing.Size(0, 12);
            this.lblQQ2.TabIndex = 38;
            // 
            // lblAddress2
            // 
            this.lblAddress2.AutoSize = true;
            this.lblAddress2.Location = new System.Drawing.Point(83, 231);
            this.lblAddress2.Name = "lblAddress2";
            this.lblAddress2.Size = new System.Drawing.Size(0, 12);
            this.lblAddress2.TabIndex = 41;
            // 
            // lblsex
            // 
            this.lblsex.AutoSize = true;
            this.lblsex.Location = new System.Drawing.Point(83, 205);
            this.lblsex.Name = "lblsex";
            this.lblsex.Size = new System.Drawing.Size(0, 12);
            this.lblsex.TabIndex = 40;
            // 
            // lblniname2
            // 
            this.lblniname2.AutoSize = true;
            this.lblniname2.Location = new System.Drawing.Point(83, 181);
            this.lblniname2.Name = "lblniname2";
            this.lblniname2.Size = new System.Drawing.Size(0, 12);
            this.lblniname2.TabIndex = 39;
            // 
            // lblbrith
            // 
            this.lblbrith.AutoSize = true;
            this.lblbrith.Location = new System.Drawing.Point(170, 205);
            this.lblbrith.Name = "lblbrith";
            this.lblbrith.Size = new System.Drawing.Size(0, 12);
            this.lblbrith.TabIndex = 43;
            // 
            // btnEdit
            // 
            this.btnEdit.Location = new System.Drawing.Point(374, 360);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(75, 23);
            this.btnEdit.TabIndex = 44;
            this.btnEdit.Text = "编辑资料";
            this.btnEdit.UseVisualStyleBackColor = true;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // FrmPerson
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(459, 385);
            this.Controls.Add(this.btnEdit);
            this.Controls.Add(this.lblbrith);
            this.Controls.Add(this.lblAddress2);
            this.Controls.Add(this.lblsex);
            this.Controls.Add(this.lblniname2);
            this.Controls.Add(this.lblQQ2);
            this.Controls.Add(this.lblAddress);
            this.Controls.Add(this.lblperson);
            this.Controls.Add(this.lblniname1);
            this.Controls.Add(this.lblQQ1);
            this.Controls.Add(this.paneltop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmPerson";
            this.Text = "FrmPerson";
            this.Load += new System.EventHandler(this.FrmPerson_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.FrmPerson_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.FrmPerson_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.FrmPerson_MouseUp);
            this.paneltop.ResumeLayout(false);
            this.paneltop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picturehead)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel paneltop;
        private System.Windows.Forms.Label lblniname;
        private System.Windows.Forms.PictureBox picturehead;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnChange;
        private System.Windows.Forms.Label lblQQ;
        private System.Windows.Forms.Label lblQQ1;
        private System.Windows.Forms.Label lblniname1;
        private System.Windows.Forms.Label lblperson;
        private System.Windows.Forms.Label lblAddress;
        private System.Windows.Forms.Label lblQQ2;
        private System.Windows.Forms.Label lblAddress2;
        private System.Windows.Forms.Label lblsex;
        private System.Windows.Forms.Label lblniname2;
        private System.Windows.Forms.Label lblbrith;
        private System.Windows.Forms.TextBox txtAutograph;
        public System.Windows.Forms.Button btnEdit;
    }
}