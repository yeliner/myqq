﻿namespace myQQ
{
    partial class FrmNews
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.paneltop = new System.Windows.Forms.Panel();
            this.lblask = new System.Windows.Forms.Label();
            this.lblsex = new System.Windows.Forms.Label();
            this.lblQQ = new System.Windows.Forms.Label();
            this.lblniname = new System.Windows.Forms.Label();
            this.picturehead = new System.Windows.Forms.PictureBox();
            this.lblAddress2 = new System.Windows.Forms.Label();
            this.btnhulue = new System.Windows.Forms.Button();
            this.btnAgree = new System.Windows.Forms.Button();
            this.lbladdugrop = new System.Windows.Forms.Label();
            this.cbgroup = new System.Windows.Forms.ComboBox();
            this.lblgroup = new System.Windows.Forms.Label();
            this.txtremark = new System.Windows.Forms.TextBox();
            this.lblremark = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnChange = new System.Windows.Forms.Button();
            this.btnok = new System.Windows.Forms.Button();
            this.paneltop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picturehead)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // paneltop
            // 
            this.paneltop.BackColor = System.Drawing.Color.Transparent;
            this.paneltop.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.paneltop.Controls.Add(this.lblask);
            this.paneltop.Controls.Add(this.lblsex);
            this.paneltop.Controls.Add(this.lblQQ);
            this.paneltop.Controls.Add(this.lblniname);
            this.paneltop.Controls.Add(this.picturehead);
            this.paneltop.Controls.Add(this.lblAddress2);
            this.paneltop.Location = new System.Drawing.Point(0, 0);
            this.paneltop.Name = "paneltop";
            this.paneltop.Size = new System.Drawing.Size(158, 121);
            this.paneltop.TabIndex = 3;
            // 
            // lblask
            // 
            this.lblask.AutoSize = true;
            this.lblask.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblask.Location = new System.Drawing.Point(21, 97);
            this.lblask.Name = "lblask";
            this.lblask.Size = new System.Drawing.Size(22, 20);
            this.lblask.TabIndex = 43;
            this.lblask.Text = "uj";
            // 
            // lblsex
            // 
            this.lblsex.AutoSize = true;
            this.lblsex.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblsex.ForeColor = System.Drawing.Color.Silver;
            this.lblsex.Location = new System.Drawing.Point(93, 54);
            this.lblsex.Name = "lblsex";
            this.lblsex.Size = new System.Drawing.Size(20, 17);
            this.lblsex.TabIndex = 41;
            this.lblsex.Text = "男";
            // 
            // lblQQ
            // 
            this.lblQQ.AutoSize = true;
            this.lblQQ.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblQQ.ForeColor = System.Drawing.Color.Black;
            this.lblQQ.Location = new System.Drawing.Point(92, 16);
            this.lblQQ.Name = "lblQQ";
            this.lblQQ.Size = new System.Drawing.Size(57, 20);
            this.lblQQ.TabIndex = 32;
            this.lblQQ.Text = "523455";
            // 
            // lblniname
            // 
            this.lblniname.AutoSize = true;
            this.lblniname.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblniname.ForeColor = System.Drawing.Color.Black;
            this.lblniname.Location = new System.Drawing.Point(93, 31);
            this.lblniname.Name = "lblniname";
            this.lblniname.Size = new System.Drawing.Size(29, 20);
            this.lblniname.TabIndex = 26;
            this.lblniname.Text = "nic";
            // 
            // picturehead
            // 
            this.picturehead.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.picturehead.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.picturehead.Location = new System.Drawing.Point(14, 16);
            this.picturehead.Name = "picturehead";
            this.picturehead.Size = new System.Drawing.Size(73, 72);
            this.picturehead.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picturehead.TabIndex = 23;
            this.picturehead.TabStop = false;
            this.picturehead.Click += new System.EventHandler(this.picturehead_Click);
            // 
            // lblAddress2
            // 
            this.lblAddress2.AutoSize = true;
            this.lblAddress2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblAddress2.ForeColor = System.Drawing.Color.Silver;
            this.lblAddress2.Location = new System.Drawing.Point(94, 71);
            this.lblAddress2.Name = "lblAddress2";
            this.lblAddress2.Size = new System.Drawing.Size(32, 17);
            this.lblAddress2.TabIndex = 42;
            this.lblAddress2.Text = "地址";
            // 
            // btnhulue
            // 
            this.btnhulue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(246)))), ((int)(((byte)(251)))));
            this.btnhulue.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnhulue.Location = new System.Drawing.Point(302, 98);
            this.btnhulue.Name = "btnhulue";
            this.btnhulue.Size = new System.Drawing.Size(87, 23);
            this.btnhulue.TabIndex = 45;
            this.btnhulue.Text = "忽略";
            this.btnhulue.UseVisualStyleBackColor = false;
            this.btnhulue.Click += new System.EventHandler(this.btnhulue_Click);
            // 
            // btnAgree
            // 
            this.btnAgree.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(246)))), ((int)(((byte)(251)))));
            this.btnAgree.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAgree.Location = new System.Drawing.Point(205, 98);
            this.btnAgree.Name = "btnAgree";
            this.btnAgree.Size = new System.Drawing.Size(81, 23);
            this.btnAgree.TabIndex = 44;
            this.btnAgree.Text = "同意";
            this.btnAgree.UseVisualStyleBackColor = false;
            this.btnAgree.Click += new System.EventHandler(this.btnAgree_Click);
            // 
            // lbladdugrop
            // 
            this.lbladdugrop.AutoSize = true;
            this.lbladdugrop.ForeColor = System.Drawing.Color.Blue;
            this.lbladdugrop.Location = new System.Drawing.Point(72, 50);
            this.lbladdugrop.Name = "lbladdugrop";
            this.lbladdugrop.Size = new System.Drawing.Size(53, 12);
            this.lbladdugrop.TabIndex = 50;
            this.lbladdugrop.Text = "新建分组";
            this.lbladdugrop.Click += new System.EventHandler(this.lbladdugrop_Click);
            this.lbladdugrop.DoubleClick += new System.EventHandler(this.lbladdugrop_Click);
            // 
            // cbgroup
            // 
            this.cbgroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbgroup.FormattingEnabled = true;
            this.cbgroup.Location = new System.Drawing.Point(74, 27);
            this.cbgroup.Name = "cbgroup";
            this.cbgroup.Size = new System.Drawing.Size(164, 20);
            this.cbgroup.TabIndex = 49;
            // 
            // lblgroup
            // 
            this.lblgroup.AutoSize = true;
            this.lblgroup.BackColor = System.Drawing.Color.Transparent;
            this.lblgroup.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblgroup.ForeColor = System.Drawing.Color.Black;
            this.lblgroup.Location = new System.Drawing.Point(2, 27);
            this.lblgroup.Name = "lblgroup";
            this.lblgroup.Size = new System.Drawing.Size(79, 20);
            this.lblgroup.TabIndex = 48;
            this.lblgroup.Text = "分       组：";
            // 
            // txtremark
            // 
            this.txtremark.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtremark.Location = new System.Drawing.Point(74, 3);
            this.txtremark.Name = "txtremark";
            this.txtremark.Size = new System.Drawing.Size(164, 21);
            this.txtremark.TabIndex = 47;
            // 
            // lblremark
            // 
            this.lblremark.AutoSize = true;
            this.lblremark.BackColor = System.Drawing.Color.Transparent;
            this.lblremark.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblremark.ForeColor = System.Drawing.Color.Black;
            this.lblremark.Location = new System.Drawing.Point(2, 4);
            this.lblremark.Name = "lblremark";
            this.lblremark.Size = new System.Drawing.Size(79, 20);
            this.lblremark.TabIndex = 46;
            this.lblremark.Text = "备注姓名：";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.cbgroup);
            this.panel1.Controls.Add(this.txtremark);
            this.panel1.Controls.Add(this.lbladdugrop);
            this.panel1.Controls.Add(this.lblremark);
            this.panel1.Controls.Add(this.lblgroup);
            this.panel1.Location = new System.Drawing.Point(161, 31);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(241, 64);
            this.panel1.TabIndex = 51;
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.Transparent;
            this.btnExit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnExit.FlatAppearance.BorderSize = 0;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Image = global::myQQ.Properties.Resources.退出;
            this.btnExit.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnExit.Location = new System.Drawing.Point(376, 2);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(26, 26);
            this.btnExit.TabIndex = 53;
            this.btnExit.UseCompatibleTextRendering = true;
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnChange
            // 
            this.btnChange.BackColor = System.Drawing.Color.Transparent;
            this.btnChange.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnChange.FlatAppearance.BorderSize = 0;
            this.btnChange.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnChange.Image = global::myQQ.Properties.Resources.最小化;
            this.btnChange.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnChange.Location = new System.Drawing.Point(354, 2);
            this.btnChange.Name = "btnChange";
            this.btnChange.Size = new System.Drawing.Size(26, 26);
            this.btnChange.TabIndex = 52;
            this.btnChange.UseCompatibleTextRendering = true;
            this.btnChange.UseVisualStyleBackColor = false;
            this.btnChange.Click += new System.EventHandler(this.btnChange_Click);
            // 
            // btnok
            // 
            this.btnok.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(246)))), ((int)(((byte)(251)))));
            this.btnok.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnok.Location = new System.Drawing.Point(256, 97);
            this.btnok.Name = "btnok";
            this.btnok.Size = new System.Drawing.Size(81, 23);
            this.btnok.TabIndex = 54;
            this.btnok.Text = "确定";
            this.btnok.UseVisualStyleBackColor = false;
            this.btnok.Click += new System.EventHandler(this.btnok_Click);
            // 
            // FrmNews
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(246)))), ((int)(((byte)(251)))));
            this.ClientSize = new System.Drawing.Size(403, 136);
            this.Controls.Add(this.btnok);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnhulue);
            this.Controls.Add(this.paneltop);
            this.Controls.Add(this.btnAgree);
            this.Controls.Add(this.btnChange);
            this.Controls.Add(this.btnExit);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmNews";
            this.Text = "FrmSystemNews";
            this.Load += new System.EventHandler(this.FrmSystemNews_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.FrmNews_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.FrmFrmNews_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.FrmFrmNews_MouseUp);
            this.paneltop.ResumeLayout(false);
            this.paneltop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picturehead)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel paneltop;
        private System.Windows.Forms.Label lblask;
        private System.Windows.Forms.Label lblsex;
        private System.Windows.Forms.Label lblQQ;
        private System.Windows.Forms.Label lblniname;
        private System.Windows.Forms.PictureBox picturehead;
        private System.Windows.Forms.Label lblAddress2;
        private System.Windows.Forms.Button btnhulue;
        private System.Windows.Forms.Button btnAgree;
        private System.Windows.Forms.Label lbladdugrop;
        private System.Windows.Forms.ComboBox cbgroup;
        private System.Windows.Forms.Label lblgroup;
        private System.Windows.Forms.TextBox txtremark;
        private System.Windows.Forms.Label lblremark;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnChange;
        private System.Windows.Forms.Button btnok;
    }
}