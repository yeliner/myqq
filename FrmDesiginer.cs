﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace myQQ
{
    public partial class FrmDesiginer : Form
    {
        public FrmDesiginer()
        {
            InitializeComponent();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {//关闭
            Application.Exit();
        }



        private void FrmDesiginer_Load(object sender, EventArgs e)
        {
            this.cboleixin.Text = "不使用代理";
            loadleixin1();
            this.cbologinleixin.Text = "不使用高级选项";
            loadleixin2();
        }

        private void btnChange_Click(object sender, EventArgs e)
        {
            //最小化
            this.WindowState = FormWindowState.Minimized;
        }

        private void cboleixin_SelectedIndexChanged(object sender, EventArgs e)
        {  //网络使用类型判断
            loadleixin1();
        }
        private void cbologinleixin_SelectedIndexChanged(object sender, EventArgs e)
        {  //登录服务器
            loadleixin2();
        }
        private void loadleixin1(){
            //网络使用类型判断
            if (this.cboleixin.Text == "不使用代理" || this.cboleixin.Text == "使用浏览器设置")
            {
                this.txtdizhi.Enabled = false;
                this.txtduankou.Enabled = false;
                this.txtpwd.Enabled = false;
                this.txtuser.Enabled = false;
                this.txtare.Enabled = false;
                btntext.Enabled = false;

            }
            if (this.cboleixin.Text == "HTTP代理")
            {
                this.txtdizhi.Enabled = true;
                this.txtpwd.Enabled = true;
                this.txtuser.Enabled = true;
                this.txtduankou.Text = "80";
                this.txtare.Enabled =false;
                btntext.Enabled = false;
            }
            if (this.cboleixin.Text == "SOCKS5代理")
            {
                this.txtdizhi.Enabled = true;
                this.txtpwd.Enabled = true;
                this.txtuser.Enabled = true;
                this.txtduankou.Text = "1080";
                this.txtare.Enabled = false;
                btntext.Enabled = false;
            }
        }
        private void loadleixin2()
        {
            //登录服务器
         
            if (cbologinleixin.Text == "不使用高级选项")
            {
                cbologindizhi.Enabled = false;
                txtloginduankou.Enabled = false;
            }
            if (cbologinleixin.Text == "UDP类型")
            {

            }
            if (cbologinleixin.Text == "TCP类型")
            {

            }
        }

        private void btnok_Click(object sender, EventArgs e)
        {
            //点击确定 跳转到登录
            FrmLogin login = new FrmLogin();
            this.Visible = false;
            login.Show();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {

            //点击取消 跳转到登录
            FrmLogin login = new FrmLogin();
            this.Visible = false;
            login.Show();
        }
        Point mouseOff;//鼠标移动位置变量
        bool leftFlag;//标记是否为左键
        private void FrmDesiginer_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                mouseOff = new Point(-e.X, -e.Y); //得到变量的值
                leftFlag = true;                  //点击左键按下时标注为true;
            }
        }

        private void FrmDesiginer_MouseDown(object sender, MouseEventArgs e)
        {
            if (leftFlag)
            {
                Point mouseSet = Control.MousePosition;
                mouseSet.Offset(mouseOff.X, mouseOff.Y);  //设置移动后的位置
                Location = mouseSet;
            }
        }

        private void FrmDesiginer_MouseUp(object sender, MouseEventArgs e)
        {
            if (leftFlag)
            {
                leftFlag = false;//释放鼠标后标注为false;
            }
        }
    }
}
