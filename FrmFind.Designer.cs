﻿namespace myQQ
{
    partial class FrmFind
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblt3 = new System.Windows.Forms.Label();
            this.lblt1 = new System.Windows.Forms.Label();
            this.lblhead = new System.Windows.Forms.Label();
            this.lblt2 = new System.Windows.Forms.Label();
            this.txta3 = new System.Windows.Forms.TextBox();
            this.txta2 = new System.Windows.Forms.TextBox();
            this.txta1 = new System.Windows.Forms.TextBox();
            this.lbla3 = new System.Windows.Forms.Label();
            this.lbla2 = new System.Windows.Forms.Label();
            this.lbla1 = new System.Windows.Forms.Label();
            this.btnnext = new System.Windows.Forms.Button();
            this.btncancel = new System.Windows.Forms.Button();
            this.lblnone = new System.Windows.Forms.Label();
            this.ppwd = new System.Windows.Forms.Panel();
            this.lblpwdagintishi = new System.Windows.Forms.Label();
            this.lbl2 = new System.Windows.Forms.Label();
            this.lblresult = new System.Windows.Forms.Label();
            this.lblqiangdu = new System.Windows.Forms.Label();
            this.lbl1 = new System.Windows.Forms.Label();
            this.txtpwdagin = new System.Windows.Forms.TextBox();
            this.txtPwd = new System.Windows.Forms.TextBox();
            this.lblpwd = new System.Windows.Forms.Label();
            this.lblpwdtishi = new System.Windows.Forms.Label();
            this.lbltishi = new System.Windows.Forms.Label();
            this.lblpwdagin = new System.Windows.Forms.Label();
            this.btnok = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnChange = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.ppwd.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblt3);
            this.panel1.Controls.Add(this.lblt1);
            this.panel1.Controls.Add(this.lblhead);
            this.panel1.Controls.Add(this.lblt2);
            this.panel1.Controls.Add(this.txta3);
            this.panel1.Controls.Add(this.txta2);
            this.panel1.Controls.Add(this.txta1);
            this.panel1.Controls.Add(this.lbla3);
            this.panel1.Controls.Add(this.lbla2);
            this.panel1.Controls.Add(this.lbla1);
            this.panel1.Location = new System.Drawing.Point(82, 27);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(281, 291);
            this.panel1.TabIndex = 32;
            // 
            // lblt3
            // 
            this.lblt3.AutoSize = true;
            this.lblt3.ForeColor = System.Drawing.Color.Red;
            this.lblt3.Location = new System.Drawing.Point(3, 245);
            this.lblt3.Name = "lblt3";
            this.lblt3.Size = new System.Drawing.Size(65, 12);
            this.lblt3.TabIndex = 38;
            this.lblt3.Text = "答案错误！";
            this.lblt3.Visible = false;
            // 
            // lblt1
            // 
            this.lblt1.AutoSize = true;
            this.lblt1.ForeColor = System.Drawing.Color.Red;
            this.lblt1.Location = new System.Drawing.Point(3, 82);
            this.lblt1.Name = "lblt1";
            this.lblt1.Size = new System.Drawing.Size(65, 12);
            this.lblt1.TabIndex = 37;
            this.lblt1.Text = "答案错误！";
            this.lblt1.Visible = false;
            // 
            // lblhead
            // 
            this.lblhead.AutoSize = true;
            this.lblhead.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblhead.Location = new System.Drawing.Point(105, 13);
            this.lblhead.Name = "lblhead";
            this.lblhead.Size = new System.Drawing.Size(65, 20);
            this.lblhead.TabIndex = 33;
            this.lblhead.Text = "找回密码";
            // 
            // lblt2
            // 
            this.lblt2.AutoSize = true;
            this.lblt2.ForeColor = System.Drawing.Color.Red;
            this.lblt2.Location = new System.Drawing.Point(2, 161);
            this.lblt2.Name = "lblt2";
            this.lblt2.Size = new System.Drawing.Size(65, 12);
            this.lblt2.TabIndex = 36;
            this.lblt2.Text = "答案错误！";
            this.lblt2.Visible = false;
            // 
            // txta3
            // 
            this.txta3.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txta3.Location = new System.Drawing.Point(1, 260);
            this.txta3.Name = "txta3";
            this.txta3.Size = new System.Drawing.Size(276, 26);
            this.txta3.TabIndex = 35;
            // 
            // txta2
            // 
            this.txta2.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txta2.Location = new System.Drawing.Point(1, 176);
            this.txta2.Name = "txta2";
            this.txta2.Size = new System.Drawing.Size(276, 26);
            this.txta2.TabIndex = 34;
            // 
            // txta1
            // 
            this.txta1.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txta1.Location = new System.Drawing.Point(1, 96);
            this.txta1.Name = "txta1";
            this.txta1.Size = new System.Drawing.Size(276, 26);
            this.txta1.TabIndex = 33;
            // 
            // lbla3
            // 
            this.lbla3.AutoSize = true;
            this.lbla3.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbla3.Location = new System.Drawing.Point(6, 214);
            this.lbla3.Name = "lbla3";
            this.lbla3.Size = new System.Drawing.Size(65, 20);
            this.lbla3.TabIndex = 32;
            this.lbla3.Text = "问题三：";
            // 
            // lbla2
            // 
            this.lbla2.AutoSize = true;
            this.lbla2.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbla2.Location = new System.Drawing.Point(7, 135);
            this.lbla2.Name = "lbla2";
            this.lbla2.Size = new System.Drawing.Size(65, 20);
            this.lbla2.TabIndex = 31;
            this.lbla2.Text = "问题二：";
            // 
            // lbla1
            // 
            this.lbla1.AutoSize = true;
            this.lbla1.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbla1.Location = new System.Drawing.Point(7, 52);
            this.lbla1.Name = "lbla1";
            this.lbla1.Size = new System.Drawing.Size(65, 20);
            this.lbla1.TabIndex = 30;
            this.lbla1.Text = "问题一：";
            // 
            // btnnext
            // 
            this.btnnext.Location = new System.Drawing.Point(219, 355);
            this.btnnext.Name = "btnnext";
            this.btnnext.Size = new System.Drawing.Size(75, 23);
            this.btnnext.TabIndex = 38;
            this.btnnext.Text = "下一步";
            this.btnnext.UseVisualStyleBackColor = true;
            this.btnnext.Click += new System.EventHandler(this.btnnext_Click);
            // 
            // btncancel
            // 
            this.btncancel.Location = new System.Drawing.Point(304, 355);
            this.btncancel.Name = "btncancel";
            this.btncancel.Size = new System.Drawing.Size(73, 23);
            this.btncancel.TabIndex = 37;
            this.btncancel.Text = "取消";
            this.btncancel.UseVisualStyleBackColor = true;
            this.btncancel.Click += new System.EventHandler(this.btncancel_Click);
            // 
            // lblnone
            // 
            this.lblnone.AutoSize = true;
            this.lblnone.Font = new System.Drawing.Font("幼圆", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblnone.ForeColor = System.Drawing.Color.Blue;
            this.lblnone.Location = new System.Drawing.Point(85, 143);
            this.lblnone.Name = "lblnone";
            this.lblnone.Size = new System.Drawing.Size(280, 48);
            this.lblnone.TabIndex = 39;
            this.lblnone.Text = "很抱歉，您并没有在安全中心设置密码\r\n\r\n         很遗憾，不能帮亲找回密码";
            this.lblnone.Visible = false;
            // 
            // ppwd
            // 
            this.ppwd.Controls.Add(this.lblpwdagintishi);
            this.ppwd.Controls.Add(this.lbl2);
            this.ppwd.Controls.Add(this.lblresult);
            this.ppwd.Controls.Add(this.lblqiangdu);
            this.ppwd.Controls.Add(this.lbl1);
            this.ppwd.Controls.Add(this.txtpwdagin);
            this.ppwd.Controls.Add(this.txtPwd);
            this.ppwd.Controls.Add(this.lblpwd);
            this.ppwd.Controls.Add(this.lblpwdtishi);
            this.ppwd.Controls.Add(this.lbltishi);
            this.ppwd.Controls.Add(this.lblpwdagin);
            this.ppwd.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ppwd.Location = new System.Drawing.Point(12, 30);
            this.ppwd.Name = "ppwd";
            this.ppwd.Size = new System.Drawing.Size(461, 291);
            this.ppwd.TabIndex = 39;
            this.ppwd.Visible = false;
            this.ppwd.MouseDown += new System.Windows.Forms.MouseEventHandler(this.FrmFind_MouseDown);
            this.ppwd.MouseMove += new System.Windows.Forms.MouseEventHandler(this.FrmFind_MouseMove);
            this.ppwd.MouseUp += new System.Windows.Forms.MouseEventHandler(this.FrmFind_MouseUp);
            // 
            // lblpwdagintishi
            // 
            this.lblpwdagintishi.AutoSize = true;
            this.lblpwdagintishi.ForeColor = System.Drawing.Color.Red;
            this.lblpwdagintishi.Location = new System.Drawing.Point(369, 202);
            this.lblpwdagintishi.Name = "lblpwdagintishi";
            this.lblpwdagintishi.Size = new System.Drawing.Size(68, 17);
            this.lblpwdagintishi.TabIndex = 50;
            this.lblpwdagintishi.Text = "不能为空！";
            this.lblpwdagintishi.Visible = false;
            // 
            // lbl2
            // 
            this.lbl2.AutoSize = true;
            this.lbl2.ForeColor = System.Drawing.Color.Silver;
            this.lbl2.Location = new System.Drawing.Point(111, 227);
            this.lbl2.Name = "lbl2";
            this.lbl2.Size = new System.Drawing.Size(104, 17);
            this.lbl2.TabIndex = 49;
            this.lbl2.Text = "必须和新密码相同";
            // 
            // lblresult
            // 
            this.lblresult.AutoSize = true;
            this.lblresult.Location = new System.Drawing.Point(111, 158);
            this.lblresult.Name = "lblresult";
            this.lblresult.Size = new System.Drawing.Size(53, 17);
            this.lblresult.TabIndex = 48;
            this.lblresult.Text = "测试中...";
            // 
            // lblqiangdu
            // 
            this.lblqiangdu.AutoSize = true;
            this.lblqiangdu.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblqiangdu.Location = new System.Drawing.Point(42, 158);
            this.lblqiangdu.Name = "lblqiangdu";
            this.lblqiangdu.Size = new System.Drawing.Size(77, 14);
            this.lblqiangdu.TabIndex = 47;
            this.lblqiangdu.Text = "密码强度：";
            // 
            // lbl1
            // 
            this.lbl1.AutoSize = true;
            this.lbl1.ForeColor = System.Drawing.Color.Silver;
            this.lbl1.Location = new System.Drawing.Point(111, 123);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(142, 17);
            this.lbl1.TabIndex = 46;
            this.lbl1.Text = "密码必须为6-16位数组成";
            // 
            // txtpwdagin
            // 
            this.txtpwdagin.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtpwdagin.Location = new System.Drawing.Point(108, 198);
            this.txtpwdagin.MaxLength = 16;
            this.txtpwdagin.Name = "txtpwdagin";
            this.txtpwdagin.PasswordChar = '●';
            this.txtpwdagin.Size = new System.Drawing.Size(255, 26);
            this.txtpwdagin.TabIndex = 45;
            // 
            // txtPwd
            // 
            this.txtPwd.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtPwd.Location = new System.Drawing.Point(108, 92);
            this.txtPwd.MaxLength = 16;
            this.txtPwd.Name = "txtPwd";
            this.txtPwd.PasswordChar = '●';
            this.txtPwd.Size = new System.Drawing.Size(255, 26);
            this.txtPwd.TabIndex = 43;
            // 
            // lblpwd
            // 
            this.lblpwd.AutoSize = true;
            this.lblpwd.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblpwd.Location = new System.Drawing.Point(42, 97);
            this.lblpwd.Name = "lblpwd";
            this.lblpwd.Size = new System.Drawing.Size(63, 14);
            this.lblpwd.TabIndex = 42;
            this.lblpwd.Text = "新密码：";
            // 
            // lblpwdtishi
            // 
            this.lblpwdtishi.AutoSize = true;
            this.lblpwdtishi.ForeColor = System.Drawing.Color.Red;
            this.lblpwdtishi.Location = new System.Drawing.Point(366, 97);
            this.lblpwdtishi.Name = "lblpwdtishi";
            this.lblpwdtishi.Size = new System.Drawing.Size(68, 17);
            this.lblpwdtishi.TabIndex = 41;
            this.lblpwdtishi.Text = "不能为空！";
            this.lblpwdtishi.Visible = false;
            // 
            // lbltishi
            // 
            this.lbltishi.AutoSize = true;
            this.lbltishi.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbltishi.ForeColor = System.Drawing.Color.Blue;
            this.lbltishi.Location = new System.Drawing.Point(20, 13);
            this.lbltishi.Name = "lbltishi";
            this.lbltishi.Size = new System.Drawing.Size(404, 51);
            this.lbltishi.TabIndex = 34;
            this.lbltishi.Text = "为了保护您的帐号安全，避免您和您的好友受到损失，建议您设置密码时：\r\n1、先将电脑全面杀毒，再设置新密码。\r\n2、不同的QQ帐号使用不同的密码，不要设置和其它网站" +
    "相同的密码。";
            // 
            // lblpwdagin
            // 
            this.lblpwdagin.AutoSize = true;
            this.lblpwdagin.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblpwdagin.Location = new System.Drawing.Point(42, 205);
            this.lblpwdagin.Name = "lblpwdagin";
            this.lblpwdagin.Size = new System.Drawing.Size(77, 14);
            this.lblpwdagin.TabIndex = 44;
            this.lblpwdagin.Text = "确认密码：";
            // 
            // btnok
            // 
            this.btnok.Location = new System.Drawing.Point(219, 355);
            this.btnok.Name = "btnok";
            this.btnok.Size = new System.Drawing.Size(75, 23);
            this.btnok.TabIndex = 40;
            this.btnok.Text = "确认";
            this.btnok.UseVisualStyleBackColor = true;
            this.btnok.Visible = false;
            this.btnok.Click += new System.EventHandler(this.btnok_Click);
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.Transparent;
            this.btnExit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnExit.FlatAppearance.BorderSize = 0;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Image = global::myQQ.Properties.Resources.退出;
            this.btnExit.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnExit.Location = new System.Drawing.Point(447, 1);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(26, 26);
            this.btnExit.TabIndex = 42;
            this.btnExit.UseCompatibleTextRendering = true;
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnChange
            // 
            this.btnChange.BackColor = System.Drawing.Color.Transparent;
            this.btnChange.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnChange.FlatAppearance.BorderSize = 0;
            this.btnChange.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnChange.Image = global::myQQ.Properties.Resources.最小化;
            this.btnChange.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnChange.Location = new System.Drawing.Point(419, 1);
            this.btnChange.Name = "btnChange";
            this.btnChange.Size = new System.Drawing.Size(26, 26);
            this.btnChange.TabIndex = 41;
            this.btnChange.UseCompatibleTextRendering = true;
            this.btnChange.UseVisualStyleBackColor = false;
            this.btnChange.Click += new System.EventHandler(this.btnChange_Click);
            // 
            // FrmFind
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(235)))), ((int)(((byte)(253)))));
            this.ClientSize = new System.Drawing.Size(485, 406);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnChange);
            this.Controls.Add(this.btnok);
            this.Controls.Add(this.ppwd);
            this.Controls.Add(this.btnnext);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lblnone);
            this.Controls.Add(this.btncancel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmFind";
            this.Text = "FrmFind";
            this.Load += new System.EventHandler(this.FrmFind_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.FrmFind_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.FrmFind_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.FrmFind_MouseUp);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ppwd.ResumeLayout(false);
            this.ppwd.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblt3;
        private System.Windows.Forms.Label lblt1;
        private System.Windows.Forms.Label lblt2;
        private System.Windows.Forms.TextBox txta3;
        private System.Windows.Forms.TextBox txta2;
        private System.Windows.Forms.TextBox txta1;
        private System.Windows.Forms.Label lbla3;
        private System.Windows.Forms.Label lbla2;
        private System.Windows.Forms.Label lbla1;
        private System.Windows.Forms.Label lblhead;
        private System.Windows.Forms.Button btnnext;
        private System.Windows.Forms.Button btncancel;
        private System.Windows.Forms.Label lblnone;
        private System.Windows.Forms.Panel ppwd;
        private System.Windows.Forms.Label lblpwdtishi;
        private System.Windows.Forms.Label lbltishi;
        private System.Windows.Forms.TextBox txtpwdagin;
        private System.Windows.Forms.Label lblpwdagin;
        private System.Windows.Forms.TextBox txtPwd;
        private System.Windows.Forms.Label lblpwd;
        private System.Windows.Forms.Label lblpwdagintishi;
        private System.Windows.Forms.Label lbl2;
        private System.Windows.Forms.Label lblresult;
        private System.Windows.Forms.Label lblqiangdu;
        private System.Windows.Forms.Label lbl1;
        private System.Windows.Forms.Button btnok;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnChange;
    }
}