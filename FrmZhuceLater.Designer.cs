﻿namespace myQQ
{
    partial class FrmZhuceLater
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnExit = new System.Windows.Forms.Button();
            this.btnLogin = new System.Windows.Forms.Button();
            this.lbltishi = new System.Windows.Forms.Label();
            this.pcstubiao = new System.Windows.Forms.PictureBox();
            this.lblQQ = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pcstubiao)).BeginInit();
            this.SuspendLayout();
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(235)))), ((int)(((byte)(253)))));
            this.btnExit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnExit.FlatAppearance.BorderSize = 0;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Image = global::myQQ.Properties.Resources.退出;
            this.btnExit.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnExit.Location = new System.Drawing.Point(403, 3);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(29, 26);
            this.btnExit.TabIndex = 28;
            this.btnExit.UseCompatibleTextRendering = true;
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnLogin
            // 
            this.btnLogin.BackgroundImage = global::myQQ.Properties.Resources.登录qq;
            this.btnLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogin.Location = new System.Drawing.Point(169, 211);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(100, 40);
            this.btnLogin.TabIndex = 27;
            this.btnLogin.UseVisualStyleBackColor = true;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // lbltishi
            // 
            this.lbltishi.AutoSize = true;
            this.lbltishi.Font = new System.Drawing.Font("宋体", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbltishi.Location = new System.Drawing.Point(132, 107);
            this.lbltishi.Name = "lbltishi";
            this.lbltishi.Size = new System.Drawing.Size(289, 20);
            this.lbltishi.TabIndex = 26;
            this.lbltishi.Text = "注册成功，您获得的QQ账号为：";
            // 
            // pcstubiao
            // 
            this.pcstubiao.Image = global::myQQ.Properties.Resources.注册成功1;
            this.pcstubiao.Location = new System.Drawing.Point(36, 95);
            this.pcstubiao.Name = "pcstubiao";
            this.pcstubiao.Size = new System.Drawing.Size(90, 90);
            this.pcstubiao.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pcstubiao.TabIndex = 25;
            this.pcstubiao.TabStop = false;
            // 
            // lblQQ
            // 
            this.lblQQ.AutoSize = true;
            this.lblQQ.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblQQ.Location = new System.Drawing.Point(145, 156);
            this.lblQQ.Name = "lblQQ";
            this.lblQQ.Size = new System.Drawing.Size(0, 22);
            this.lblQQ.TabIndex = 29;
            // 
            // FrmZhuceLater
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(235)))), ((int)(((byte)(253)))));
            this.ClientSize = new System.Drawing.Size(434, 331);
            this.Controls.Add(this.lblQQ);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnLogin);
            this.Controls.Add(this.lbltishi);
            this.Controls.Add(this.pcstubiao);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmZhuceLater";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "注册成功";
            this.Load += new System.EventHandler(this.FrmZhuceLater_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.FrmZhuceLater_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.FrmZhuceLater_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.FrmZhuceLater_MouseUp);
            ((System.ComponentModel.ISupportInitialize)(this.pcstubiao)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.Label lbltishi;
        private System.Windows.Forms.PictureBox pcstubiao;
        private System.Windows.Forms.Label lblQQ;
    }
}